// lightGallery
lightGallery(document.getElementById('lightgallery'));
lightGallery(document.getElementById('lightgalleryCarouselSlide1'));
lightGallery(document.getElementById('lightgalleryCarouselSlide2'));
lightGallery(document.getElementById('lightgalleryCarouselSlide3'));
lightGallery(document.getElementById('lightgalleryCarouselSlide4'));

// Tracking Variables
var product_sku;
var product_name;
var product_category;
// var product_price;

$(document).on('ready', function () {
    $.HSCore.components.HSCarousel.init('.js-carousel');  
    var ext = $('#existencia_sku').text();
    
    if (ext <= 0) {
        $('#sinExt').show();
        $('#conExt').hide();
    }else{
        $('#conExt').show();
        $('#sinExt').hide();
    }

    
    $.each($("input[name=variantes_exist]"), function( index ) {
        if ($(this).val() == 0) {
            var id = $(this).attr('id').replace('check_', '');
            $('#'+id).click();
            return false;
        }
    });


    $(".loader").hide();

    product_sku = $("#sku_prod").text().trim();
    product_name = $('#model b').text();
    product_category = capitalizeString( $("#tipoProd").text() );
    // product_price = parseFloat($('#price_desc').text());
    // if(!product_price){
    //     product_price = parseFloat($('#product_price').text());;
    // }

    var aux_price = $('#price').text();
    var price = parseFloat(aux_price.replace(',', ''));
    var aux_price_desc = $('#price_desc').text();
    var price_desc = parseFloat(aux_price_desc.replace(',', ''));


    gtag('event', 'view_item', {
        "items": [
            {
                "id": product_sku,
                "name": product_name,
                "list_name": "Product View",
                "category": product_category,
                "price": isNaN(price_desc) ? price : price_desc,
            }
        ]
    });

    fbq('track', 'ViewContent', {
        content_type: "product",
        content_ids: [product_sku],
        content_name: product_name,
        content_category: product_category,
    });
});

var URLactual = window.location.pathname;
var protocol = window.location.protocol;
var host = window.location.hostname;
var port = window.location.port;

var url = protocol+"//" + host
if (port) {
    url = url + ":" + port
}

var NO_PARTE = URLactual.split('/');

function addCar() {
    var name = $('#model').text();
    var aux_price = $('#price').text();
    var price = parseFloat(aux_price.replace(',', ''));
    var aux_price_desc = $('#price_desc').text();
    var price_desc = parseFloat(aux_price_desc.replace(',', ''));
    var cantidad = $('#cantidad').val();
    var existencia = $('#existenciaParte').val();
    var producto = $('#tipoProd').val();
    var imagen = $('#imgProd').attr('data-src');
    var id = getId();
    var talla = id.replace("  ", " ");
    talla = talla.split(" ")[1];
    $('#name').text(talla);
    
    if (id.substring(0, 9) === "MT9C1-006"){
        id = id.replace(' ','  ');
    }

    if (talla && (existencia - cantidad) >= 0 && cantidad > 0) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: url + "/productos/addcar",
            dataType: 'json',
            data: {
                id: id,
                talla: talla,
                name: name,
                price: isNaN(price_desc) ? price : price_desc,
                price_origin: price,
                cantidad: cantidad,
                producto: producto, 
                category: product_category,
                imagen: imagen
            },
            success: function(msg) {
                $('#modalCheckout').modal('show');
            }
        });
    } else {
        if (existencia == 0 || !talla) {
            alertMuletta('talla no añadida');
        } else {
            alertMuletta('La talla solicitada no está disponible');
        }
    }

    gtag('event', 'add_to_cart', {
        "items": [
            {
                "id": product_sku,
                "name": product_name,
                "list_name": "Product added To Cart",
                "category": product_category,
                "price": isNaN(price_desc) ? price : price_desc,
                "variant": talla,
                "quantity": cantidad,
            }
        ]
    });

    fbq('track', 'AddToCart', {
        content_type: "product",
        currency: "MXN",
        content_ids: [product_sku],
        content_name: product_name,
        content_category: product_category,
        value: isNaN(price_desc) ? price : price_desc,
        contents: [{id:product_sku, quantity: cantidad, size: talla}]
    });
}

function refreshCheckout() {
    location.reload(true);
}

function refreshProductPrice(s, ext) {

    $('#existenciaParte').val(ext);

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url + "/producto/getProductsWithVariants",
        dataType: 'json',
        data: {
            s: s
        },
        success: function(msg) {
            $("#existencia_sku").text(ext);
            $("#price").html(msg.html);

            if (ext <= 0) {
                $('#sinExt').show();
                $('#conExt').hide();
            }else{
                $('#conExt').show();
                $('#sinExt').hide();
            }
        }
    });

}

function refreshProductName(d) {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url + "/producto/getProductsWithVariantsName",
        dataType: 'json',
        data: {
            d: d
        },
        success: function(msg) {
            $("#tag").html(msg.html);
        }
    });

}

function getId() {

    var variantes = $('input[name=variantes]:checked').val();
    if (variantes === undefined) {
        variantes = NO_PARTE[(NO_PARTE.length - 1)];
    }

    return variantes;
}


function alertMuletta(text){
    Swal.fire({
        title: text,
        icon: 'error',
        confirmButtonColor: '#b78b1e',
        confirmButtonText: 'Aceptar'
    }).then((result) => {
        location.reload();
    });
}

function productoNegado(){
    var correo = $('#txtCorreo').val();
    var nombre = $('#txtNombre').val();

    var variantes = $('input[name=variantes]:checked').val();

    var talla = $('input[name=variantes]:checked').attr('id');

    if (!variantes) {
        Swal.fire({
            title: 'Favor de seleccionar una talla',
            icon: 'error',
            confirmButtonColor: '#b78b1e',
            confirmButtonText: 'Aceptar'
        });
    }else{
        if (!correo && !nombre) {
            $('#modalNotificaciones').modal('show');
            $('#modal_'+talla).click();
            $('#errorCorreo').hide();
            $('#errorNombre').hide();
            $('#errorCorreoValidacion').hide();
        }else{
            saveProductoNegado(nombre, correo);
        }
    }

}

function refreshProductSize(talla_modelo){
    $('#'+talla_modelo).click();
}

function btnProductoNegado(){
    var correo = $('#txtCorreoManual').val();
    var nombre = $('#txtNombreManual').val();

    var error = false;

    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z-])+\.)+([a-zA-Z]{2,4})+$/;

    if (!correo) {
        error = true;
        $('#errorCorreo').show();
    }

    if (correo && !regex.test(correo)) {
        error = true;
        $('#errorCorreo').hide();
        $('#errorCorreoValidacion').show();
    }

    if (!nombre) {
        error = true;
        $('#errorNombre').show();
    }

    if (!error) {
        saveProductoNegado(nombre, correo);
    }
}

function closeModal(){
    $('#modalNotificaciones').modal('hide');
}

function saveProductoNegado(nombre, correo){
    var id = getId();
    var url = $('#urlNotify').val();
    $(".loader").show();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url,
        dataType: 'json',
        data: {
            NO_PARTE: id,
            NOMBRE: nombre,
            CORREO: correo
        },
        success: function(msg) {
            if (msg) {
                Swal.fire({
                    title: 'Exito te notificaremos cuando este disponible',
                    icon: 'success',
                    confirmButtonColor: '#b78b1e',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    $('#modalNotificaciones').modal('hide');
                });
            } else {
                alertMuletta('Lo sentimos ha ocurrido un error')
            }

            $(".loader").hide();
        },
        error: function(error){
            console.error(error);
            $(".loader").show();
        }
    });
    
}