
$('#add_adress').click(function () {
    $('#title-modal').text('Agregar direccion');
    $('#addInput').val('nuevo');
    openModal();
});

function openModal(){
    $('#form-pass')[0].reset();
    $('#id_address').val(null);
    $('#origen').val(null);
    $('#mun_user').empty();
    $('#modalAdress').modal('show');
}

$('#form-pass').submit(function( event ) {
    event.preventDefault();
    var id = $('#addInput').val();

    if (id == 'nuevo') {
        addAdress();
    }else{
        uploadAddress();
    }
    
});

function addAdress(){

    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    url = url + '/usuario/cuenta/agregar-direccion';

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "post",
        url: url,
        dataType: 'json',
        data: $('#form-pass').serialize(),
        success: function(msg) {
            console.log(msg);
            if (msg[0]) {
                $('#modalCheckout').modal('hide');
                reloadWindow('Direccion guardada');
            }
        },
        error: function(err) {
            console.log(err);
            
        }
    });
    
}

function editarAdress(id_direccion, origen){
    var cliente = $('#email_cliente').val();
    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    url = url + '/usuario/cuenta/obtener-direccion';

    $('#mun_user').empty();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "post",
        url: url,
        dataType: 'json',
        data: {
            id_direccion: id_direccion,
            origen: origen,
            email: cliente
        },
        success: function(msg) {
            if (msg['CALLE']) {
                $('#title-modal').text('Editar direccion');
                openModal();
                $('#addInput').val('editar');
                $('#id_address').val(id_direccion);
                $('#origen').val(origen);
                $('#street_user').val(msg['CALLE']);
                $('#num_int_user').val(msg['NUM_INT']);
                $('#num_ext_user').val(msg['NUM_EXT']);
                $('#col_user').val(msg['COLONIA']);
                $('#code_postal_user').val(msg['CODIGO']);
                $('#state_user').val(msg['ID_ESTADO']);
                buscarMunicipios();
                $('#alias').val(msg['ALIAS']);
                $('#referencia').val(msg['REFERENCIA']);

                setTimeout(function() {
                    $('#mun_user').val(msg['ID_MUNICIPIO']);
                }, 360)
            }
        },
        error: function() {
        }
    });
}

function buscarMunicipios(){
    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    var estado = $('#state_user').val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url + '/municipios',
        data: {
            ID_ESTADO: estado
        },
        dataType: 'json',
        success: function( msg ) {
            $('#div_municipios').show();
            
            var opciones = '';
            msg.forEach(element => {

                opciones = opciones + '<option value="'+element.ID_MUNICIPIO+'"> '+element.DESCRIPCION_MUNICIPIO+' </option>';
                
            });

            $('#mun_user').empty();
            $('#mun_user').append(opciones);
        }
    });
}

function uploadAddress(){

    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    url = url + '/usuario/cuenta/upload-direccion';

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "post",
        url: url,
        dataType: 'json',
        data: $('#form-pass').serialize(),
        success: function(msg) {
            if (msg[0]['CALLE']) {
                $('#modalCheckout').modal('hide');
                reloadWindow('Direccion actualizada');
            }
        },
        error: function(err) {
            console.log(err);
        }
    });

}

function eliminarAddress(id_direccion){
    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    url = url + '/usuario/cuenta/eliminar-direccion';

    Swal.fire({
        title: 'Estas seguro que desea eliminar?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#b78b1e',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
      }).then((result) => {
        if (result.value) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: "post",
                url: url,
                dataType: 'json',
                data: {id_direccion: id_direccion},
                success: function(msg) {
                    console.log(msg);
                    
                    reloadWindow('Tu direccion a sido eliminada.');
                },
                error: function() {
                }
            });
        }
    });
}


function reloadWindow(text){
    Swal.fire({
        title: text,
        icon: 'success',
        confirmButtonColor: '#b78b1e',
        confirmButtonText: 'Aceptar'
    }).then((result) => {
        location.reload();
    });
}