function changedelete(index){
    $('.loader').show();

    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    url = url + '/compras/changedelete';
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url,
        dataType: 'json',
        data: { position: index },
        success: function( response ) {

            var removed_item = response.removed_item;

            gtag('event', 'remove_from_cart', {
                "items": [
                    {
                        "id": removed_item.id.substring(0, removed_item.id.indexOf(' ')),
                        "name": removed_item.name,
                        "list_name": "Product View",
                        "category": removed_item.category,
                        "price": removed_item.price,
                        "variant": removed_item.talla,
                        "quantity": removed_item.quantity,
                    }
                ]
            });

            location.reload(true);
        }
    });
}