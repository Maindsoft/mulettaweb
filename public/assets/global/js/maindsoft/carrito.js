/*Esconde elementos de paso 2 - direcciones al cargar pagina y manda llamar PayPal*/
$(document).on('ready', function () {
    $('.loader').hide();

    initStepForm();
    getDevices();

    $('#save_address').hide();
    $('#regalo_div_ult').hide();
    $('#errorCodigo').hide();
    $('#regalo_div').hide();

    esconderFormularioDir();
    esconderTarjeta();
    $('#btn_saved_dir').hide();

    $('#fwebpay_url').hide();
    $('#btn_finalizar').show();

    if ($(':input[name=radGroup2_1]').is(':checked') == true) {

        $(this).attr('checked', false).val('');
    }

    trackingMetadata = getCartTrackingMetadata('Checkout Cart List');

    gtag('event', 'begin_checkout', {
        "items": trackingMetadata.google_items,
    });

    fbq("track", "InitiateCheckout", {
        content_type: 'product',
        content_ids: trackingMetadata.fb_content_ids,
        contents: trackingMetadata.fb_contents
    });

});

/*esconde tarjeta*/
function esconderTarjeta() {
    $('#dir-cards').hide();
}

/*esconde el formulario*/
function esconderFormularioDir() {
    $('#0-row-dir').hide();
    $('#1-row-dir').hide();
    $('#2-row-dir').hide();
    $('#3-row-dir').hide();
}

/*muestra el formulario*/
function mostrarFormularioDir() {
    $('#0-row-dir').show();
    $('#1-row-dir').show();
    $('#2-row-dir').show();
    $('#3-row-dir').show();
    $('#save_address').show();
}

/*Esconde pestañas 2 y 3  y mensajes de error del step form - */
function initStepForm() {
    $("#firstStep").addClass("active");
    $("#step2").hide();
    $("#step3").hide();
    $("#alias_user-error").hide();
    $("#street_user-error").hide();
    $("#num_ext_user-error").hide();
    $("#col_user-error").hide();
    $("#code_postal_user-error").hide();
    $("#mun_user-error").hide();
    $("#state_user-error").hide();
}

function changeup(e, existencia, cantidad) {
    $(".loader").show();

    if ((cantidad + 1) > existencia) {
        Swal.fire({
            title: 'Error',
            text: 'El producto no cuenta con esta existencia',
            icon: 'error',
            confirmButtonColor: '#b78b1e',
            confirmButtonText: 'Aceptar'
        });

        $(".loader").hide();
    }else{
        $.ajax({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            method: "POST",
            url: "changeup",
            dataType: "json",
            data: {
                position: e
            },
            success: function (e) {
                if (1 == e) {
                    location.reload();
                } else {
                    alert("No existe existencias");
                    location.reload();
                }
            }
        });
    }
}

function changedown(e) {
    $(".loader").show();

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        method: "POST",
        url: "changedown",
        dataType: "json",
        data: {
            position: e
        },
        success: function (e) {
            location.reload()
        }
    });
}

function editarAdress() {
    
    var email = $("#email").val();
    var protocol = window.location.protocol;
    host = window.location.hostname;
    port = window.location.port;
    url = protocol + "//" + host;

    if (port) {
        url = url + ":" + port;
    }

    url = url + "/usuario/cuenta/obtener-direccion";

    alert($('#direcciones').val());

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        method: "post",
        url: url,
        dataType: "json",
        data: {
            id_direccion: $('#direcciones').val(),
            origen: 1,
            email: email
        },
        success: function (e) {
            $('#save_address').show();
            $(".loader").hide();
            if (e.CALLE) {
                $("#alias_user").val(e.ALIAS);
                $("#pais_user").val(e.PAIS);
                
                $("#street_user").val(e.CALLE);
                $("#num_int_user").val(e.NUM_INT);
                $("#num_ext_user").val(e.NUM_EXT);
                $("#col_user").val(e.COLONIA);
                
                $("#code_postal_user").val(e.CODIGO);
                $("#state_user").val(e.ID_ESTADO);
                searchMunicipios();
                setTimeout(function () {
                    $("#mun_user").val(e.ID_MUNICIPIO);

                }, 400);
                $("#referencia").val(e.REFERENCIA);
            }
        },
        error: function () {}
    });
}

$("#guardar").on( 'change', function() {
    if( $(this).is(':checked') ) {
        // Hacer algo si el checkbox ha sido seleccionado
        $(this).val(1);
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $(this).val(0)
    }
});

$("input[name='radInline1_1']").on( 'change', function() {
    changeinput($(this).val());
});

function changeinput(value){
    if (value == 1) {
        $('#btn_finalizar').hide();
        $('#btn_finalizar').prop('disabled', false);
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", true );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#webpay" ).prop( "checked", false );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        payment = "PayPal";
    }else if(value == 2){
        $('#btn_finalizar').show();
        $('#btn_finalizar').prop('disabled', false);
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", true );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#webpay" ).prop( "checked", false );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        payment = "MercadoPago";
    }else if(value == 4){
        $('#btn_finalizar').hide();
        $('#fwebpay_url').show();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#webpay" ).prop( "checked", true );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        payment = "Webpay";
    }else if(value == 5){
        $('#btn_finalizar').show();
        $('#btn_finalizar').prop('disabled', true);
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#inputOpenPayTransf" ).prop( "checked", true );
        $( "#webpay" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        payment = "Transferencia";
    }else if(value == 6){
        $('#btn_finalizar').show();
        $('#btn_finalizar').prop('disabled', false);
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", true );
        $( "#webpay" ).prop( "checked", false );
        payment = "OpenPay";
    }else{
        $('#btn_finalizar').show();
        $('#btn_finalizar').prop('disabled', false);
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", true );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        $( "#webpay" ).prop( "checked", false );
        payment = "OpenPay";
    }

    gtag('event', 'set_checkout_option', {
        "checkout_step": 3,
        "checkout_option": value,
        "value": payment,
    });
}

function editarAdressRadio(dir) {

    var dirRadio = dir;

    var email = $("#email").val();
    var protocol = window.location.protocol;
    host = window.location.hostname;
    port = window.location.port;
    url = protocol + "//" + host;

    if (port) {
        url = url + ":" + port;
    }

    url = url + "/usuario/cuenta/obtener-direccion";

    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        method: "post",
        url: url,
        dataType: "json",
        data: {
            id_direccion: dirRadio,
            origen: 1,
            email: email
        },
        success: function (e) {

            $(".loader").hide();
            if (e.CALLE) {
                $("#alias_user").val(e.ALIAS);
                $("#pais_user").val(e.PAIS);
                
                $("#street_user").val(e.CALLE);
                $("#num_int_user").val(e.NUM_INT);
                $("#num_ext_user").val(e.NUM_EXT);
                $("#col_user").val(e.COLONIA);
                
                $("#code_postal_user").val(e.CODIGO);
                $("#state_user").val(e.ID_ESTADO);
                searchMunicipios();
                setTimeout(function () {
                    $("#mun_user").val(e.ID_MUNICIPIO);

                }, 400);
                $("#referencia").val(e.REFERENCIA);
            }
        },
        error: function () {}
    });
}

function veryfySales(url) {
    $.ajax({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        method: "POST",
        url: url,
        success: function (e) {
            var obj = jQuery.parseJSON( e );
            var error = false;
            
            if (obj['ventas'].NO_TICKET == null) {
                $("#step2").show();
                $(window).scrollTop(0); 
                $("#step3").hide();
                error = true;

                Swal.fire({
                    title: 'Error',
                    text: 'Error al reservar productos',
                    icon: 'error',
                    confirmButtonColor: '#b78b1e',
                    confirmButtonText: 'Aceptar'
                });
            }


            if (obj['productos'].length == 0) {
                $("#step2").show();
                $(window).scrollTop(0); 
                $("#step3").hide();
                error = true;

                Swal.fire({
                    title: 'Error',
                    text: 'Error al cargar los productos favor de intentar mas tarde',
                    icon: 'error',
                    confirmButtonColor: '#b78b1e',
                    confirmButtonText: 'Aceptar'
                });
            }

            if (!error) {
                secondStep();
            }
        },
        error: function( jqXHR, textStatus, errorThrown ) {
            $("#step2").show();
            $(window).scrollTop(0);
            $("#step3").hide();

            Swal.fire({
                title: 'Error',
                text: 'Error al cargar los productos favor de intentar mas tarde',
                icon: 'error',
                confirmButtonColor: '#b78b1e',
                confirmButtonText: 'Aceptar'
            });
        }
    });
}

function getRegalo(regalo) {
    var check = $('#regalo_check').prop('checked');
    var desc = $('input[name="monto_desc"]').val();
    var envio = $('#envios').text().replace('$', '');
    var total = $('#subtotal').text().replace(',', '');
    var regalo = parseFloat(regalo);
    var monto = 0;

    if (desc == '') {
        desc = 0;
    }

    if (check == true) {
        $('#regalo_div').css('display', 'flex');
        $('#regalo_div_ult').css('display', 'flex');
        $('input[name="regalo"]').val(regalo);
        $('.val_regalo').text(regalo.toFixed(2));
        monto = parseFloat(total) + parseFloat(regalo) + parseFloat(envio) - parseFloat(desc);

    } else {
        $('#regalo_div').hide();
        $('input[name="regalo"]').val(0);
        $('.val_regalo').text('0');

        monto = parseFloat(total) +  + parseFloat(envio) - parseFloat(desc);
    }

    $('.total').text(( Math.round(monto * 100) / 100 ).toFixed(2) );

    this.setMulettaCoins();
}

/*toggle datos del formulario para tarjetas radio*/
$("input[name='radGroup2_1']").change(function () {
    esconderFormularioDir();
    selected_value = $("input[name='radGroup2_1']:checked").val();

    editarAdressRadio(selected_value);
});

/*toggle el fondo de la tarjeta en direccions multiples radio*/
$(document).delegate("input[name='radGroup2_1']", 'change', function () {
    $(this).closest("a").hide();
    $("div.muletta-gear-bg").removeClass('muletta-gear-bg');
    $(this).closest("div").toggleClass('muletta-gear-bg');

});

/*Resetea campos del formulario*/
function resetDirFields() {
    $("#street_user").val(null);
    $("#num_int_user").val(null);
    $("#num_ext_user").val(null);
    $("#col_user").val(null);
    $("#code_postal_user").val(null);
    $("#mun_user").val(null);
    $("#state_user").val(null);
}

/*Esconde elementos del formulario y muestra el boton proceder a pago - al cambiar estado del dropDown*/
$('#direcciones').change(function () {
    resetDirFields();
    $('#1-row-dir').show();
    $('#2-row-dir').show();
    $('#3-row-dir').show();
    $('#btnSecondStep').show();
});

/*Esconde y muestra elementos al dar clic en boton AGREGAR DIRECCION*/
$('#edit_dir').click(function () {

    $('#dir-card-1').hide();
    $('#select-dir').hide();
    $('#btn_nuevo').hide();
    mostrarFormularioDir();

    $('#form-new-dir').show();
    $('#save_address').show();
    $('#btnSecondStep').show();

});

/*Esconde y muestra elementos al dar clic en boton AGREGAR DIRECCION*/
$('#btn_nuevo').click(function () {
    $('#dir-card-1').hide();
    resetDirFields();
    $("#alias_user").val(null);
    $("#pais_user").val(null);

    $('#select-dir').hide();
    $('#btn_nuevo').hide();
    mostrarFormularioDir();

    $('#form-new-dir').show();
    $('#save_address').show();
    $('#btnSecondStep').show();

});

/*Esconde y muestra elementos al dar clic en boton USAR DIRECCION GUARDADA*/
$('#btn_saved_dir').click(function () {
    resetDirFields();
    $('#btn_nuevo').show();
    $('#btn_saved_dir').hide();

    $('#0-row-dir').hide();
    $('#1-row-dir').hide();
    $('#2-row-dir').hide();
    $('#3-row-dir').hide();


    $('#guardar_address').hide();
    $('#select-dir').show();
    $('#btnSecondStep').show();

});

/*Boton Paso 1 - PROCESAR COMPRA - Esconde primer pestaña y muestra segunda pestaña*/
$("#procesar").on("click", function () {
    var error = false;
    var total = parseFloat($('#total').text().replace('$', ''));

    $('.existencia').each(function(){
        if ($(this).attr("value") < 0) {
            error = true;   
        }
    });

    if (!error) {
        
        if (total < 0) {
            Swal.fire({
                title: 'Error',
                text: 'El total no puede ser negativo favor de agregar mas productos',
                icon: 'error',
                confirmButtonColor: '#b78b1e',
                confirmButtonText: 'Aceptar'
            });
        }else{
            $("#secondStep").addClass("active");
            $("#firstStep").removeClass("active");
            $("#firstStep").addClass("g-checked");
            $("#step1").fadeOut(200);
            $("#step2").fadeIn(200);
            $(window).scrollTop(0);

            selected_value = $("input[name='radGroup2_1']:checked").val();
            if(selected_value){
                
                editarAdressRadio(selected_value);

            }

            trackingMetadata = getCartTrackingMetadata('Checkout Cart Shipping');
            gtag('event', 'checkout_progress', {
                "items": trackingMetadata.google_items,
            });

            fbq("trackCustom", "Datos de envio en checkout");
        }
    }else{
        Swal.fire({
            title: 'Error',
            text: 'Existen productos que no tienen existencia',
            icon: 'error',
            confirmButtonColor: '#b78b1e',
            confirmButtonText: 'Aceptar'
        });
    }
});

/*Boton Paso 2 - PROCEDER A PAGAR - Esconde segunda pestaña y muestra tercer pestaña*/
function secondStep() {

    gtag("event", "payment-loaded", {
        "event_category" : "checkout",
        "event_label" : "Pago en checkout"
    });
    fbq("trackCustom", "Pago en checkout");

    /*Revisamos por campos obligatorios sino mostramos campo de error con jquery*/
    if ("" == $("#street_user").val()) {
        $("#form_street_user").addClass("u-has-error-v1");
        $("#street_user-error").show();
    } else {
        $("#form_street_user").removeClass("u-has-error-v1");
        $("#street_user-error").hide();
    }

    if ("" == $("#num_ext_user").val()) {
        $("#form_num_ext_user").addClass("u-has-error-v1");
        $("#num_ext_user-error").show();
    } else {
        $("#form_num_ext_user").removeClass("u-has-error-v1");
        $("#num_ext_user-error").hide();
    }

    if ("" == $("#col_user").val()) {
        $("#form_col_user").addClass("u-has-error-v1");
        $("#col_user-error").show();
    } else {
        $("#form_col_user").removeClass("u-has-error-v1");
        $("#col_user-error").hide();
    }

    if ("" == $("#code_postal_user").val()) {
        $("#form_code_postal_user").addClass("u-has-error-v1");
        $("#code_postal_user-error").show();
    } else {
        $("#form_code_postal_user").removeClass("u-has-error-v1");
        $("#code_postal_user-error").hide();
    }

    if ("" == $("#mun_user").val()) {
        $("#form_mun_user").addClass("u-has-error-v1");
        $("#mun_user-error").show();
    } else {
        $("#form_mun_user").removeClass("u-has-error-v1");
        $("#mun_user-error").hide();
    }

    if ("" == $("#state_user").val()) {
        $("#form_state_user").addClass("u-has-error-v1");
        $("#state_user-error").show();
    } else {
        $("#form_state_user").removeClass("u-has-error-v1");
        $("#state_user-error").hide();
    }

    if ("" == $("#pais_user").val()) {
        $("#form_pais_user").addClass("u-has-error-v1");
        $("#pais_user-error").show();
    } else {
        $("#form_pais_user").removeClass("u-has-error-v1");
        $("#pais_user-error").hide();
    }

    if ("" == $("#alias_user").val()) {
        $("#form_alias_user").addClass("u-has-error-v1");
        $("#alias_user-error").show();
    } else {
        $("#form_alias_user").removeClass("u-has-error-v1");
        $("#alias_user-error").hide();
    }

    if ("" == $("#referencia").val()) {
        $("#form_referencia").addClass("u-has-error-v1");
        $("#referencia-error").show();
    } else {
        $("#form_referencia").removeClass("u-has-error-v1");
        $("#referencia-error").hide();
    }

    /*Evita que avance a menos de que estos campos esten llenos*/


    if ("" == $("#street_user").val() || "" == $("#num_ext_user").val() || "" == $("#col_user").val() || "" == $("#code_postal_user").val() || "" == $("#mun_user").val() || "" == $("#state_user").val() || "" == $('#referencia').val()) {
        Swal.fire({
            title: 'No cuenta con direcciones',
            html: 'Favor de agregar una nueva direcci&oacute;n',
            icon: 'error',
            confirmButtonColor: '#b78b1e',
            confirmButtonText: 'Aceptar'
        });

        return !1;
    }

    /*Esconder campos de 2nd pestaña y mostrar campos de tercer pestaña*/
    $("#finaltStep").addClass("active");
    $("#secondStep").removeClass("active");
    $("#secondStep").addClass("g-checked");
    $("#step2").fadeOut(200);
    $(window).scrollTop(0);
    $("#step3").fadeIn(200);
    
    initPaypal();
}

$('#btn_desc').click(function () {
    var promo = $('#txt_desc').val();
    var total = $('#subtotal').text().replace(',', '');
    var envio = $('#envios').text().replace('$', '');
    var regalo = parseFloat($('#regalo_input').val());
    $('.loader').show();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: "getDesc",
        dataType: 'json',
        data: {
            promocion: promo
        },
        success: function (msg) {
            var datos = msg;            
            
            $('#errorCodigo').hide();
            if (datos.length != 0) {
                // $('#id_desc').val(datos.ID_DESCUENTOS);
                $('input[name="id_desc"]').val(datos.ID_DESCUENTOS);
                $('#row_descuentos').hide();
                $('#descuentos_success').show();
                // Descuentos en moneda
                if (datos.TIPO_DESCUENTO == 1) {
                    $('.val_desc').text(parseFloat(datos.VALOR_DESCUENTO).toFixed(2));
                    $('#monto_desc').val(datos.VALOR_DESCUENTO);
                    $('.total').text( parseFloat(parseFloat(total) + parseFloat(regalo) + parseFloat(envio) - parseFloat(datos.VALOR_DESCUENTO) ).toFixed(2) );
                }
                // Descuentos en porcentaje
                else {
                    var monto = (parseFloat(total) * datos.VALOR_DESCUENTO) / 100
                    $('.val_desc').text(monto.toFixed(2));
                    // $('#monto_desc').val(monto);
                    $('input[name="monto_desc"]').val(monto);
                    var num = parseFloat(total) + parseFloat(regalo) + parseFloat(envio) - parseFloat(monto);
                    $('.total').text( parseFloat(Math.round(num * 100) / 100).toFixed(2) );
                }

                trackingMetadata = getCartTrackingMetadata('Coupon added');
                gtag('event', 'checkout_progress', {
                    "items": trackingMetadata.google_items,
                    "coupon": promo
                });
                fbq('trackCustom', 'ShareDiscount', {promotion: promo});
            } else {
                $('#errorCodigo').show();
                gtag("event", "coupon-failed", {
                    "event_category" : "coupon",
                    "event_label" : "Canje de cupón fallido"
                });
                fbq("trackCustom", "Canje de cupón fallido");
            }

            setMulettaCoins();
            $('.loader').hide();
        }
    });
});

$('#procesar').click(function () {
    var url = $('#urlVerificar').val();
    var ID = $('#id_user').val();
    if (ID == '' || ID == null) {
        location.href = url;
        gtag("event", "user-not-auth", {
            "event_category" : "checkout",
            "event_label" : "Usuario no autenticado en checkout"
        });
        fbq("trackCustom", "Usuario no autenticado en checkout");
    }
});

function formSubmit(form) {
    $('.loader').show();
    $('#btn_finalizar').attr("disabled", true);
    
    var ID_CLIENTE = $('input[name="id_user"]').val();
    var nombre = $('#nombre').val();
    var telefono = $('#tel_user').val();
    var EMAIL = $('#email').val();
    var street_user = $('input[name="street_user"]').val();
    var num_int_user = $('input[name="num_int_user"]').val();
    var num_ext_user = $('input[name="num_ext_user"]').val();
    var col_user = $('input[name="col_user"]').val();
    var code_postal_user = $('input[name="code_postal_user"]').val();
    var mun_user = $('[name="mun_user"]').val();
    var state_user = $('[name="state_user"]').val();
    var id_desc = $('input[name="id_desc"]').val();
    var monto_desc = $('input[name="monto_desc"]').val();
    var monto_coins = $('input[name="coins"]').val();
    var regalo = $('input[name="regalo"]').val();
    var alias = $('input[name="alias_user"]').val();
    var referencia = $('textarea[name="referencia"]').val();
    var guardar = $('input[name="guardar"]').val();

    var total = $('#total').text().replace(',', '').replace('$', '');

    var metodo_pago = $("input[name='radInline1_1']:checked").val();
    var metodo = 0;

    if (metodo_pago == 2) {
        metodo = 13;
    }else if(metodo_pago == 3){
        metodo = 18;
    }else if(metodo_pago == 5){
        metodo = 24;
    }else if(metodo_pago == 6){
        metodo = 18;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: "CompletarPago",
        dataType: 'json',
        data: {
            ID_CLIENTE: ID_CLIENTE,
            EMAIL: EMAIL,
            NOMBRE: nombre,
            TELEFONO: telefono,
            street_user: street_user,
            num_int_user: num_int_user,
            num_ext_user: num_ext_user,
            col_user: col_user,
            code_postal_user: code_postal_user,
            mun_user: mun_user,
            state_user: state_user,
            id_desc: id_desc,
            monto_desc: monto_desc,
            regalo: regalo,
            alias: alias,
            referencia: referencia,
            guardar: guardar,
            metodo_pago: metodo,
            coins: monto_coins
        },
        success: function(order){
            
            $('.loader').hide();
            if (order.id_venta) {
                $('#id_venta').val(order.id_venta);
                
                if (total <= 0) {            
                    $('.loader').hide();
                    var protocol = window.location.protocol;
                    host = window.location.hostname;
                    port = window.location.port;
                    url = protocol + "//" + host;

                    if (port) {
                        url = url + ":" + port;
                    }
                
                    url = url + "/compras/muletta-pago/"+order.id_venta;

                    window.location.href = url;
                }else{
                    
                    if (metodo_pago == 2) {
                        form.submit();
                    }else if(metodo_pago == 3){
                        var protocol = window.location.protocol;
                        host = window.location.hostname;
                        port = window.location.port;
                        url = protocol + "//" + host;

                        if (port) {
                            url = url + ":" + port;
                        }

                        url = url + "/compras/PagoTarjeta/"+order.id_venta;

                        window.location.href = url;
                    }else if(metodo_pago == 5){
                        var protocol = window.location.protocol;
                        host = window.location.hostname;
                        port = window.location.port;
                        url = protocol + "//" + host;

                        if (port) {
                            url = url + ":" + port;
                        }

                        url = url + "/usuario/cuenta/pedidos";

                        window.location.href = url;
                    }else if(metodo_pago == 6){
                        var protocol = window.location.protocol;
                        host = window.location.hostname;
                        port = window.location.port;
                        url = protocol + "//" + host;

                        if (port) {
                            url = url + ":" + port;
                        }

                        url = url + "/compras/paynetPago/"+order.id_venta;

                        window.location.href = url;
                    }
                }
            }else{
                Swal.fire({
                    title: 'Error',
                    text: 'Error al procesar la compra favor de intentar mas tarde',
                    icon: 'error',
                    confirmButtonColor: '#b78b1e',
                    confirmButtonText: 'Aceptar'
                });
            }
        }
    });

    return false;
    
}

function getCartTrackingMetadata(list_name){
    var google_items = [], fb_content_ids = [], fb_contents = [];

    $(".product-cart-row").each(function () {

        var product_id = $(this).find('.product-cart-id').text();
        product_id = product_id.substring(0, product_id.indexOf(' '));
        var name = $(this).find('.product-cart-name').text();
        var list_name = list_name;
        var category = $(this).find('.product-cart-category').text();
        var variant = $(this).find('.product-cart-size').text();
        var quantity = parseInt( $(this).find('.product-cart-quantity').val() );
        var price = parseFloat( $(this).find('.product-cart-price').text() ) ;
        google_items.push({
            "id": product_id,
            "name":name,
            "list_name": list_name,
            "category": category,
            "variant": variant,
            "quantity": quantity,
            "price": price,
        });

        fb_content_ids.push(product_id);

        fb_contents.push({
            "id": product_id,
            "quantity": quantity,
            "name":name,
            "variant": variant,
            "price": price,
        });

    });

    return {"google_items": google_items, "fb_content_ids": fb_content_ids, "fb_contents": fb_contents};
}


function setMulettaCoins(){
    var checked = $('#money_check').is(':checked');
    var cantidad = $('#money_check').val();
    var subtotal = $('#subtotal').text().replace(',', '');
    var desc = $('input[name="monto_desc"]').val();
    var envio = parseFloat($('#envios').text().replace('$', ''));
    var regalo = parseFloat($('#regalo_input').val());

    subtotal = subtotal.replace('$', '').replace(',', '');

    var total_final = 0;
    var total  = 0;
    
    if (checked == true && cantidad != '') {
        if (cantidad <= (subtotal - desc)) {
            total_final = cantidad;
        }else{
            total_final = subtotal - desc;
        }
        total = (subtotal - desc) - total_final + regalo + envio
    }else{
        total = (subtotal - desc) + regalo + envio
    }

    $('.val_coins').text(parseFloat(total_final).toFixed(2));
    $('input[name="coins"]').val(parseFloat(total_final).toFixed(2));

    $('.total').text(parseFloat(total).toFixed(2));
}

//Crea los ID de refencia y dirección
$("#fwebpay_url").click(function () {
    //Obtiene los parametro para poder generar el ID de ventas y el ID de Dir.
    var id_cliente = $('input[name="id_user"]').val();
    var email = $('input[name="email"]').val();
    var subtotal = $('input[name="subtotal"]').val();
    var importe = $('input[name="input_total"]').val();
    var envio = $('#costo_envio').val();

    jQuery.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "verifySales/" + id_cliente + "/" + email + "/" + subtotal + "/" + importe + "/" + envio,
        type: 'POST',
        contentType: "text/xml; charset=utf-8",
        dataType: "text",
        error: function (xhr, textStatus, errorThrown) {
            xhr + ' ' + textStatus + '' + errorThrown;
        },
        success: function (response, status, xmlData) {
            var venta = JSON.parse(response).ventas.ID_VENTA;
            var direc = $("input:radio[name=radGroup2_1]:checked").val();
            //Una vez genedara pasa los paramatreos para generar la URL de Webpay
            fwebpay_url(venta, direc);
        }
    });

});

function fwebpay_url(id_venta, id_direccion) {
    //Obtiene los parametros de ID Ventas y Dirección
    jQuery.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/compras/webpay_url",
        type: 'GET',
        contentType: "text/xml; charset=utf-8",
        dataType: "text",
        data: {
            id_venta: id_venta,
            id_direccion: id_direccion
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(xhr + ' ' + textStatus + '' + errorThrown);
        },
        success: function (response, status, xmlData) {
            //Una vez mandado los parametros se mandan a para procesar la URL
            $(response).find("nb_url").each(function () {
                //Una vez generada la procesa y la manda a una pestaña nueva
                window.open($(this).text(), "_self");
            });
        }
    });
    
}

$(".winCerrar").click(function () {
    const querystring = window.location.search
    const params = new URLSearchParams(querystring)
    window.open("/compras/checkout?" + params, "_self")
});

$("#cerrarCart").click(function () {
    const querystring = window.location.search
    const params = new URLSearchParams(querystring)
    window.open("/compras/clearCart?" + params, "_self")
});

// $("#procesar").click(function () {
//     document.querySelector("input[name='radGroup2_1']").checked = true;
// });

function fwebpay_order(form) {
    $('.loader').show();
    var ID_CLIENTE = $('input[name="id_user"]').val();
    var nombre = $('#nombre').val();
    var telefono = $('#tel_user').val();
    var EMAIL = $('#email').val();
    var street_user = $('input[name="street_user"]').val();
    var num_int_user = $('input[name="num_int_user"]').val();
    var num_ext_user = $('input[name="num_ext_user"]').val();
    var col_user = $('input[name="col_user"]').val();
    var code_postal_user = $('input[name="code_postal_user"]').val();
    var mun_user = $('[name="mun_user"]').val();
    var state_user = $('[name="state_user"]').val();
    var id_desc = $('input[name="id_desc"]').val();
    var monto_desc = $('input[name="monto_desc"]').val();
    var monto_coins = $('input[name="coins"]').val();
    var regalo = $('input[name="regalo"]').val();
    var alias = $('input[name="alias_user"]').val();
    var referencia = $('textarea[name="referencia"]').val();
    var guardar = $('input[name="guardar"]').val();
    var total = $('#total').text().replace(',', '').replace('$', '');
    var metodo_pago = $("input[name='radInline1_1']:checked").val();
    
    jQuery.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "CompletarPagoWebpay",
        type: 'POST',
        dataType: 'json',
        data: {
            ID_CLIENTE: ID_CLIENTE,
            EMAIL: EMAIL,
            NOMBRE: nombre,
            TELEFONO: telefono,
            street_user: street_user,
            num_int_user: num_int_user,
            num_ext_user: num_ext_user,
            col_user: col_user,
            code_postal_user: code_postal_user,
            mun_user: mun_user,
            state_user: state_user,
            id_desc: id_desc,
            monto_desc: monto_desc,
            regalo: regalo,
            alias: alias,
            referencia: referencia,
            guardar: guardar,
            metodo_pago: metodo_pago,
            coins: monto_coins
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(xhr + ' ' + textStatus + '' + errorThrown);
        },
        success: function (response, status, xmlData) {
            alert(response);
        }
    });
}

function closeModal(){
    $('#error-modal').modal('hide');
    window.location.href = $('#url_pedidos').text();
}

function reintentarPago(){
    $('#error-modal').modal('hide');
    var id_venta = $('#id_venta').val();
    var url = $('#url_reintentar_pago').val().replace('cambio_venta', id_venta);
    window.location.href = url;
}

function initPaypal(){

    var monto_desc = $('input[name="monto_desc"]').val();
    var total = $('#subtotal').text().replace(',', '');
    var envio = $('#envios').text().replace('$', '');
    var regalo = $('#regalo_input').val();

    if (!monto_desc) {
        monto_desc = 0;
    }

    paypal.Buttons({
        env: 'sandbox',
        createOrder: function(data, actions) {
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: parseFloat(total) + parseFloat(regalo) + parseFloat(envio) - parseFloat(monto_desc)
                    }
                }]
            });
        },
        onApprove: function(data, actions) {
            $('.loader').show();

            return actions.order.capture().then(function(details) {
                completarPago(data);
            });
        }
    }).render('#paypal-button-container');
}

function completarPago(details) {
    
    var ID_CLIENTE = $('input[name="id_user"]').val();
    var EMAIL = $('#email').val();
    var street_user = $('input[name="street_user"]').val();
    var num_int_user = $('input[name="num_int_user"]').val();
    var num_ext_user = $('input[name="num_ext_user"]').val();
    var col_user = $('input[name="col_user"]').val();
    var code_postal_user = $('input[name="code_postal_user"]').val();
    var mun_user = $('[name="mun_user"]').val();
    var state_user = $('[name="state_user"]').val();
    var id_desc = $('input[name="id_desc"]').val();
    var monto_desc = $('input[name="monto_desc"]').val();
    var regalo = $('input[name="regalo"]').val();
    var alias = $('input[name="alias"]').val();
    var guardar = $('input[name="guardar"]').val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: "CompletarPago",
        dataType: 'json',
        data: {
            ID_CLIENTE: ID_CLIENTE,
            EMAIL: EMAIL,
            street_user: street_user,
            num_int_user: num_int_user,
            num_ext_user: num_ext_user,
            col_user: col_user,
            code_postal_user: code_postal_user,
            mun_user: mun_user,
            state_user: state_user,
            id_desc: id_desc,
            monto_desc: monto_desc,
            regalo: regalo,
            alias: alias,
            guardar: guardar,
            metodo_pago: 14
        },
        success: function(msg){
            if (msg.id_venta) {
                $('#id_venta').val(msg.id_venta);
                
                setTimeout(() => {
                    finalizarPagoPaypal(details);
                }, 500);
            }else{
                Swal.fire({
                    title: 'Error',
                    text: 'Error al procesar la compra favor de intentar mas tarde',
                    icon: 'error',
                    confirmButtonColor: '#b78b1e',
                    confirmButtonText: 'Aceptar'
                });
            }
        }
    });
}

function finalizarPagoPaypal(data){

    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol + "//" + host;

    if (port) {
        url = url + ":" + port
    }
    
    var id_venta = $('#id_venta').val();
    var id_user = $('input[name="id_user"]').val();
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url + "/compras/paypalOrder/"+id_user+"/"+id_venta,
        dataType: 'json',
        data: { collection_id: data.orderID },
        success: function( msg ) {
            $('.loader').hide();
            setTimeout(() => {
                window.location.href = url + "/compras/detailsVenta/"+msg;
            }, 500);
        },
        error: function( jqXHR, textStatus, errorThrown ) {
            console.log( 'jqXHR: ', jqXHR );
            console.log( 'textStatus: ', textStatus );
            console.log( errorThrown );
            //window.location.href = url + "usuario/cuenta/pedidos";
        }
    });
}

function searchMunicipios(){
    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    var estado = $('#state_user').val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url + '/municipios',
        data: {
            ID_ESTADO: estado
        },
        dataType: 'json',
        success: function( msg ) {
            $('#div_municipios').show();
            
            var opciones = '';
            msg.forEach(element => {

                opciones = opciones + '<option value="'+element.ID_MUNICIPIO+'"> '+element.DESCRIPCION_MUNICIPIO+' </option>';
                
            });

            $('select[name="mun_user"]').empty();
            $('select[name="mun_user"]').append(opciones);
        }
    });
}

$("#SubirArchivo").on("click", function(e){
    e.preventDefault();
    $('.loader').show();
    // var f = $("#formuploadajax");
    var pictureInput = document.getElementById("archivo1");

    var formData = new FormData();
    formData.append('file', pictureInput.files[0]);
    // formData.append("dato", "valor");
    //formData.append(f.attr("name"), $(this)[0].files[0]);
    var usuario = $("#id_usuario").val();

    var port = window.location.port;
    var url;
    if (port) {
        url = "http://localhost:82/subirComprobanteTransferencia/"+usuario;
    }else{
        url = "https://prueba.asaiint.com/subirComprobanteTransferencia/"+usuario;
    }
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .success(function(res){
        $("#mensaje").html(res);
        $('#btn_finalizar').prop('disabled', false);
        $('.loader').hide();
    });
    // ... resto del código de mi ejercicio
});

// EMPIZA CODIGO DE TRANSFERENCIAS
'use strict';
document.getElementById('canvas').style.display = "none";
const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const photo = document.querySelector('#photo');
const startbutton  = document.querySelector('#startbutton');
const cameraOptions = document.querySelector('#select-camara');

const contraints = {
    audio: false,
    video: {
        width: 1024,
        height:700
    }
};

async function getDevices(){
    const devices = await navigator.mediaDevices.enumerateDevices();
    const videoDevices = devices.filter(device => device.kind === 'videoinput');
    const options = videoDevices.map(videoDevice => {
        return `<option value="${videoDevice.deviceId}">${videoDevice.label}</option>`;
    });
    
    cameraOptions.innerHTML = options.join('');
}

async function cambiarCamara(){
    alert("cambio de camara");
    const updatedConstraints = {
        ...contraints,
        deviceId: {
            exact: cameraOptions.value
        }
    };
    
    video.srcObject.getTracks().forEach(track => track.stop())

    setTimeout(() => {
        iniciarCamara(updatedConstraints);
    }, 1000);
}

function abrirCamara(){
    $("#camara-modal").modal('show');

    try{
        iniciarCamara(contraints);
    }catch(e){
        $("#mensaje2").html("Debe se seleccionar un dispositivo y permitir el acceso al navegador. Precione F5 o Refresh en el navegador <br clear='all'><br clear='all'>");
    }
}

async function iniciarCamara(contraints){
    const stream = await navigator.mediaDevices.getUserMedia(contraints);
    hadleSuccess(stream);
    var context = canvas.getContext('2d');
}

function takepicture() {
    canvas.width = 730;
    canvas.height = 500;
    canvas.getContext('2d').drawImage(video, 0, 0, 730, 500);
    var data = canvas.toDataURL('image/png');
    // photo.setAttribute('src', data);

    document.getElementById('canvas').style.display = "block";
    $("#div-video-envivo").hide();
    $("#div-imagen-tomada").show();
}

function dataURLtoFile(dataurl, filename) {
 
    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
        
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    return new File([u8arr], filename, {type:mime});
}

document.querySelector('#save_image_cam').addEventListener('click', function(ev){
    $('.loader').show();
    var data = canvas.toDataURL('image/png');
    var info = data.split(",", 2);
    var usuario = $("#id_usuario").val();
    var nombre = `comprobante-${usuario}.png`;

    var file = dataURLtoFile(data, nombre);
    console.log(file);

    var formData = new FormData();
    formData.append('file', file);
    // formData.append("dato", "valor");
    //formData.append(f.attr("name"), $(this)[0].files[0]);
    var usuario = $("#id_usuario").val();

    var port = window.location.port;
    var url;
    if (port) {
        url = "http://localhost:82/subirComprobanteTransferencia/"+usuario;
    }else{
        url = "https://prueba.asaiint.com/subirComprobanteTransferencia/"+usuario;
    }
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .success(function(res){
        $("#mensaje").html(res);
        $('#btn_finalizar').prop('disabled', false);
        $("#camara-modal").modal('hide');
        $('.loader').hide();
    });

    ev.preventDefault();
}, false);

$("#camara-modal").on('hide.bs.modal', function(){
    video.srcObject.getTracks().forEach(track => track.stop())
});

startbutton.addEventListener('click', function(ev){
    takepicture();
    ev.preventDefault();
}, false);

document.querySelector('#archivo1').addEventListener('change', function(ev){
    setTimeout(() => {
        $("#SubirArchivo").click();
    }, 1000);
}, false);

document.querySelector('#cancel_image_cam').addEventListener('click', function(ev){
    $("#div-video-envivo").show();
    $("#div-imagen-tomada").hide();
    ev.preventDefault();
}, false);

function hadleSuccess(stream){
    window.stream = stream;
    video.srcObject = stream;
}