<?php

namespace App\Http\Middleware;

use App\Http\Controllers\UsersController;

use Closure;

class UsersMiddleware
{
    protected $usersController;

    public function __construct(UsersController $usersController) {
        $this->usersController = $usersController;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if (session()->has('perfilUsuario')){
            $perfil_usuario = session()->get('perfilUsuario');
            foreach ($perfil_usuario as $usuario) {
                $this->usersController->getVirtualMoney($usuario->ID_CLIENTE);

                if (print_r($usuario->NOMBRE, true) == null) {
                    return redirect('/');
                }
            }
        }else{
            return redirect('/usuario/login');
        }

        return $next($request);
    }
}
