<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Orders;
use App\Repositories\Products;
use Barryvdh\DomPDF\Facade as PDF;

class PdfController extends Controller
{
    protected $orders;
    protected $products;

    public function __construct(Orders $orders, Products $products){
        $this->orders = $orders;
        $this->products = $products;
    }

    public function getGenerar($id_venta)
    {
        return $this->pdf($id_venta);
    }

    public function pdf($id_venta){
        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $devolucion = $this->orders->getDevolucion($ordenes->ID_DEVOLUCION_WEB);
        $productos = $this->orders->obtenerProductosDevolucion($ordenes->ID_DEVOLUCION_WEB);

        $producto_final = [];
        $producto_anterior = '';
        foreach ($productos as $producto){
            if ($producto->ID_MODELO != $producto_anterior) {
                $variantes_productos = $this->orders->obtenerProductosDevolucionPDF($ordenes->ID_DEVOLUCION_WEB, $producto->ID_MODELO);
                $producto_final[] = $variantes_productos;
                $producto_anterior = $producto->ID_MODELO; 
            }
        }

        $pdf = app('dompdf.wrapper');
        $html = view('producto.pdf-generar', compact('id_venta', 'ordenes', 'producto_final', 'devolucion'));

        // return $html->render();
        return PDF::loadHTML($html)->stream('archivo.pdf');
    }
}
