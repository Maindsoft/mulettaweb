<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\MessageLogin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Repositories\Users;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{

    protected $users;

    public function __construct(Users $users){
        $this->middleware('guest');
        $this->users = $users;

    }

    public function showLoginForm($id_compra = 0){
        $perfil_usuario = session()->get('perfilUsuario');
        if ($perfil_usuario == NULL) {
            return view('usuario.login', ['id_compra' => $id_compra]);
        }else{
            return redirect()->route('mi-cuenta');
        }

    }

    public function login($id_compra){
        $mensaje_error = "No encontrado";
        
        $credenciales = $this->validate(request(), [
            'email' => 'email|required|string',
            'contrasena' => 'required|string',
        ]);

        if ($credenciales = true) {
            $email = Input::get('email');
            $contrasena = Input::get('contrasena');
            $usuario = $this->users->getClientAccessNormal($email, $contrasena);
            if (empty($usuario)) {
                return back()
                    ->withErrors(['email' => trans('Usuario no encontrado. Por favor regístrate en nuestro sistema o verifica tus datos.')])
                    ->withInput(request(['email']));
            }
            session(['perfilUsuario' => compact('usuario')]);
            $perfil_usuario = session('perfilUsuario');

            //Enviar e-mail
            //Mail::to($email)->queue(new MessageLogin(compact('usuario')));

            if ($id_compra != '0'){
                return redirect()->route('compras');
            }else{
                return redirect()->route('index');
            }
        }

        return back()
            ->withErrors(['email' => trans('auth.failed')])
            ->withInput(request(['email']));
    }

    public function logout(){
        if (session()->has('perfilUsuario')) {
            session()->forget('perfilUsuario');
        }
        if (session()->has('virtualMoney')) {
            session()->forget('virtualMoney');
        }
        return redirect('/');
    }

    /**
     * Redirect the user to the google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(){
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(){
        $mensaje_error = "NO ENCONTRADO";
        $user = Socialite::driver('google')->stateless()->user();
        $email = $user->email;
        $id_plataforma = $user->id;
        $url_imagen = $user->avatar;
        $nombre = $user->name;
        $id_tipo_registro = 3;
        $access_token = null;
        $usuario = $this->users->getClientAccessRs($nombre,$email, $id_tipo_registro, $id_plataforma, $access_token, $url_imagen);

        if ($email){
            session(['perfilUsuario' => compact('usuario')]);
            $perfil_usuario = session('perfilUsuario');
            //dd($perfil_usuario);
            return redirect()->route('mi-cuenta');
        }else{
            return redirect(route('loginForm'))
                ->withErrors(['email' => trans('Hubo un fallo en el sistema, intenta más tarde.')])
                ->withInput(request(['email']));
        }

    }

    /**
     * Redirect the user to the google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProviderFacebook(){
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallbackFacebook(){
        $mensaje_error = "NO ENCONTRADO";
        $user = Socialite::driver('facebook')->stateless()->user();
        $email = $user->email;
        $id_plataforma = $user->id;
        $url_imagen = $user->avatar;
        $nombre = $user->name;
        $id_tipo_registro = 2;
        $access_token = null;
        $usuario = $this->users->getClientAccessRs($nombre,$email, $id_tipo_registro, $id_plataforma, $access_token, $url_imagen);
        if ($email){
            session(['perfilUsuario' => compact('usuario')]);
            $perfil_usuario = session('perfilUsuario');
            //dd($perfil_usuario);
            return redirect()->route('mi-cuenta');
        }else{
            return redirect(route('loginForm'))
                ->withErrors(['email' => trans('Hubo un fallo en el sistema, intenta más tarde.')])
                ->withInput(request(['email']));
        }

    }


}
