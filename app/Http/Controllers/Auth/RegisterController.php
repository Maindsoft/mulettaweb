<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use RealRashid\SweetAlert\Facades\Alert;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['nombre'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    protected $users;

    public function __construct(Users $users)

    {
        $this->middleware('guest');
        $this->users = $users;

    }


    public function showRegisterForm()
    {
        return view('usuario.registro');
    }


    public function registerUser(Request $request){

        $nombre = $request->nombre_cliente;
        $telefono = $request->telefono;
        $email = $request->correo_cliente;
        
        $usuario = $this->users->insertClientSale($nombre,$telefono,$email);

        return json_encode($usuario);
    }

    public function register()
    {
        $credenciales = $this->validate(request(),[
            'nombre' => 'required',
            'email' => 'email|required|string',
            'contrasena' => 'required|string|min:8',
        ]);

        if ($credenciales = true){
            $nombre = Input::get('nombre');
            $apellidoP = Input::get('apellidoP');
            $apellidoM = Input::get('apellidoM');
            $telefono = Input::get('telefono');
            $email = Input::get('email');
            if ((Input::get('año') && Input::get('mes') && Input::get('dia')) && (Input::get('año') != '' && Input::get('mes') != '' && Input::get('dia') != '')) {
                $fecha_nacimiento =  Input::get('año').'-'.Input::get('mes').'-'.Input::get('dia');
            }else{
                $fecha_nacimiento = '';
            }
            $contrasena = Input::get('contrasena');
            $usuario = $this->users->insertClientNormal($nombre, $apellidoP, $apellidoM, $telefono,$email,$fecha_nacimiento,$contrasena);

            if ($usuario && isset($usuario[0]->EXISTE)) {
                return back()->with('warning', 'Este usuario ya ha sido registrado');
            }else{
                $usuario = $usuario[0];

                session(['perfilUsuario' => compact('usuario')]);
                $perfil_usuario = session('perfilUsuario');
                $carrito = session('carrito');

                if ($carrito) {
                    return redirect()->route('compras');
                }else{
                    return redirect('/');
                }
            }
        }

        return back()->with('info', 'Error al registrar usuario');
    }
}
