<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 8/21/2019
 * Time: 5:02 PM
 */

namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class Products
{

    //Creamos un constructor de la clase cliente
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getProductCategories()
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getProductCategories',
            ["json" => ['ORIGEN' => '1']]);


        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $categorias_productos = json_decode($response->getBody()->getContents());


        return $categorias_productos;

    }

    public function getMenuPrimerNivel()
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getMenuPrincipal',
        ["json" => ['ORIGEN' => '1']]);
            
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $menuPrimerNivel = json_decode($response->getBody()->getContents());

        return $menuPrimerNivel;

    }

    public function getMenuSubNivel()
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getMenuSubNivel',
            ["json" => ['ORIGEN' => '1']]);


        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $menuSubNivel = json_decode($response->getBody()->getContents());

        
        return $menuSubNivel;

    }

    
    public function getFooterContent($id)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getFooterContent',
            ["json" => ['ID_PADRE' => $id]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $menuSubNivel = json_decode($response->getBody()->getContents());
        
        return $menuSubNivel;
    }

    public function getCountDown()
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getCountDown',
            ["json" => ['ORIGEN' => '1']]);


        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $menuSubNivel = json_decode($response->getBody()->getContents());

        return isset($menuSubNivel[0]) ? $menuSubNivel[0] : $menuSubNivel;

    }

    public function getProductsCategories($id_tipo_producto, $page)
    {

        $response = $this->client->request('POST', 'getProducts',
            ["json" => ['ID_TIPO_PRODUCTO' => "$id_tipo_producto",
                'ROWS_PER_PAGE' => '3000',
                'PAGE_NUMBER' => "$page",
                'TYPE_OF_SEARCH' => '1',
                'ORIGEN' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $productos = json_decode($response->getBody()->getContents());

        return $productos;

    }

    public function getProductsBanners($id_tipo_producto, $page)
    {

        $response = $this->client->request('POST', 'getProductsBanners',
            ["json" => ['ID_TIPO_PRODUCTO' => "$id_tipo_producto",
                'ROWS_PER_PAGE' => '3000',
                'PAGE_NUMBER' => "$page",
                'TYPE_OF_SEARCH' => '1',
                'ORIGEN' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $productos = json_decode($response->getBody()->getContents());

        return $productos;

    }

    public function getProductFeaturedproducts()
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', '/getProductFeaturedproducts',
            ["json" => ['ORIGEN' => '1']]);


        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $productos_destacados = json_decode($response->getBody()->getContents());

        return $productos_destacados;

    }

    public function getAllProducts($id_categoria)
    {
        $response = $this->client->request('POST', 'getProducts',
            ["json" => ['ID_TIPO_PRODUCTO' => "$id_categoria",
                'ROWS_PER_PAGE' => '3000',
                'PAGE_NUMBER' => '1',
                'TYPE_OF_SEARCH' => '2',
                'ORIGEN' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $productos = json_decode($response->getBody()->getContents());


        return $productos;
    }

    public function getProductsFiltered($productos_por_pagina,$tipo_de_busqueda,$id_categoria)
    {
        $response = $this->client->request('POST', 'getProducts',
            ["json" => ['ID_TIPO_PRODUCTO' => "$id_categoria",
                'ROWS_PER_PAGE' => "$productos_por_pagina",
                'PAGE_NUMBER' => '1',
                'TYPE_OF_SEARCH' => "$tipo_de_busqueda",
                'ORIGEN' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $productos = json_decode($response->getBody()->getContents());

        return $productos;

    }

    public function getProduct($id)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getProduct',
            ["json" => ['ID_MODELO' => "$id",
                'ORIGEN' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $productos = json_decode($response->getBody()->getContents());

        foreach($productos as $producto){

            if(substr($producto->IMAGEN_100, 0, 19) == "https://muletta.net"){
                $producto->IMAGEN_100 = substr($producto->IMAGEN_100, 19);
                $producto->IMAGEN_100 = "http://creatmos.net" . $producto->IMAGEN_100;
            }
            if(substr($producto->IMAGEN_101, 0, 19) == "https://muletta.net"){
                $producto->IMAGEN_101 = substr($producto->IMAGEN_101, 19);
                $producto->IMAGEN_101 = "http://creatmos.net" . $producto->IMAGEN_101;
            }
            if(substr($producto->IMAGEN_102, 0, 19) == "https://muletta.net"){
                $producto->IMAGEN_102 = substr($producto->IMAGEN_102, 19);
                $producto->IMAGEN_102 = "http://creatmos.net" . $producto->IMAGEN_102;
            }
            if(substr($producto->IMAGEN_103, 0, 19) == "https://muletta.net"){
                $producto->IMAGEN_103 = substr($producto->IMAGEN_103, 19);
                $producto->IMAGEN_103 = "http://creatmos.net" . $producto->IMAGEN_103;
            }
        }

        return $productos;

    }

    public function getProductsCorousel($id)
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getProductsCorousel',
            ["json" => ['ID_MODELO' => $id,
                'ORIGEN' => 1]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $productos = json_decode($response->getBody()->getContents());

        foreach($productos as $producto){
            
            if(substr($producto->IMAGEN_100, 0, 1) == "~"){
                $producto->IMAGEN_100 = substr($producto->IMAGEN_100, 1);
                $producto->IMAGEN_100 = "http://creatmos.net" . $producto->IMAGEN_100;
            }
            if(substr($producto->IMAGEN_101, 0, 1) == "~"){
                $producto->IMAGEN_101 = substr($producto->IMAGEN_101, 1);
                $producto->IMAGEN_101 = "http://creatmos.net" . $producto->IMAGEN_101;
            }
            if(substr($producto->IMAGEN_102, 0, 1) == "~"){
                $producto->IMAGEN_102 = substr($producto->IMAGEN_102, 1);
                $producto->IMAGEN_102 = "http://creatmos.net" . $producto->IMAGEN_102;
            }
            if(substr($producto->IMAGEN_103, 0, 1) == "~"){
                $producto->IMAGEN_103 = substr($producto->IMAGEN_103, 1);
                $producto->IMAGEN_103 = "http://creatmos.net" . $producto->IMAGEN_103;
            }

            if(substr($producto->IMAGEN_100, 0, 19) == "https://muletta.net"){
                $producto->IMAGEN_100 = substr($producto->IMAGEN_100, 19);
                $producto->IMAGEN_100 = "http://creatmos.net" . $producto->IMAGEN_100;
            }
            if(substr($producto->IMAGEN_101, 0, 19) == "https://muletta.net"){
                $producto->IMAGEN_101 = substr($producto->IMAGEN_101, 19);
                $producto->IMAGEN_101 = "http://creatmos.net" . $producto->IMAGEN_101;
            }
            if(substr($producto->IMAGEN_102, 0, 19) == "https://muletta.net"){
                $producto->IMAGEN_102 = substr($producto->IMAGEN_102, 19);
                $producto->IMAGEN_102 = "http://creatmos.net" . $producto->IMAGEN_102;
            }
            if(substr($producto->IMAGEN_103, 0, 19) == "https://muletta.net"){
                $producto->IMAGEN_103 = substr($producto->IMAGEN_103, 19);
                $producto->IMAGEN_103 = "http://creatmos.net" . $producto->IMAGEN_103;
            }
        }

        return $productos;

    }

    
    public function getProductsCorouselDestacados($id)
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getProductsCorouselDestacados',
            ["json" => ['ID_TIPO_PRODUCTO' => $id,
                'ORIGEN' => 1]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $productos = json_decode($response->getBody()->getContents());

        return $productos;

    }

    public function getProductSize($id)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getProductSize',
            ["json" => ['ID_MODELO' => "$id",
                'ORIGEN' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $variantes_productos = json_decode($response->getBody()->getContents());

        return $variantes_productos;

    }

    public function getImagenes($id, $nom)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getImagenes',
            ["json" => ['idPadre' => $id, 'nombre' => $nom]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $variantes_productos = json_decode($response->getBody()->getContents());

        return $variantes_productos;

    }

    public function searchModelo($nom)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'searchModelo',
            ["json" => ['NOMBRE' => $nom]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $variantes_productos = json_decode($response->getBody()->getContents());

        if($variantes_productos != "Sin resultados"){
            foreach($variantes_productos as $producto){
    
                if(substr($producto->IMAGEN_100, 0, 19) == "https://muletta.net"){
                    $producto->IMAGEN_100 = substr($producto->IMAGEN_100, 19);
                    $producto->IMAGEN_100 = "http://creatmos.net" . $producto->IMAGEN_100;
                }
            }
        }

        return $variantes_productos;

    }

    public function insertarProductoNegado($nombre, $no_parte, $correo)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'insertarProductoNegado',
            ["json" => [
                'NOMBRE' => $nombre,
                'NO_PARTE' => $no_parte,
                'CORREO' => $correo
            ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $variantes_productos = json_decode($response->getBody()->getContents());

        return $variantes_productos;

    }

    public function getTallas() {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getTallasDistribuidores',
            ["json" => ['ORIGEN' => '2']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $menuSubNivel = json_decode($response->getBody()->getContents());
        
        return $menuSubNivel;
    }

    public function getGenero() {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getGeneroDistribuidores',
            ["json" => ['ORIGEN' => '2']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $menuSubNivel = json_decode($response->getBody()->getContents());
        
        return $menuSubNivel;
    }

    public function getProducto() {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getProductoDistribuidores',
            ["json" => ['ORIGEN' => '2']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $menuSubNivel = json_decode($response->getBody()->getContents());
        
        return $menuSubNivel;
    }

    public function getTipo() {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getTipoDistribuidores',
            ["json" => ['ORIGEN' => '2']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $menuSubNivel = json_decode($response->getBody()->getContents());
        
        return $menuSubNivel;
    }

}