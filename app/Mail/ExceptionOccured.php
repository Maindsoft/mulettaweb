<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExceptionOccured extends Mailable
{
    use Queueable, SerializesModels;

    public $content;
    public $detalles;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $detalles)
    {
        $this->content = $content;
        $this->detalles = $detalles;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contacto@muletta.com')->to(['carlos.ibarra@maindsoft.net', 'rodolfo.sanchez@maindsoft.net'])->subject('Error en Muletta')->view('mail.excpetion');

        // return $this->to('carlos.ibarra@maindsoft.net')
        //             ->subject('Error en Muletta')
        //             ->view('mail.excpetion');
                    // ->with('content', $this->content);
    }
}