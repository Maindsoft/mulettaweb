<?php
//|----------------------------------------------------
//| link_to option can be only 'home', 'reload',or 'previous' 
//|----------------------------------------------------
return [
        '400' => [
        'code' => "400",
        'title' => "400 Solicitud incorrecta",
        'description' => "El servidor de <em>:domain</em> no pudo entender la solicitud debido a una sintaxis no válida.",
        'icon' => "fa fa-ban",
        'button' => [
            'name' => "Volver a la página anterior",
            'link_to' => "previous",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "El estado de error 400 indica que el servidor no pudo entender la solicitud debido a una sintaxis no válida."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Utilice el botón de retroceso de su navegador y verifique que los datos introducidos en el formulario son correctos. Si necesita asistencia inmediata, envíenos un correo electrónico a contacto@muletta.com"
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Verifique que todo esté bien o póngase en contacto con el proveedor de su sitio web si cree que se trata de un error."
            ],
        ],
    ],
    '401' => [
        'code' => "401",
        'title' => "401 No autorizado",
        'description' => "Debes de estar autenticado para acceder a este recurso en <em>:domain</em>.",
        'icon' => "fa fa-lock",
        'button' => [
            'name' => "Intenta esta página nuevamente",
            'link_to' => "reload",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Un estado de error 401 indica que la solicitud no se ha aplicado porque carece de credenciales de autenticación válidas para el recurso de destino."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Actualice esta página o intente iniciar sesión nuevamente. Si necesita asistencia inmediata, envíenos un correo electrónico."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Actualice esta página o intente iniciar sesión nuevamente. Si el error persiste, póngase en contacto con el proveedor de su sitio web si cree que se trata de un error."
            ],
        ],
    ],
    '403' => [
        'code' => "403",
        'title' => "403 Forbidden",
        'description' => "¡Lo siento! No tienes permisos de acceso para eso en <em>:domain</em>.",
        'icon' => "fa fa-ban ",
        'button' => [
            'name' => "Llévame a la página de inicio",
            'link_to' => "home",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Un estado de error 403 indica que no tiene permiso para acceder al archivo o la página. En general, los servidores web y los sitios web tienen directorios y archivos que no están abiertos a la web pública por razones de seguridad."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Utilice el botón de retroceso de su navegador y compruebe que está en el lugar correcto. Si necesita asistencia inmediata, envíenos un correo electrónico."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Compruebe que está en el lugar correcto y póngase en contacto con el proveedor de su sitio web si cree que se trata de un error."
            ],
        ],
    ],
    '404' => [
        'code' => "404",
        'title' => "404 No encontrado",
        'description' => "No pudimos encontrar lo que estás buscando en <em>:domain</em>.",
        'icon' => "fa fa-frown-o",
        'button' => [
            'name' => "Llévame a la página de inicio",
            'link_to' => "home",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Un estado de error 404 implica que no se pudo encontrar el archivo o la página que está buscando."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Utilice el botón de retroceso de su navegador y verifique que esté en el lugar correcto. Si necesita asistencia inmediata, envíenos un correo electrónico."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Compruebe que está en el lugar correcto y póngase en contacto con el proveedor de su sitio web si cree que se trata de un error."
            ],
        ],
    ],
    '405' => [
        'code' => "405",
        'title' => "405 Método no permitido",
        'description' => "Solicitó esta página con un método HTTP no válido o inexistente en <em>:domain</em>.",
        'icon' => "fa fa-ban",
        'button' => [
            'name' => "Volver a la página anterior",
            'link_to' => "previous",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Un estado de error 405 indica que el servidor conoce el método de solicitud pero que se ha deshabilitado y no se puede usar."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Vaya a la página anterior y vuelva a intentarlo. Si necesita asistencia inmediata, envíenos un correo electrónico."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Vaya a la página anterior y vuelva a intentarlo. Si el error persiste, póngase en contacto con el proveedor de su sitio web si cree que se trata de un error."
            ],
        ],
    ],
    '408' => [
        'code' => "408",
        'title' => "408 Tiempo de espera de solicitud excedida",
        'description' => "The server <em>:domain</em> would like to shut down this unused connection.",
        'icon' => "fa fa-clock-o red",
        'button' => [
            'name' => "Try This Page Again",
            'link_to' => "reload",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "La petición que usted envió al servidor del sitio web tardó más tiempo del que el servidor del sitio web estaba preparado para esperar. Un estado de error 408 indica que al servidor le gustaría cerrar esta conexión no utilizada. Algunos servidores lo envían en una conexión inactiva, incluso sin ninguna solicitud previa del cliente.
            "
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Actualice esta página e intente nuevamente. Si necesita asistencia inmediata, envíenos un correo electrónico."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Actualice esta página e intente nuevamente. Si el error persiste, póngase en contacto con el proveedor de su sitio web si cree que se trata de un error."
            ],
        ],
    ],
    '419' => [
        'code' => "419",
        'title' => '419 Tiempo de espera de autenticación excedida',
        'description' => "La página ha expirado debido a la inactividad en <em>:domain</em>.",
        'icon' => "fa fa-lock",
        'button' => [
            'name' => "Llévame a la página de inicio",
            'link_to' => "home",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "El estado del error 419 indica que la autenticación válida anteriormente ha expirado."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Actualice esta página o intente iniciar sesión nuevamente. Si necesita asistencia inmediata, envíenos un correo electrónico."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Actualice esta página o intente iniciar sesión nuevamente. Si el error persiste, póngase en contacto con el proveedor de su sitio web si cree que se trata de un error."
            ],
        ],
    ],
    '429' => [
        'code' => "429",
        'title' => '429 Demasiadas solicitudes',
        'description' => "El servidor web está devolviendo una notificación de limitación de velocidad en <em>:domain</em>.",
        'icon' => "fa fa-fire",
        'button' => [
            'name' => "Intenta esta página nuevamente",
            'link_to' => "reload",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Este error significa que ha excedido el límite de velocidad de solicitud para el servidor web al que está accediendo.<br><br>Los umbrales de límite de velocidad se establecen por encima de lo que una persona que navega por este sitio debería poder alcanzar y, sobre todo, para protegerse contra solicitudes y ataques automáticos."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Lo mejor que puede hacer es reducir la velocidad de sus solicitudes e intentar nuevamente en unos minutos. <br><br>Nos disculpamos por cualquier inconveniente."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Es probable que este error sea muy breve, lo mejor que puede hacer es volver a comprobarlo en unos minutos y todo volverá a funcionar normalmente. Si el error persiste, comuníquese con el host de su sitio web."
            ],
        ],
    ],
    '500' => [
        'code' => "500",
        'title' => "500 Error interno de servidor",
        'description' => "El servidor web está devolviendo un error interno para <em>:domain</em>.",
        'icon' => "fa fa-fire",
        'button' => [
            'name' => "Prueba con la pagina previa de Muletta",
            'link_to' => "reload",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Un estado de error 500 implica que hay un problema con el software del servidor web que causa un mal funcionamiento."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Puedes probar usando la version previa de nuestro sitio web. Es más estable y cuenta con todas las funcionalidades. <br><br>Si necesita asistencia inmediata, envíenos un correo electrónico. Nos disculpamos por cualquier inconveniente."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Este error solo puede ser solucionado por los administradores del servidor, comuníquese con el proveedor de su sitio web."
            ],
        ],
    ],
    '502' => [
        'code' => "502",
        'title' => "502 Bad Gateway",
        'description' => "The web server is returning an unexpected networking error for <em>:domain</em>.",
        'icon' => "fa fa-bolt orange",
        'button' => [
            'name' => "Try This Page Again",
            'link_to' => "reload",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "es un error que nos devuelve el servidor cuando no es capaz de procesar una petición de entrada por diversos motivos, como un fallo de comprensión entre dos servidores o que el servidor no sea capaz de procesar determinadas tareas."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "<a href=\"http://isup.me/:domain\" target=\"_blank\" rel=\"nofollow\">Compruebe si este sitio web está inactivo para todos o solo para usted.</a>"
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Borrar el caché del navegador y actualizar la página puede solucionar este problema. Si el problema persiste y necesita asistencia inmediata, comuníquese con el proveedor de su sitio web."
            ],
        ],
    ],
    '503' => [
        'code' => "503",
        'title' => "503 Service Unavailable",
        'description' => "El servidor web está devolviendo un error temporal inesperado para <em>:domain</em>.",
        'icon' => "fa fa-exclamation-triangle",
        'button' => [
            'name' => "Intenta esta página nuevamente",
            'link_to' => "reload",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Un estado de error 503 implica que esta es una condición temporal debido a una sobrecarga o mantenimiento temporal del servidor. Este error es normalmente una breve interrupción temporal."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Si necesita asistencia inmediata, envíenos un correo electrónico. Nos disculpamos por cualquier inconveniente."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Es muy probable que este error sea muy breve, lo mejor que puede hacer es volver a comprobarlo en unos minutos y todo volverá a funcionar normalmente."
            ],
        ],
    ],
    '504' => [
        'code' => "504",
        'title' => "504 Gateway Timeout",
        'description' => "El servidor web está devolviendo un error de red inesperado para <em>:domain</em>.",
        'icon' => "fa fa-clock-o",
        'button' => [
            'name' => "Intenta esta página nuevamente",
            'link_to' => "reload",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Un estado de error 504 implica que hay un problema de comunicación IP lento entre los servidores de fondo que intentan cumplir con esta solicitud."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "<a href=\"http://isup.me/:domain\" target=\"_blank\" rel=\"nofollow\">Compruebe si este sitio web está inactivo para todos o solo para usted.</a></p><p>Además, borrar el caché del navegador y actualizar la página puede solucionar este problema. Si el problema persiste y necesita asistencia inmediata, envíenos un correo electrónico."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Borrar el caché del navegador y actualizar la página puede solucionar este problema. Si el problema persiste y necesita asistencia inmediata, comuníquese con el proveedor de su sitio web."
            ],
        ],
    ],
    'maintenance' => [
        'title' => "Mantenimiento Temporal",
        'description' => "El servidor web para <em>:domain</em> Actualmente se encuentra en mantenimiento.",
        'icon' => "fa fa-cogs",
        'button' => [
            'name' => "Intenta esta página nuevamente",
            'link_to' => "reload",
        ],
        'why' => [
            'title' => "¿Que pasó?",
            'description' => "Los servidores y los sitios web necesitan un mantenimiento regular al igual que un automóvil para mantenerlos en funcionamiento sin problemas. Actualmente el sitio se encuentra bajo mantenimiento."
        ],
        'what_do' => [
            'title' => "¿Que puedo hacer?",
            'visitor' => [
                'title' => "Si eres un visitante del sitio",
                'description' => "Puedes probar de nuevo en una hora o acceder a nuestro antiguo sitio web. Si necesita asistencia inmediata, envíenos un correo electrónico. Nos disculpamos por cualquier inconveniente."
            ],
            'owner' => [
                'title' => "Si eres el dueño/staff del sitio",
                'description' => "Lo más probable es que el período de mantenimiento sea muy breve, lo mejor que puede hacer es volver a comprobarlo en unos minutos y todo volverá a funcionar normalmente."
            ],
        ],
    ],
];
