<div style="display: flex; align-items: baseline;">
    <?php
        $menu_PrimerNivel = session()->get('menuPrimerNivel');
        $segundoNivel = session()->get('menuSubNivel');
    ?>
    @foreach($menu_PrimerNivel as $primer)
        @foreach($primer as $details)
            @if($sub == $details->ID_CATEGORIA_FLEXIBLE)
                <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $details->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $details->URL_CATEGORIA])}}" class="categoriaPrincipal indicador_Categoria alinear txt-muletta-oro mb-3" style="padding-left: 9%;">
                    <?=$details->NOMBRE?>
                </a>
                @foreach($segundoNivel as $segundo)
                    @foreach($segundo as $s)
                        @if($details->ID_CATEGORIA_FLEXIBLE == $s->CATEGORIA_PADRE)
                            <?php
                                $auxi = 0;
                                foreach($segundoNivel as $tercer){
                                    foreach($segundo as $t){
                                        if($t->ID_CATEGORIA_FLEXIBLE == $id_tipo_producto && $t->CATEGORIA_PADRE == $s->ID_CATEGORIA_FLEXIBLE){
                                            $auxi = 1;
                                        }
                                    }
                                }
                            ?>

                            @if($s->ID_CATEGORIA_FLEXIBLE == $id_tipo_producto)
                                <i class="categoriaPrincipal g-color-gray-light-v2 g-ml-5 g-mr-5 fa fa-angle-right"></i>
                                <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $s->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA])}}" class="categoriaPrincipal categoriaSecundaria indicador_Categoria alinear txt-muletta-oro mb-3">
                                    <?=$s->NOMBRE?>
                                </a>
                            @elseif($auxi == 1 )
                                @foreach($segundo as $t)
                                    @if($id_tipo_producto == $t->ID_CATEGORIA_FLEXIBLE)
                                        @if($t->CATEGORIA_PADRE == $s->ID_CATEGORIA_FLEXIBLE)
                                            <i class="categoriaPrincipal g-color-gray-light-v2 g-ml-5 g-mr-5 fa fa-angle-right"></i>
                                            <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $s->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA])}}" class="categoriaPrincipal indicador_Categoria alinear txt-muletta-oro mb-3">
                                                <?=$s->NOMBRE?>
                                            </a>
                                        @endif
                                        <i class="categoriaPrincipal g-color-gray-light-v2 g-ml-5 g-mr-5 fa fa-angle-right"></i>
                                        <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $t->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $t->URL_CATEGORIA])}}" class="categoriaPrincipal indicador_Categoria alinear txt-muletta-oro mb-3">
                                            <?=$t->NOMBRE?>
                                        </a>
                                    @endif
                                @endforeach
                            @endif
                            
                        @endif
                    @endforeach
                @endforeach

                <div id="sector_categorias" style="margin-left: auto; margin-right: auto; padding-left: 1%;">
                @foreach($segundoNivel as $segundo)
                    @foreach($segundo as $s)
                        @if($details->ID_CATEGORIA_FLEXIBLE == $s->CATEGORIA_PADRE)
                            <?php
                            $aux = 0;
                            foreach($segundoNivel as $tercer){
                                foreach($segundo as $t){
                                    if($t->ID_CATEGORIA_FLEXIBLE == $id_tipo_producto){
                                        $aux = 1;
                                    }
                                }
                            }
                            ?>

                            @if($aux == 1)
                                
                                    @foreach($segundoNivel as $tercer)
                                        @if($s->ID_CATEGORIA_FLEXIBLE == $id_tipo_producto)
                                            @foreach($segundo as $t)
                                                @if($id_tipo_producto == $t->CATEGORIA_PADRE)
                                                    <button class="alinear accordion" style="padding-left: 1em;">
                                                        <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $t->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $t->URL_CATEGORIA])}}">
                                                            @if($t->ID_CATEGORIA_FLEXIBLE == $id_tipo_producto)
                                                                <?=$t->NOMBRE?> <i class="fa fa-circle" style="font-size: 5px; color: #b78b1e;"></i>
                                                            @else
                                                                <?=$t->NOMBRE?>
                                                            @endif

                                                        </a>
                                                    </button>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                
                            <!--.panel-->
                            @else
                            
                                <button class="alinear accordion" style="padding-left: 1em;">
                                    <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $s->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA])}}">
                                        @if($s->ID_CATEGORIA_FLEXIBLE == $id_tipo_producto)
                                            <?=$s->NOMBRE?> <i class="fa fa-circle" style="font-size: 5px; color: #b78b1e;"></i>
                                        @else
                                            <?=$s->NOMBRE?>
                                        @endif
                                    </a>
                                </button>
                            @endif
                            
                        @endif
                    @endforeach
                @endforeach
                </div>
            @endif
        @endforeach
    @endforeach
    <div class="filtro-btn" style="margin-right: 11%">
        <div class="txt-muletta-oro" onclick="mostrarFiltros()">
            <span class="cool-link">
                <i class="fa fa-sliders-h"></i> &nbsp;
                FILTROS
            <span>
        </div>
        <div id="menu-categoria">
            @include('componentes.filtros_productos')
        </div>
    </div>
</div>