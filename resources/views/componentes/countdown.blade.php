<?php
    $coundown = session()->get('countDown')['countDown'];
?>

<div id="yith-topbar-countdown" class="topbar-countdown-container">
    <div id="countDown_time">
        <div class="countdown_information text-center">
            <div class="grid_countdown">
                <div class="grid_slogan margin-7">
                    <span class="countdown_slogan" style="display: table-cell; vertical-align: middle;"><?php echo str_replace( "/*", "<br>", $coundown->TITULO_COUNTDOWN ); ?></span>
                </div>
                <span class="grid_fecha countdown hasCountdown col-sm-12 col-md-4 margin-12">
                    <div class="grid_fecha_countdown">
                        <div class="text-center">
                            <span class="num days div_numeros">0</span>
                            <div class="margin-7">
                                <span class="countdown-label">DÍAS</span>
                            </div>
                        </div>
                        <div class="text-center">
                            <span class="num hours div_numeros">0</span>
                            <div class="margin-7">
                                <span class="countdown-label">HRS</span>
                            </div>
                        </div>
                        <div class="text-center">
                            <span class="num minutes div_numeros">0</span>
                            <div class="margin-7">
                                <span class="countdown-label">MIN</span>
                            </div>
                        </div>
                        <div class="text-center">
                            <span class="num seconds div_numeros">0</span>
                            <div class="margin-7">
                                <span class="countdown-label no-margin">SEG</span>
                            </div>
                        </div>
                    </div>
                </span>
                <div class="grid_mensaje text-center margin-7">
                    <div>
                        <span class="message">{{ $coundown->MENSAJE }}</span>
                    </div>
                    <div>
                        <a class="btn u-btn-thirdy g-font-size-12 text-uppercase g-px-25" href="{{ $coundown->URL_ENLACE }}"> {{ $coundown->TEXTO_BOTON }} </a>
                    </div>
                </div>
            </div>
            <input id="fecha_vencimiento" type="hidden" value="{{ $coundown->FECHA_VENCIMIENTO }}">
        </div>
    </div>
</div>