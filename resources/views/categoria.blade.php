@extends('template.main')

@section('title', ucfirst( str_replace ('-', ' ', $url_tipo_producto) ))

<?php
    $host= $_SERVER["HTTP_HOST"];
    $url= $_SERVER["REQUEST_URI"];
?>

@section('meta_title', ucfirst( str_replace ('-', ' ', $url_tipo_producto) ))

@section('meta_url', "https://".$host.$url)

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/catalogo-productos.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/filtros.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30" style="padding-bottom: 0px !important">
        <div class="container-flex grid-menu" style="border-top: 1px solid #eee">
            <ul id="navegacion_categorias" class="u-list-inline" style="display: none;">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item txt-muletta-oro ">
                    <span>Catálogo de productos</span>
                </li>
            </ul>

            <div id="sidebar" >
                @include('componentes.menu_categorias_productos')
            </div>

            <!-- <div id="filtro-movil" class="filtro-btn" style="display:none">
                <div class="txt-muletta-oro" onclick="mostrarFiltros()">
                    <span class="cool-link">
                        <i class="fa fa-sliders-h"></i> &nbsp;
                        FILTROS
                    <span>
                </div>
                <div id="menu-categoria">
                </div>
            </div> -->
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->
        
        <div id="div_productos" class="main_custom" onscroll="scrollWindows()">
            <!-- Products -->
            @if($productos != "Sin resultados")
                @include('componentes.productos')
            @else
                <div class="row g-pt-30 g-mb-50">
                    <div class="no-results text-center" style="min-height:50vw;">
                        <h1>
                            <img class="photo" data-src="{{ asset('assets/global/img/icons/icono-buscar.png') }}" width="100em">
                            No hay productos que coincidan con tu búsqueda.
                        </h1>
                    </div>
                </div>
            @endif
            <!-- End Products -->

            <!-- <div class="loader"></div> -->
        </div>

@endsection

@section('script')
    <script src="{{ asset('assets/global/js/maindsoft/infiniteScroll.js') }}"></script>
    <script src="{{ asset('assets/global/js/maindsoft/filtros.js') }}"></script>
@endsection