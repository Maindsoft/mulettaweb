@extends('template.main')

@section('title', 'Registrar')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/register.css') }}">
@endsection

@section('content')
     <!-- Pestañas Navegación -->
     <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover"
                       href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item g-color-primary text-lowercase">
                    <span>Restablecer</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- START: REGISTER FORM  -->
    <div class="container mb-5 margin-top">
        <div class="g-max-width-445 text-center rounded mx-auto g-pa-20">
            <div class="g-bg-white mb-4">
        
                <div class="text-center ">
                    <h1 class="h4 g-font-weight-600 txt-muletta-gris">Recupera tu contraseña</h1>
                </div>
        
                <!-- Form -->
                <form class="g-py-15" id="register_user" method="POST">
                    {{ csrf_field() }}
                    <div class="row no-gutters">
                        <!-- nombre -->
                        <div class="col-12 px-5 mb-4">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15"
                                        name="Email"
                                        type="text"
                                        id="Email"
                                        placeholder="Email">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <!-- <button class="btn btn-md btn-block u-btn-primary rounded-0  mb-3" type="submit">RECUPERAR CONTRASEÑA</button> -->
                            <button class="btn btn-md btn-block u-btn-primary rounded-0  mb-3 g-recaptcha" 
                                data-sitekey="6Ld9iqUdAAAAANf2RrIPNpn-L8uGUPFxCUX9Af-x" 
                                data-callback='onSubmitResetPassword' 
                                data-action='submit'>RECUPERAR CONTRASEÑA</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection