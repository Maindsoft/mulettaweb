@extends('template.main')

@section('title', 'Login')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/login.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover"
                        href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item g-color-primary text-lowercase">
                    <span>Login</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- Login Header -->
    <section class="container g-pt-100 g-pb-20">
        <div class="row">
            <div class="mb-3">
                <h2 class=" g-font-weight-600 mb-3 txt-muletta-oro">Muletta</h2>
                <p class="txt-muletta-gris-claro">Estas a un sólo paso de pertenecer a la Comunidad Muletta.</p>
            </div>
        </div>
    </section>

    <!-- START: LOGIN  -->
    <div class="container mb-3">
        <div class=" g-max-width-380 g-brd-around g-brd-gray-light-v4 text-center rounded mx-auto g-pa-20 ">
            <div class=" g-brd-gray-light-v3 g-bg-white rounded mb-4">
                <div class="text-center mb-4">
                    <h1 class="h4  g-font-weight-400 txt-muletta-gris-claro">Entra a tu cuenta</h1>
                </div>
                {!! $errors->first('email', '<br><p><span class="help-block" style="color:red;">:message</span></p>') !!}
                <!-- START: Form -->
                <form class="g-py-15" method="POST" action="{{route('verificar_compra', $id_compra)}}">
                
                    {{ csrf_field() }}
                    <span class="{{session()->has('usuarioRegistrado') ? 'g-color-primary g-font-size-20' : ''}}"><b>
                        {{session()->has('usuarioRegistrado') ? session()->get('usuarioRegistrado') : ''}}</b>
                    </span>
                    {{session()->forget('usuarioRegistrado')}}

                    <div class="mb-4 {{ $errors->has('email') ? 'g-color-red': '' }}">
                        <div class="input-group g-rounded-left-3">
                            <span class="input-group-prepend g-width-45">
                                <span class="input-group-text justify-content-center w-100 g-bg-transparent g-brd-gray-light-v3 g-color-gray-dark-v5">
                                    <i class="fa fa-envelope-open"></i>
                                </span>
                            </span>
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-rounded-left-0 g-rounded-right-3 g-py-15 g-px-15"
                                value="{{old('email')}}"
                                name="email"
                                type="email"
                                placeholder="Correo electronico">                            
                        </div>
                    </div>

                    <div class="mb-4 {{ $errors->has('contrasena') ? 'g-color-red': '' }}">
                        <div class="input-group g-rounded-left-3 mb-4">
                            <span class="input-group-prepend g-width-45">
                                <span class="input-group-text justify-content-center w-100 g-bg-transparent g-brd-gray-light-v3 g-color-gray-dark-v5">
                                    <i class="fa fa-key"></i>
                                </span>
                            </span>
                            <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-rounded-left-0 g-rounded-right-3 g-py-15 g-px-15"
                                value="{{old('contrasena')}}"
                                name="contrasena"
                                type="password"
                                placeholder="Contraseña" id="password-field">
                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password "></span>
                        </div>
                        {!! $errors->first('contrasena', '<br><span class="help-block">:message</span>') !!}
                    </div>
                    <div class="row justify-content-between mb-3">
                        <div class="col align-self-center">
                            <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-13 g-pl-25 mb-0">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                <span class="d-block u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon=""></i>
                                </span>
                                Recordar usuario
                            </label>
                        </div>
                        <div class="col align-self-center text-right">
                            <a class="g-font-size-13" href="{{route('restablecer')}}">¿Olvidaste tu contraseña?</a>
                        </div>
                    </div>
                    <div class="text-center mb-3">
                        <p class="g-color-gray-dark-v5">
                            ¿No tienes una cuenta?
                            <a class="g-font-weight-600 g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('register')}}">Crea una aquí</a>
                        </p>
                    </div>
                    <br>
                    <div class="mb-3">
                        <button class="btn btn-block u-btn-primary g-font-size-20 text-uppercase  g-px-25" type="submit">
                            INICIAR SESIÓN
                        </button>
                    </div>
                </form>
                <!-- End: Form -->


                <!-- START: SOCIAL LOGIN BOTONS -->
                <div class="row no-gutters">
                    <div class="col-6">
                        <a class="g-color-white--hover" href="{{route('login-facebook')}}">
                            <button class="btn btn-block u-btn-facebook g-font-size-18   g-px-25 mr-2" type="button">
                                Facebook
                            </button>
                        </a>
                    </div> 
                    <div class="col-6">
                        <a class="g-color-white--hover" href="{{route('login-google')}}">
                            <button type="button" class="btn btn-block u-btn-google-plus g-font-size-18   g-px-25 ml-2">
                                Google
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <!-- END: LOGIN  -->

    <div class="row">
        <div class="<?= $id_compra ? 'col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 offset-md-3 offset-lg-3 offset-xl-3' : 'col-12' ?>">
            <!-- START: CREA UN NUEVO USUARIO -->
            <div class="container mb-3">
                <div class=" g-max-width-380 g-brd-around g-brd-gray-light-v4 text-center rounded mx-auto g-pa-20 ">
                    <div class=" g-brd-gray-light-v3 g-bg-white rounded mb-4">
                        <div class="text-center mb-4">
                            <h1 class="h4  g-font-weight-400 txt-muletta-gris-claro"></h1>
                        </div>
                        <div class="mb-3">
                            <a class="btn btn-block u-btn-primary g-font-size-20 text-uppercase  g-px-25 mb-3 g-color-white--hover" href="{{route('register')}}">
                                CREA NUEVO USUARIO
                            </a>
                            <div class="text-left desc">
                                <P>Pague con facilidad</P>
                                <p>Evite llenar de nuevo sus datos de envio o pago</p>
                                <p>Aprovecha las ofertas y promociones</p>
                                <p>Gestionar de forma ágil sus pedidos</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: CREA UN NUEVO USUARIO -->
        </div>
        @if($id_compra)
            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <!-- START: CREA UN NUEVO USUARIO -->
                <div class="container mb-3">
                    <div class=" g-max-width-380 g-brd-around g-brd-gray-light-v4 text-center rounded mx-auto g-pa-20 ">
                        <div class=" g-brd-gray-light-v3 g-bg-white rounded mb-4">
                            <div class="text-center mb-4">
                                <h1 class="h4  g-font-weight-400 txt-muletta-gris-claro"></h1>
                            </div>
                            <div class="mb-3">
                                <div class="text-left desc">
                                    <P class="text-center"><b>Compra sin registro</b></P>
                                    <p>Realice su compra y cuando termine puede crear su cuenta si lo desea</p>
                                </div>
                                <a class="btn btn-block u-btn-primary g-font-size-20 text-uppercase  g-px-25 mb-3 g-color-white--hover" href="{{route('carrito-user')}}">
                                    Comprar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: CREA UN NUEVO USUARIO -->
            </div>
        @endif
    </div>
    
@endsection


@section('script')
    <script type="text/javascript" src="{{ asset('assets/global/js/maindsoft/login.js') }}"></script>
@endsection