@extends('template.main')

@section('title', 'Devoluciones')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/account.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <div class="d-sm-flex ">
                <div class="align-self-left ">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Muletta Cash</span>
                        </li>
                    </ul>
                </div>
                <div class="align-self-center ml-auto">
                    <h1 class="h3 mb-0">Muletta Cash | {{ $perfil_usuario->NOMBRE }}</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- Mi Cuenta -->
    <div class="container g-pt-70 g-pb-30">
        <div class="row">
            <!-- Profile Settings -->
            <div class="col-lg-3 g-mb-50">
                <?php
                    $pagina = 'muletta-cash';
                ?>
                @include('componentes.lateral_perfil')
            </div>
            <!-- End Profile Settings -->

            <!-- Orders -->
            <div class="col-lg-9 g-mb-50">

                <div class="coins-muletta" style="padding-left:16px;padding-right:16px;">
                    <?php
                        $virtualMoney = session()->get('virtualMoney')['virtualMoney'];
                    ?>
                    
                    <div class="text-center">
                        <h4>SALDO DISPONIBLE</h4>
                        <h1 class="txt-muletta-oro coins-cantidad">
                            <img src="{{ asset('assets/global/img/icons/moneda_muletta.png') }}" class="g-width-50 g-height-50">
                            {{ $virtualMoney && $virtualMoney->DINERO_VIRTUAL ? $virtualMoney->DINERO_VIRTUAL : '0.00' }}
                        </h1>
                    </div>
                </div>
                <div class="coins-detalles" style="padding-left:16px;padding-right:16px;">
                    <h3 class="txt-muletta-oro margin-top" style="font-weight: bold;font-size: 1.5rem;">DETALLES DE CUENTA</h3>
                    <span  style="font-weight: bold;color:#9c9b9b;">ÚLTIMOS MOVIMIENTOS</span>
                    
                    @if($mulettaCash)
                        <div class="div-coins-detalles">
                            @foreach($mulettaCash as $key => $coins)
                                <div class="table-responsive margin-top" style="border-bottom: 1px #9c9b9b solid;padding-bottom:3vh;">
                                    <table style="width:100%;">
                                        <thead class="text-center">
                                            <th></th>
                                            <th>
                                                <span  style="font-weight: bold;color:#9c9b9b;">FECHA</span>
                                            </th>
                                            <th>
                                                <span  style="font-weight: bold;color:#9c9b9b;">ID</span>
                                            </th>
                                            <th>
                                                <span  style="font-weight: bold;color:#9c9b9b;">CONCEPTO</span>
                                            </th>
                                            <th>
                                                <span  style="font-weight: bold;color:#9c9b9b;">SALDO <br> ANTERIOR</span>
                                            </th>
                                            <th>
                                                <span  style="font-weight: bold;color:#9c9b9b;">CARGO</span>
                                            </th>
                                            <th>
                                                <span  style="font-weight: bold;color:#9c9b9b;">SALDO <br> TOTAL</span>
                                            </th>
                                        </thead>
                                        <tbody>
                                            <tr class="text-center">
                                                <td style="position: relative;top: -1em;font-size: 1.5em;">
                                                    @if($coins->ID_TIPO_ABONO == 1)
                                                        <i class="fa fa-arrow-up" style="color:green;"></i>
                                                    @else
                                                        <i class="fa fa-arrow-down" style="color:red;"></i>
                                                    @endif
                                                </td>
                                                <td>
                                                    <span style="font-weight: bold;">{{ $coins->FECHA }}</span>
                                                </td>
                                                <td>
                                                    <span style="font-weight: bold;">{{ $coins->DOCUMENTO_REFERENCIA }}</span>
                                                </td>
                                                <td>
                                                    <span style="font-weight: bold;">{{ $coins->DESCRIPCION }}</span>
                                                </td>
                                                <td>
                                                    <span style="font-weight: bold;">$ {{ $coins->MONTO_ANTERIOR }}</span>
                                                </td>
                                                <td>
                                                    @if($coins->ID_TIPO_ABONO == 1)
                                                        <span style="color:green;font-weight: bold;">+ $ {{ $coins->MONTO }}</span>
                                                    @else
                                                        <span style="color:red;font-weight: bold;">- $ {{ $coins->MONTO }}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <span class=" <?=  $key == 0 ? 'txt-muletta-oro' : '' ?> " style="font-weight: bold;">
                                                        @if($coins->ID_TIPO_ABONO == 1)
                                                            $ {{ $coins->MONTO_ANTERIOR + $coins->MONTO }}
                                                        @else
                                                            $ {{ $coins->MONTO_ANTERIOR - $coins->MONTO }}
                                                        @endif
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    
                </div>
                
                
            </div>
            <!-- Orders -->
        </div>
    </div>
    <!-- Fin mi Cuenta -->
@endsection
