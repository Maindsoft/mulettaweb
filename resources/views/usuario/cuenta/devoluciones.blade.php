@extends('template.main')

@section('title', 'Devoluciones')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/account.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <div class="d-sm-flex ">
                <div class="align-self-left ">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Devoluciones</span>
                        </li>
                    </ul>
                </div>
                <div class="align-self-center ml-auto">
                    <h1 class="h3 mb-0">Devoluciones | {{ $perfil_usuario->NOMBRE }}</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- Mi Cuenta -->
    <div class="container g-pt-70 g-pb-30">
        <div class="row">
            <!-- Profile Settings -->
            <div class="col-lg-3 g-mb-50">
                <?php
                    $pagina = 'Devoluciones';
                ?>
                @include('componentes.lateral_perfil')
            </div>
            <!-- End Profile Settings -->

            <!-- Orders -->
            <div class="col-lg-9 g-mb-50" >

                <!-- Links -->
                <ul class="list-inline g-brd-bottom--sm g-brd-gray-light-v3 mb-5">
                    <li class="list-inline-item g-pb-10 g-pr-10 g-mb-20 g-mb-0--sm">
                        <a class="g-brd-bottom g-brd-2 g-brd-primary g-color-main g-color-black g-font-weight-600 g-text-underline--none--hover g-px-10 g-pb-13" href="#!">Devoluciones</a>
                    </li>
                </ul>
                <!-- End Links -->
                
                @foreach($ordenes as $orden)
                    @if($orden->ID_DEVOLUCION_WEB != null)
                        
                        <!-- Order Block -->
                        <div class="g-brd-around g-brd-gray-light-v4 rounded g-mb-30">
                            <div class="g-bg-gray-light-v5 g-pa-20">
                                <div class="row">
                                    <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                        <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2"> Fecha de la Orden</h4>
                                        <span class="g-color-black g-font-weight-300 g-font-size-13">{{ $orden->FECHA_VENTA }}</span>
                                    </div>

                                    <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                        <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Total</h4>
                                        <span class="g-color-black g-font-weight-300 g-font-size-13">${{ number_format($orden->TOTAL_DEVOLUCION ,2) }}</span>
                                    </div>

                                    <div class="col-sm-3 col-md-2 ml-auto text-sm-right">
                                        <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Pedido #{{ $orden->ID_VENTA }} </h4>
                                    </div>
                                </div>
                            </div>

                            <!-- Order Content -->
                            <div class="g-pa-20">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="mb-4">
                                            <h3 class="h5 mb-1 <?= $orden->DESCRIPCION == 'Cancelado' ? 'text-red' : 'text-green' ?>">
                                                {{ $orden->DESCRIPCION }}
                                            </h3>
                                        </div>
                                        @foreach($productos as $producto)
                                            @for($i = 0; $i< count($producto); $i++)
                                                @if($producto[$i]->ID_DEVOLUCION_WEB == $orden->ID_DEVOLUCION_WEB)
                                                <div class="row">
                                                    <div class="col-4 col-sm-3 g-mb-30">
                                                        @if(substr($producto[$i]->IMAGEN, 0, 4) != 'http')
                                                            <img class="img-fluid" src="{{ 'http://creatmos.net'.str_replace('~', '', $producto[$i]->IMAGEN) }}" alt="imagen del producto">
                                                        @else
                                                            <img class="img-fluid" src="{{ $producto[$i]->IMAGEN }}" alt="imagen del producto">
                                                        @endif
                                                    </div>

                                                    <div class="col-8 col-sm-9 g-mb-30">
                                                        <h4 class="h6 g-font-weight-400">
                                                            <a href="#!">
                                                                {{ $producto[$i]->DESCRIPCION }}
                                                            </a>
                                                        </h4>
                                                        <span class="d-block g-color-gray-dark-v4 g-font-size-13 mb-2">{{ $producto[$i]->DESCRIPCION_TALLA }}</span>
                                                        <span class="d-block g-color-gray-dark-v4 g-font-size-13 mb-2">x {{ $producto[$i]->CANTIDAD_VENDIDA }}</span>
                                                        <span class="d-block g-color-lightred mb-2">${{ number_format( $producto[$i]->PRECIO_ORIGINAL ,2) }}</span>
                                                    </div>
                                                </div>
                                                @endif
                                            @endfor
                                        @endforeach
                                    </div>
                                    
                                    @if($orden->ID_ESTATUS_DEVOLUCION == 1)
                                        <div class="col-md-4">
                                            <div class="row">
                                                <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4" href="{{ Route('GenerarPdf', $orden->ID_VENTA) }}" target="_blank">Descargar PDF</a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                                <!-- End Order Content -->
                        </div>
                        <!-- End Order Block -->
                    @endif
                @endforeach
                
            </div>
            <!-- Orders -->
        </div>
    </div>
    <!-- Fin mi Cuenta -->
@endsection
