@extends('template.main')

@section('title', 'Mi cuenta')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/account.css') }}">
@endsection

@section('content')
<!-- Pestañas Navegación -->
<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
        <div class="d-sm-flex ">
            <div class="align-self-left ">
                <ul class="u-list-inline">
                    <li class="list-inline-item g-mr-5">
                        <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                        <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                    </li>
                    <li class="list-inline-item g-color-primary">
                        <span>Mis Direcciones</span>
                    </li>
                </ul>
            </div>
            <div class="align-self-center ml-auto">
                <h1 class="h3 mb-0">Direcciones</h1>
            </div>
        </div>
    </div>
</section>
<!-- Fin Pestañas Navegación -->

<!-- Mi Cuenta -->
<div class="container g-pt-70 g-pb-30">
    <input type="hidden" id="email_cliente" class="form-control" value="<?=$perfil_usuario->EMAIL?>">

    <div class="row">
        <!-- Profile Settings -->
        <div class="col-lg-3 g-mb-50">
            <?php
                $pagina = 'Direcciones';
            ?>
            @include('componentes.lateral_perfil')
        </div>
        <!-- End Profile Settings -->
        <!-- Orders -->
        <div class="col-lg-9 g-mb-50 g-brd-around">

            <div class="row">
                <div class="col-md-12">
                   <br><br>
                    <div class="row">
                        <?php
                            $borde = 0;
                        ?>
                        <!-- Default Outline Panel-->
                        @foreach($direcciones as $direccion)
                            @if($direccion->CALLE != 'null' && $direccion->CALLE != NULL)
                                <?php
                                    $borde++;
                                ?>
                                <div class="col-12 col-md-4 card rounded-0">
                                    <div class="card-block" style="<?= $borde < 3 ? 'border-right: 1px black solid;' : ''?>">
                                        <h4 class="h5">
                                            {{ $direccion->ALIAS }}
                                        </h4>
                                        <p class="card-text">
                                            {{ $direccion->CALLE }} #{{ $direccion->NUM_EXT }} 
                                            @if($direccion->NUM_INT)
                                                int. {{ $direccion->NUM_INT }}
                                            @endif
                                            <br>
                                            Fracc. {{ $direccion->COLONIA }}
                                            <br>
                                            C.P. {{ $direccion->CODIGO }}
                                            <br>
                                            {{ $direccion->MUNICIPIO }}
                                            <br>
                                            {{ $direccion->ESTADO }}, {{ $direccion->PAIS }}
                                        </p>
                                        <div class="float-right">
                                            <a class="g-color-gray-dark-v3" style="font-size: 1.3rem;margin-right: 13px;" onclick="editarAdress('<?=$direccion->ID_DIRECCION?>', '<?=$direccion->ORIGEN?>')">
                                                <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                            </a>
                                            @if($direccion->ID_DIRECCION)
                                            <a class="g-color-red-v4" style="font-size: 1.3rem;margin-right: 10px;" onclick="eliminarAddress('<?=$direccion->ID_DIRECCION?>')">
                                                <i class="fa fa-trash-alt" aria-hidden="true"></i>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <?php
                                    if ($borde == 3) {
                                        $borde = 0;
                                    }
                                ?>
                            @endif()
                        @endforeach()
                        <!-- End Default Outline Panel-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button id="add_adress" class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4">
                        Agregar dirección
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fin mi Cuenta -->

<!-- Modal añadir o editar las direcciones -->
<div class="modal fade" id="modalAdress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="title-modal"></h3>
                <a href="#!" data-dismiss="modal" class="float-right g-font-size-20 g-text-underline--none--hover g-color-muletta-gray-dark g-color-primary--hover g-mb-10">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <form id="form-pass">
                <div class="modal-body">
                    <input type="hidden" name="email" id="email" class="form-control" value="<?=$perfil_usuario->EMAIL?>">
                    <input type="hidden" name="id_address" id="id_address" class="form-control">
                    <input type="hidden" name="origen" id="origen" class="form-control">
                    <input type="hidden" name="addInput" id="addInput" class="form-control">
                    <div class="row">
                        <div class="col-md-12 g-mb-20">
                            <div class="form-group">
                                <label class="d-block g-color-gray-dark-v2 g-font-size-13">Alias</label>
                                <input id="alias" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="alias" type="text" placeholder="Casa, Trabajo" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 g-mb-20">
                            <div class="form-group">
                                <label class="d-block g-color-gray-dark-v2 g-font-size-13">Calle</label>
                                <input id="street_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="address" type="text" placeholder="Av. aguascalientes" required>
                            </div>
                        </div>

                        <div class="col-md-6 g-mb-20">
                            <div class="form-group">
                                <label class="d-block g-color-gray-dark-v2 g-font-size-13">Num. ext</label>
                                <input id="num_ext_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="num_ext" type="number" placeholder="123" required>
                            </div>
                        </div>

                        <div class="col-md-6 g-mb-20">
                            <div class="form-group">
                                <label class="d-block g-color-gray-dark-v2 g-font-size-13">Num. int</label>
                                <input id="num_int_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="num_int" type="text" placeholder="A">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 g-mb-20">
                            <div class="form-group">
                                <label class="d-block g-color-gray-dark-v2 g-font-size-13">Colonia</label>
                                <input id="col_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="col" type="text" placeholder="Las americas" required>
                            </div>
                        </div>

                        <div class="col-md-12 g-mb-20">
                            <div class="form-group">
                                <label class="d-block g-color-gray-dark-v2 g-font-size-13">Código postal</label>
                                <input id="code_postal_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="zip" type="text" placeholder="25326" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 g-mb-20">
                            <div id="div_municipios" class="form-group">
                                <label class="d-block g-color-gray-dark-v2 g-font-size-13">Municipio</label>
                                <select class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="province" id="mun_user">
                                        
                                </select>
                                <!-- <input id="mun_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="province" type="text" placeholder="Aguascalientes" required> -->
                            </div>
                        </div>

                        @if($estados)
                            <div class="col-md-12 g-mb-20">
                                <div class="form-group">
                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Estado</label>
                                    <select id="state_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="stateProvince" required onchange="buscarMunicipios()">
                                        <option value=""></option>
                                        @foreach($estados as $estado)
                                            <option value="{{ $estado->ID_ESTADO }}">{{ $estado->DESCRIPCION_ESTADO }}</option>
                                        @endforeach
                                    </select>
                                    <!-- <input id="state_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="stateProvince" type="text" placeholder="Aguascalientes" required> -->
                                </div>
                            </div>
                        @endif

                        <div class="col-md-12 g-mb-20">
                            <div class="form-group">
                                <label class="d-block g-color-gray-dark-v2 g-font-size-13">Referencia</label>
                                <textarea id="referencia" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="referencia" type="text" placeholder="Entre que calles" required> </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn_contrasenia_update" type="submit" class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 ml-auto">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal añadir o editar las direcciones -->

@endsection

@section('script')
    <script src="{{ asset('assets/global/js/maindsoft/direcciones.js') }}"></script>
@endsection()
