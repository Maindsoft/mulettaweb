@extends('template.main')

@section('title', 'Mi cuenta')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/account.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <div class="d-sm-flex ">
                <div class="align-self-left ">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Mis Pedidos</span>
                        </li>
                    </ul>
                </div>
                <div class="align-self-center ml-auto">
                    <h1 class="h3 mb-0">Mis Pedidos | {{ $perfil_usuario->NOMBRE }}</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- Mi Cuenta -->
    <div class="container g-pt-70 g-pb-30">
        <div class="row">
            <!-- Profile Settings -->
            <div class="col-lg-3 g-mb-50">
                <?php
                    $pagina = 'Pedidos';
                ?>
                @include('componentes.lateral_perfil')
            </div>
            <!-- End Profile Settings -->

            <!-- Orders -->
            <div class="col-lg-9 g-mb-50" >

                <!-- Links -->
                <ul class="list-inline g-brd-bottom--sm g-brd-gray-light-v3 mb-5">
                    <li class="list-inline-item g-pb-10 g-pr-10 g-mb-20 g-mb-0--sm">
                        <a class="g-brd-bottom g-brd-2 g-brd-primary g-color-main g-color-black g-font-weight-600 g-text-underline--none--hover g-px-10 g-pb-13" href="#!">Pedidos</a>
                    </li>
                </ul>
                <!-- End Links -->
                
                @foreach($ordenes as $orden)
                    <!-- Order Block -->
                    <div class="g-brd-around g-brd-gray-light-v4 rounded g-mb-30">
                        <div class="g-bg-gray-light-v5 g-pa-20">
                            <div class="row">
                                <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                    <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2"> Fecha de la Orden</h4>
                                    <span class="g-color-black g-font-weight-300 g-font-size-13">{{ $orden->FECHA_VENTA }}</span>
                                </div>

                                <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                    <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Subtotal</h4>
                                    <span class="g-color-black g-font-weight-300 g-font-size-13">${{ number_format($orden->SUBTOTAL,2) }}</span>
                                </div>

                                <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                    <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Envio</h4>
                                    <span class="g-color-black g-font-weight-300 g-font-size-13">${{ number_format($orden->COSTO_ENVIO,2) }}</span>
                                </div>

                                <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                                    <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Total</h4>
                                    <span class="g-color-black g-font-weight-300 g-font-size-13">${{ number_format($orden->IMPORTE ,2) }}</span>
                                </div>

                                <div class="col-sm-3 col-md-2 ml-auto text-sm-right">
                                    <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Pedido #{{ $orden->ID_VENTA }} </h4>
                                </div>
                            </div>
                        </div>

                        <!-- Order Content -->
                        <div class="g-pa-20">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="mb-4">
                                        <h3 class="h5 mb-1 <?= $orden->STATUS_VENTA == 'CANCELADA' ? 'text-red' : 'text-green' ?>">
                                            {{ $orden->STATUS_VENTA }}
                                        </h3>
                                    </div>
                                    @foreach($productos as $producto)
                                        @for($i = 0; $i< count($producto); $i++)
                                            @if($producto[$i]->ID_VENTA == $orden->ID_VENTA)
                                            <div class="row">
                                                <div class="col-4 col-sm-3 g-mb-30">
                                                    @if(substr($producto[$i]->IMAGEN, 0, 4) != 'http')
                                                        <img class="img-fluid" src="{{ 'http://creatmos.net'.str_replace('~', '', $producto[$i]->IMAGEN) }}" alt="imagen del producto">
                                                    @else
                                                        <img class="img-fluid" src="{{ $producto[$i]->IMAGEN }}" alt="imagen del producto">
                                                    @endif
                                                </div>

                                                <div class="col-8 col-sm-9 g-mb-30">
                                                    <h4 class="h6 g-font-weight-400">
                                                        <a href="#!">
                                                            {{ $producto[$i]->DESCRIPCION }}
                                                        </a>
                                                    </h4>
                                                    <span class="d-block g-color-gray-dark-v4 g-font-size-13 mb-2">{{ $producto[$i]->TALLA }}</span>
                                                    <span class="d-block g-color-gray-dark-v4 g-font-size-13 mb-2">x {{ $producto[$i]->CANTIDAD_VENDIDA }}</span>
                                                    <span class="d-block g-color-lightred mb-2">${{ number_format( $producto[$i]->PRECIO ,2) }}</span>
                                                </div>
                                            </div>
                                            @endif
                                        @endfor
                                    @endforeach
                                </div>

                                <div class="col-md-4">
                                    <div class="row">
                                        @if( $orden->STATUS_VENTA === 'CANCELADA' )
                                            <!-- <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4 not-active" href="#!">Rastrear Envío</a> -->
                                            <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4 not-active" href="#!">Detalles de envio</a>
                                        @else
                                            <!-- <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4" href="http://tracking.muletta.com/">Rastrear Envío</a> -->
                                            <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4" href="{{route('detailsVenta', $orden->ID_VENTA)}}">Detalles de envio</a>
                                        @endif

                                        @if( $orden->STATUS_VENTA === 'PENDIENTE DE PAGO' )
                                            <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4" href="{{route('reintentarPago', $orden->ID_VENTA)}}">Reintentar pago</a>
                                        @endif
                                    </div>
                                    <?php
                                        $fecha = explode('/',$orden->FECHA_VENTA);
                                        $date_venta = new DateTime($fecha[2].'-'.$fecha[1].'-'.$fecha[0]);
                                        $fecha_actual = new DateTime('now');
                                        $diff = $date_venta->diff($fecha_actual);   
                                    ?>
                                    @if( $orden->STATUS_VENTA === 'COMPLETADA' && $diff->days <= 31 && $orden->ID_DEVOLUCION_WEB == null)
                                        <div class="row">
                                            <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4" href="{{route('proceso-devolucion', $orden->ID_VENTA)}}">Cambio o Devolución</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                            <!-- End Order Content -->
                    </div>
                    <!-- End Order Block -->
                @endforeach
                
            </div>
            <!-- Orders -->
        </div>
    </div>
    <!-- Fin mi Cuenta -->
@endsection
