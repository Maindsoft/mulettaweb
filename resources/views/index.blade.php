@extends('template.main')

@section('title', 'Inicio')

@section('content')
    <!-- Start: Slider Principal -->

    
    @if(count($imagenes) > 0)
        <div id="primer-modal" class="slider center slick-slider" style="margin-top: -19px">
            @foreach($imagenes as $key => $imagen)
                <div>
                    <a href="{{ $imagen->URL ? $imagen->URL : '#' }}">
                        <img data-lazy="{{ 'https://creatmos.net/'.$imagen->RUTA }}" class="image-change-img"  width="1920" height="1080">
                    </a>
                </div>
            @endforeach
        </div>
    @endif

    @if(count($img_categorias) > 0)
        <section class="div_categoria">
            @foreach($img_categorias as $key => $value)
                <div>
                    <a href="{{ $value->URL }}">
                        <img class="photo card-img" data-src="{{ 'https://creatmos.net/'.$value->RUTA }}" alt="Card image">
                        @if($value->TEXTO)
                            <h1><?=$value->TEXTO?></h1>
                        @endif
                    </a>
                </div>
            @endforeach
        </section>
    @endif

    @if(count($new_arrivals) > 0)
        <section id="index-seccion-7">
            <div class="container-flex div_banner">
                <a href="{{ $new_arrivals[0]->URL }}">
                    <div class="card flex-md-row banner-330" style="background: url(<?='https://creatmos.net/'.$new_arrivals[0]->RUTA?>);">
                        @if($new_arrivals[0]->TEXTO)
                            <h1><?=$new_arrivals[0]->TEXTO?></h1>
                        @endif
                    </div>
                </a>
            </div>
        </section>
    @endif

    @if(count($productos) > 0)
        <section class="showcase">
            <div id="product-index-carousel1" class="slider center slick-slider">
                @foreach($productos as $key => $value)
                    @if($value->POSICION == 1)
                        <a href="{{ route('producto_individual', ['DESCRIPCION_MODELO' => str_replace(' ','-',str_replace('/','-',$value->DESCRIPCION_MODELO)), 'ID_MODELO' => $value->ID_MODELO])}}">
                            <img data-lazy="<?=$value->IMAGEN_100 ? str_replace('~', 'https://creatmos.net', $value->IMAGEN_100) : asset('assets/global/img/index/temporada-7/placeholder-500x620.png')?>" class="image-change-img">

                            <div class="text-uppercase text-center txt-muletta-oro">
                                <br>
                                <h2><?=$value->DESCRIPCION_MODELO?></h2>
                                @if($value->PRECIO_VENTA_DESCUENTO == NULL || $value->PRECIO_VENTA == $value->PRECIO_VENTA_DESCUENTO)
                                    <h3>$<?=$value->PRECIO_VENTA?></h3>
                                @else
                                    <h3> <span class="txt-muletta-gris-claro" style="text-decoration:line-through;"> $<?=$value->PRECIO_VENTA?> </span> $<?=$value->PRECIO_VENTA_DESCUENTO?></h3>
                                @endif
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>
        </section>
    @endif

    <section class="compra_segura margin-top">
        <div class="envio text-center">
            <img src="{{ asset('assets/global/img/mail/Entregado.png') }}" width="14%" height="14%">
            <br>
            <h5>ENVÍO GRATIS</h5>
            <h6>a partir de $999</h6>
            <h6>costo envío $99</h6>
        </div>
        <div class="seguro text-center">
            <img src="{{ asset('assets/global/img/compras/PAGOSEGURO_MULETTA2.png') }}" style="width: 45%;">
            <br>
            <h5>MERCADO PAGO</h5>
        </div>
        <div class="entrega text-center">
            <img src="{{ asset('assets/global/img/mail/Envio.png') }}" width="14%" height="14%">
            <br>
            <h5>TIEMPO DE ENTREGA</h5>
            <h6>de 3 a 5 días hábiles</h6>
            <h6>Envíos Nacionales</h6>
        </div>
    </section>

    <section id="index-seccion-4" class="compra_segura2 margin-top">

        <div class="container-fluid">
            <div class="carousel-wrap" style="padding: 0 2% !important;">
                <div class="owl-carousel envios">
                    <div class="item precio_envio_carrusel" align="center">
                        <img src="{{ asset('assets/global/img/mail/Entregado.png') }}">
                        
                        <h5>ENVÍO GRATIS</h5>
                        <h6>a partir de $999</h6>
                        <h6>costo envío $99</h6>
                    </div>
                    <div class="item mercado_pago_carrusel" align="center">
                        <img src="{{ asset('assets/global/img/compras/PAGOSEGURO_MULETTA2.png') }}">
                        <br>
                        <h5>Mercado Pago</h5>
                    </div>
                    <div class="item tiempo_envio_carrusel" align="center">
                        <img src="{{ asset('assets/global/img/mail/Envio.png') }}">
                        
                        <h5>TIEMPO DE ENTREGA</h5>
                        <h6>de 3 a 5 días hábiles</h6>
                        <h6>Envíos Nacionales</h6>
                    </div>
                </div>
            </div>
        </div>

    </section>



    @if(count($promociones) > 0)
        <section class="showcase">
            <div id="promociones" class="slider center slick-slider">
                @foreach($promociones as $key => $value)
                    <a href="<?=$value->URL?>" class="div_banner">
                        <div>
                            <img data-lazy="{{ 'https://creatmos.net/'.$value->RUTA }}" class="image-change-img">
                            @if($value->TEXTO)
                                <h1><?=$value->TEXTO?></h1>
                            @endif
                        </div>
                    </a>
                @endforeach
            </div>
        </section>
    @endif


    @if(count($demand) > 0)
    <section id="index-seccion-7" >
        <div class="container-flex div_banner">
            <a href="{{ $demand[0]->URL }}">
                <div class="card flex-md-row banner-330" style="background: url(<?='https://creatmos.net/'.$demand[0]->RUTA?>);">
                    @if($demand[0]->TEXTO)
                        <h1><?=$demand[0]->TEXTO?></h1>
                    @endif
                </div>
            </a>
        </div>
    </section>
    @endif

    @if(count($productos) > 0)
        <section class="showcase">
            <div id="product-index-carousel2" class="slider center slick-slider">
                @foreach($productos as $key => $value)
                    @if($value->POSICION == 2)
                        <a href="{{ route('producto_individual', ['DESCRIPCION_MODELO' => str_replace(' ','-',str_replace('/','-',$value->DESCRIPCION_MODELO)), 'ID_MODELO' => $value->ID_MODELO])}}">
                            <img data-lazy="<?=$value->IMAGEN_100 ? str_replace('~', 'https://creatmos.net', $value->IMAGEN_100) : asset('assets/global/img/index/temporada-7/placeholder-500x620.png')?>" class="image-change-img">

                            <div class="text-uppercase text-center txt-muletta-oro">
                                <br>
                                <h2><?=$value->DESCRIPCION_MODELO?></h2>
                                @if($value->PRECIO_VENTA_DESCUENTO == NULL || $value->PRECIO_VENTA == $value->PRECIO_VENTA_DESCUENTO)
                                    <h3>$<?=$value->PRECIO_VENTA?></h3>
                                @else
                                    <h3> <span class="txt-muletta-gris-claro" style="text-decoration:line-through;"> $<?=$value->PRECIO_VENTA?> </span> $<?=$value->PRECIO_VENTA_DESCUENTO?></h3>
                                @endif
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>
        </section>
    @endif

    @if(count($showroom) > 0)
        <section id="index-seccion-7" class="margin-top">
            <div class="container-flex div_banner">
                <a href="{{ $showroom[0]->URL }}">
                    <div class="card flex-md-row banner-330" style="background: url(<?='https://creatmos.net/'.$showroom[0]->RUTA?>);">
                        @if($showroom[0]->TEXTO)
                            <h1><?=$new_arrivals[0]->TEXTO?></h1>
                        @endif
                    </div>
                </a>
            </div>
        </section>
    @endif

    @if(count($emprendedor) > 0)
        <section id="index-seccion-7">
            <div class="container-flex div_banner">
                <a href="{{ $emprendedor[0]->URL }}">
                    <div class="card flex-md-row banner-330" style="background: url(<?='https://creatmos.net/'.$emprendedor[0]->RUTA?>);">
                        @if($emprendedor[0]->TEXTO)
                            <h1><?=$emprendedor[0]->TEXTO?></h1>
                        @endif
                    </div>
                </a>
            </div>
        </section>
    @endif

    <section class="text-center">
        <h1 class="txt-comunidad txt-muletta-oro">#COMUNIDADMULETTA</h1>
    </section>

    <section>
        <div class="grid_comunidad div_banner">
            @foreach($comunidad as $key => $value)
                <div class="grid_imagen">
                    <a href="{{ $value->URL }}">
                        <img class="photo" data-src="{{ 'https://creatmos.net/'.$value->RUTA }}" alt="">
                        @if($value->TEXTO)
                            <h1><?=$value->TEXTO?></h1>
                        @endif
                    </a>
                </div>
            @endforeach
        </div>
    </section>
@endsection
