<?php
$code = app()->isDownForMaintenance() ? 'maintenance' : $exception->getStatusCode();
?>

<html lang="{!! app()->getLocale() !!}">
@extends('template.main')

@section('title', 'Error 500')

@section('content')
<!-- Breadcrumbs -->
<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
        <ul class="u-list-inline">
            <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
            </li>
            
            
            <li class="list-inline-item ">
                <span>500 Error interno de servidor</span>
            </li>
        </ul>
    </div>
</section>
<!-- End Breadcrumbs -->

<div class="container text-center g-py-50" style="max-width: 500px;">
    <div class="mb-5">
        <span class="d-block g-color-gray-light-v1 g-font-size-70 g-font-size-90--md mb-4">                        
            <i class="fa fa-fire">
            </i>
        </span>

        <h2 class="g-mb-30">
            500 Error interno de servidor
        </h2>
        <p class="lead text-center">
        El servidor web está devolviendo un error interno para <em>Muletta</em>.
        </p>  
        <p class="lead text-center">{!! config('app.env') === 'production' ? trans("server-error-pages::server-error-pages.$code.description", ['domain'=> request()->getHost()]) : $exception->getMessage() ?? trans("server-error-pages::server-error-pages.$code.description", ['domain'=> request()->getHost()]) !!}
                               </p>
        
        
        <div class="">
           <hr class="g-brd-gray-light-v3 g-my-15">
         <p class="g-font-size-18 mb-0 text-center">
          <a class="text-center g-color-black g-color-primary--hover g-text-no-underline--hover" href="https://version1.muletta.com/">
                       <span class="">Prueba con la pagina previa de Muletta</span> →
              </a>
          </p>
          <hr class="g-brd-gray-light-v3 g-my-15">
        </div>

            <div style="font-family: Rufina;max-height: 200px;" class="g-font-size-180  g-font-weight-600er g-color-gray-light-v4">
                500
            </div>

            <div class="row">
                <div class="col-12 accordeon-ctm-wraper">
                   
                    <ul class="accordion-ctm">
                        <li class="accordion-ctm-item">
                            <a href="#!">
                                ¿Que pasó?
                                <i class=" ml-auto  fas fa-chevron-down"></i>
                            </a>
                            <div class="content">
                                <p>
                                    Un estado de error 500 implica que hay un problema con el software del servidor web que causa un mal funcionamiento.
                                </p>
                            </div>
                            
                        </li>
                        <li class="accordion-ctm-item">
                            <a href="#!">
                                ¿Que puedo hacer?
                                <i class="ml-auto  fas fa-chevron-down"></i></a>
                            <div class="content">

                                <ul>
                                    <strong>
                                        Si eres un visitante del sitio
                                    </strong>
                                    
                                    <li>
                                        Puedes probar usando la version previa de nuestro sitio web. Es más estable y cuenta con todas las funcionalidades. <br><br>Si necesita asistencia inmediata, envíenos un correo electrónico. Nos disculpamos por cualquier inconveniente.
                                    </li>
                                    <br>
                                    <strong>
                                        Si eres el dueño/staff del sitio
                                    </strong>
                                    
                                    <li>
                                        Este error solo puede ser solucionado por los administradores del servidor, comuníquese con el proveedor de su sitio web.
                                    </li>
                                    
                                </ul>
                            </div><!-- .content -->
                        </li><!-- .accordion-ctm-item -->

                      
                    </ul><!-- .accordion-ctm -->
                </div> <!-- .col-12 accordeon-ctm-wraper -->
            </div><!-- .row -->
    </div><!-- .mb-5 -->
</div><!-- .container text-center g-py-100 -->
@endsection
