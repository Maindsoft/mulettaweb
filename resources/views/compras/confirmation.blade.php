 
@extends('template.main')

@section('title', 'Confirmar Pago')

@section('css')
   
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
          integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
          crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
          integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
          crossorigin="anonymous"/>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Nunito:400|Nunito:400');

        html {
            font-size: 100%;
        }

        body {
            background-color: white;
            font-family: 'Nunito', sans-serif;
            font-weight: 400;
            line-height: 1.65;
            color: #333;
        }

        p {
            margin-bottom: 1.15rem;
        }

        h1, h2, h3, h4, h5 {
            margin: initial;
            font-family: 'Nunito', sans-serif;
            font-weight: 400;
            line-height: 1.15;
        }

        h1 {
            margin-top: 0;
            font-size: 3.052rem;
        }

        h2 {
            font-size: 2.441rem;
        }

        h3 {
            font-size: 1.953rem;
        }

        h4 {
            font-size: 1.563rem;
        }

        h5 {
            font-size: 1.25rem;
        }

        small, .text_small {
            font-size: 0.8rem;
        }

        .btn-primay {
            background-color: #b78b1e !important;
        }
    </style>


@endsection

@section('content')
 
<div class="container w-75 p-1">
    @if ($request_nbResponse == 'Aprobado')
        <div class="card m-4 text-center card border-success animate__animated animate__fadeIn">
            <div class="card-header">
                <i class="fas fa-check-circle fa-5x text-success animate__animated animate__zoomIn"></i>
            </div>
            <div class="card-body">
                <h3 class="card-title text-success text-uppercase animate__animated animate__fadeInDown">{{$request_nbResponse}}</h3>
                <img width="150px" height="auto" src="{{ asset('assets/global/img/logo/logo-muletta-header.png') }}">
                <p class="card-text">
                <ul class="list-group">
                    <li class="list-group-item p-1"><small
                                class="font-weight-bold">REFERENCIA</small></br>{{$request_referencia}}</li>
                    <li class="list-group-item p-1"><small class="font-weight-bold">IMPORTE</small></br><h3>
                            ${{$request_importe}} {{$request_nbMoneda}}</h3></li>
                    <li class="list-group-item p-1"><small class="font-weight-bold">EMAIL</small></br>{{$request_email}}
                    </li>
                    <li class="list-group-item p-1"><strong class="text-success"><small class="font-weight-bold">REFERENCIA</small></br>{{$request_nuAut}}
                        </strong></li>
                </ul>
                <br>
                <img width="200px" height="auto"
                     src="{{ asset('assets/global/img/index/03_Logo_webpay_plus-300px.png') }}">
                </p>
            </div>
            <dvi class="card-footer">
                <div class="row">
                    <div class="col-12 text-center boder-0">
                        <a id="cerrarCart" class="btn btn-success w-100 text-white animate__animated animate__fadeInUp"
                           href="#">Aceptar</a>
                    </div>
                </div>
            </dvi>
        </div>
    @elseif($request_nbResponse == 'Rechazado')
        @if($request_nb_error == "FONDOS INSUFICIENTES")
            <div class="card m-4 text-center card border-danger animate__animated animate__fadeIn">
                <div class="card-header">
                    <i class="fas fa-times-circle fa-5x text-danger animate__animated animate__zoomIn"></i>
                </div>
                <div class="card-body">
                    <h3 class="card-title text-danger text-uppercase animate__animated animate__fadeInDown">{{$request_nbResponse}}</h3>
                    <img width="150px" height="auto"
                         src="{{ asset('assets/global/img/logo/logo-muletta-header.png') }}">
                    <p class="card-text  animated fadeIn">
                    <ul class="list-group  animated fadeIn">
                        <li class="list-group-item p-1"><small
                                    class="font-weight-bold">REFERENCIA</small></br>{{$request_referencia}}</li>
                        <li class="list-group-item p-1"><small class="font-weight-bold">IMPORTE</small></br><h3>
                                ${{$request_importe}} {{$request_nbMoneda}}</h3></li>
                        <li class="list-group-item p-1"><small
                                    class="font-weight-bold">EMAIL</small></br>{{$request_email}}</li>

                        <li class="list-group-item p-1"><strong class="text-danger"><small class="font-weight-bold">MOTIVO</small></br>{{$request_nb_error}}
                            </strong></li>
                    </ul>
                    <br>
                    <img width="200px" height="auto"
                         src="{{ asset('assets/global/img/index/03_Logo_webpay_plus-300px.png') }}">
                    </p>
                </div>
                <dvi class="card-footer">
                    <div class="row">
                        <div class="col-12 text-center boder-0">
                            <a class="winCerrar btn btn-danger w-100 text-white animate__animated animate__fadeInUp"
                               href="/compras/checkout">Reintentar</a>
                        </div>
                    </div>
                </dvi>
            </div>
        @else
            <div class="card m-4 text-center card border-danger animate__animated animate__fadeIn">
                <div class="card-header">
                    <i class="fas fa-exclamation-circle fa-5x text-danger animate__animated animate__zoomIn"></i>
                </div>
                <div class="card-body">
                    <h3 class="card-title text-danger text-uppercase animate__animated animate__fadeInDown">{{$request_nbResponse}}</h3>
                    <img width="150px" height="auto"
                         src="{{ asset('assets/global/img/logo/logo-muletta-header.png') }}">
                    <p class="card-text  animated fadeIn">
                    <ul class="list-group  animated fadeIn">
                        <li class="list-group-item p-1"><small
                                    class="font-weight-bold">REFERENCIA</small></br>{{$request_referencia}}</li>
                        <li class="list-group-item p-1"><small class="font-weight-bold">IMPORTE</small></br><h3>
                                ${{$request_importe}} {{$request_nbMoneda}}</h3></li>
                        <li class="list-group-item p-1"><small
                                    class="font-weight-bold">EMAIL</small></br>{{$request_email}}</li>
                        <li class="list-group-item p-1"><strong class="text-danger"><small class="font-weight-bold">MOTIVO</small></br>{{$request_nb_error}}
                            </strong></li>
                    </ul>
                    <br>
                    <img width="200px" height="auto"
                         src="{{ asset('assets/global/img/index/03_Logo_webpay_plus-300px.png') }}">
                    </p>
                </div>
                <dvi class="card-footer">
                    <div class="row">
                        <div class="col-12 text-center boder-0">
                            <a class=" btn btn-danger w-100 text-white animate__animated animate__fadeInUp"
                               href="/compras/checkout">Reintentar</a>
                        </div>
                    </div>
                </dvi>
            </div>
        @endif
    @endif
</div>
@endsection

@section('script')
 
    <script src="{{ asset('assets/global/js/maindsoft/carrito.js') }}"></script>

@endsection

 

