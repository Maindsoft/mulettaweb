@extends('template.main')

@section('title', ucfirst( $nombre ))

<?php
    $host= $_SERVER["HTTP_HOST"];
    $url= $_SERVER["REQUEST_URI"];
?>

@section('meta_title', ucfirst( $nombre ))

@section('meta_url', "http://".$host.$url)

@section('content')

   
    <!-- Start: Slider Principal -->
    <div class="box ">
        <div class="slider-box ratio-9-16" id="sb_1" data-auto-slide="true" data-speed="5000">
            <div class="slider-content">
                @if (count($imagenes) > 0)
                    @foreach($imagenes as $key => $value)
                        @if($value->POSISION == 1)
                            <div class="slider-item" data-background-url="<?='http://creatmos.net/'.str_replace(' ', '', $value->RUTA)?>">
                                <h3>Slide 1</h3>
                            </div>
                        @endif
                    @endforeach
                @else
                    <div class="slider-item" data-background-url="http://via.placeholder.com/1350x680"> </div>
                @endif
            </div>
            <div class="slider-pagin"></div>
            <div class="slider-fillbar"></div>
            <div class="slider-background"></div>
        </div>
    </div>
    <!-- End: Slider Principal -->

    
    <!-- Start: Edición especial
    <section id="index-seccion-02">
        <div class="container-flex margin-top">
            <div class="row">
                <div class="col g-hidden-md-up-v2">
                    <div class="card">
                        @if (count($imagenes) > 0)
                            @foreach($imagenes as $key => $value)
                                @if($value->POSISION == 2)
                                    <img class="photo card-img-top" data-src="{{ $value->RUTA }}" alt="Card image">
                                @endif
                            @endforeach
                        @else
                            <img class="photo card-img-top" data-src="http://via.placeholder.com/780X350" alt="Card image">
                        @endif
                        <div class="card-body">
                            <p class="card-text mb-auto"></p>
                            <h2 class="muleta-side-text">
                                EDICIÓN <br><span>LIMITADA</span>
                            </h2>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card flex-md-row mb-4  h-md-250 g-hidden-md-down-v2">
                        <div class="slider-box" id="sb_2">
                            @if (count($imagenes) > 0)
                                @foreach($imagenes as $key => $value)
                                    @if($value->POSISION == 2)
                                        <img class="photo card-img-top" data-src="{{ $value->RUTA }}" alt="Card image">
                                    @endif
                                @endforeach
                            @else
                                <img class="photo card-img-top" data-src="http://via.placeholder.com/890X400" alt="Card image">
                            @endif
                        </div>

                        <div class="card-body d-flex flex-column align-items-center">
                            <p class="card-text mb-auto"></p>
                            <h2 class="muleta-side-text">
                                EDICIÓN <br><span>LIMITADA</span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    End: Edición especial -->

    <!-- Start: SubCategorias -->
    <section id="index-seccion-3">
        <div class="container margin-top">

            <?php
                $menuSubNivel = session()->get('menuSubNivel');
                $aux = 0;
            ?>

            <div class="row ">
                @if (count($imagenes) > 0)
                    @foreach($imagenes as $key => $value)
                        @if($value->POSISION == 3)
                            @if($aux < 2)
                                <?php
                                    $aux++;
                                ?> <!-- vista version desktop -->
                                <div class="col px-5 py-5 g-hidden-md-down-v2">

                                    <div class="card  text-white ">
                                        @foreach($menuSubNivel as $sub)
                                            @foreach($sub as $s)

                                                @if($s->ID_CATEGORIA_FLEXIBLE == $value->ID_CATEGORIA_FLEXIBLE)
                                                    <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $value->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA]) }}" style="color: white;">
                                                        <img class="photo card-img" data-src="<?=$value->RUTA?>" alt="Card image">
                                                        <div class="card-img-overlay ">
                                                            <div class="centered">
                                                                <h2><?=strtoupper($s->NOMBRE)?></h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                                <!-- vista version movil -->
                                <div class=" px-5 py-5 g-hidden-md-up-v2">
                                    <div class="card  text-white ">
                                        @foreach($menuSubNivel as $sub)
                                            @foreach($sub as $s)
                                                @if($s->ID_CATEGORIA_FLEXIBLE == $value->ID_CATEGORIA_FLEXIBLE)
                                                    <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $value->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA]) }}" style="color: white;">
                                                        <img class="photo card-img" data-src="<?=$value->RUTA?>" alt="Card image">
                                                        <div class="card-img-overlay ">
                                                            <div class="centered">
                                                                <h2><?=strtoupper($s->NOMBRE)?></h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endif
                    @endforeach
                @else
                    <!--SINO: cargar 4 tarjetas con placeholder images-->
                    <div class="row ">
                        <!-- vista version desktop -->
                        <div class="col px-5 py-5 g-hidden-md-down-v2">
                            <div class="card  text-white ">
                                <a href="http://mulettaweb2.me:82/categoria/18/mujer-camisas" style="color: white;">
                                    <img class="photo card-img" data-src="http://via.placeholder.com/545x700" alt="Card image">
                                    <div class="card-img-overlay ">
                                        <div class="centered">
                                            <h2>Categoria A</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- vista version movil -->
                        <div class=" px-5 py-5 g-hidden-md-up-v2">
                            <div class="card  text-white ">
                                <a href="http://mulettaweb2.me:82/categoria/18/mujer-camisas" style="color: white;">
                                    <img class="photo card-img" data-src="http://via.placeholder.com/333x444" alt="Card image">
                                    <div class="card-img-overlay ">
                                        <div class="centered">
                                            <h2>Categoria a</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- vista version desktop -->
                        <div class="col px-5 py-5 g-hidden-md-down-v2">

                            <div class="card  text-white ">

                                <a href="http://mulettaweb2.me:82/categoria/21/mujer-polos" style="color: white;">
                                    <img class="photo card-img" data-src="http://via.placeholder.com/545x700" alt="Card image">
                                    <div class="card-img-overlay ">
                                        <div class="centered">
                                            <h2>Categoria B</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- vista version movil -->
                        <div class=" px-5 py-5 g-hidden-md-up-v2">
                            <div class="card  text-white ">
                                <a href="http://mulettaweb2.me:82/categoria/21/mujer-polos" style="color: white;">
                                    <img class="photo card-img" data-src="http://via.placeholder.com/333x444" alt="Card image">
                                    <div class="card-img-overlay ">
                                        <div class="centered">
                                            <h2>Categoria b</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row ">
                        <!-- vista version desktop -->
                        <div class="col px-5 py-5 g-hidden-md-down-v2">

                            <div class="card  text-white ">
                                <a href="http://mulettaweb2.me:82/categoria/25/mujer-chalecos" style="color: white;">
                                    <img class="photo card-img" data-src="http://via.placeholder.com/545x700g" alt="Card image">
                                    <div class="card-img-overlay ">
                                        <div class="centered">
                                            <h2>Categoria C</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- vista version movil -->
                        <div class=" px-5 py-5 g-hidden-md-up-v2">
                            <div class="card  text-white ">
                                <a href="http://mulettaweb2.me:82/categoria/25/mujer-chalecos" style="color: white;">
                                    <img class="photo card-img" data-src="http://via.placeholder.com/333x444" alt="Card image">
                                    <div class="card-img-overlay ">
                                        <div class="centered">
                                            <h2>Categoria c</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- vista version desktop -->
                        <div class="col px-5 py-5 g-hidden-md-down-v2">

                            <div class="card  text-white ">
                                <a href="http://mulettaweb2.me:82/categoria/27/mujer-accesorios" style="color: white;">
                                    <img class="photo card-img" data-src="http://via.placeholder.com/545x700" alt="Card image">
                                    <div class="card-img-overlay ">
                                        <div class="centered">
                                            <h2>Categoria D</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- vista version movil -->
                        <div class=" px-5 py-5 g-hidden-md-up-v2">
                            <div class="card  text-white ">
                                <a href="http://mulettaweb2.me:82/categoria/27/mujer-accesorios" style="color: white;">
                                    <img class="photo card-img" data-src="http://via.placeholder.com/333x444" alt="Card image">
                                    <div class="card-img-overlay ">
                                        <div class="centered">
                                            <h2>Categoria d</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row ">
                @foreach($imagenes as $key => $value)
                    @if($value->POSISION == 3)
                        <?php
                            $aux++;
                        ?>
                        @if($aux > 4)
                        <!-- vista version desktop -->
                        <div class="col px-5 py-5 g-hidden-md-down-v2">
                            <div class="card  text-white ">
                                @foreach($menuSubNivel as $sub)
                                    @foreach($sub as $s)
                                        @if($s->ID_CATEGORIA_FLEXIBLE == $value->ID_CATEGORIA_FLEXIBLE)
                                            <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $value->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA]) }}" style="color: white;">
                                                <img class="photo card-img" data-src="<?=$value->RUTA?>" alt="Card image">
                                                <div class="card-img-overlay ">
                                                    <div class="centered">
                                                        <h2><?=strtoupper($s->NOMBRE)?></h2>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                        <!-- vista version movil -->
                        <div class=" px-5 py-5 g-hidden-md-up-v2">
                            <div class="card  text-white ">
                                @foreach($menuSubNivel as $sub)
                                    @foreach($sub as $s)
                                        @if($s->ID_CATEGORIA_FLEXIBLE == $value->ID_CATEGORIA_FLEXIBLE)
                                            <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $value->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA]) }}" style="color: white;">
                                                <img class="photo card-img" data-src="<?=$value->RUTA?>" alt="Card image">
                                                <div class="card-img-overlay ">
                                                    <div class="centered">
                                                        <h2><?=strtoupper($s->NOMBRE)?></h2>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>
    </section>
    <!-- End: SubCategorias -->
    @if(count($productos) > 0)
        <!-- Start: Destacados -->
        <section id="index-seccion-4">
            <div class="container-fluid margin-top">
                <div class="row">
                    <div class="col txt-muletta-gris mbot-20 text-center">
                        <h2>
                            DESTACADOS
                        </h2>
                    </div>
                </div>
                <div class="carousel-wrap">
                    <div class="owl-carousel product-carousel">
                        
                        @if($productos === 'Sin resultados')
                            <a href="#!">
                                <div class="item  container-change-img">
                                    <img data-src="photo http://via.placeholder.com/500x620" class="image-change-img">
                                    <div class="overlay-change-img">
                                        <img data-src="photo http://via.placeholder.com/511x621" class="snd-img">
                                    </div>

                                    <div class="product-content text-center">
                                        <h2>NOMBRE</h2>
                                        <span>$999</span>
                                    </div>
                                </div>
                            </a>
                        @else
                            @foreach($productos as $key => $value)
                                <a href="{{ route('producto_individual', ['DESCRIPCION_MODELO' => $value->DESCRIPCION_MODELO, 'ID_MODELO' => $value->ID_MODELO])}}">
                                    <div class="item  container-change-img">
                                        @if(substr($value->IMAGEN_100, 0, 1) == '~')
                                            <img src="<?=$value->IMAGEN_100 ? str_replace("~","http://creatmos.net", $value->IMAGEN_100) : 'http://via.placeholder.com/500x620'?>" class="image-change-img">
                                        @else
                                            <img src="<?=$value->IMAGEN_100 ? $value->IMAGEN_100 : 'http://via.placeholder.com/500x620'?>" class="image-change-img">
                                        @endif
                                        <div class="overlay-change-img">
                                            @if(substr($value->IMAGEN_100, 0, 1) == '~')
                                                <img src="<?=$value->IMAGEN_101 ? str_replace("~","http://creatmos.net", $value->IMAGEN_101) : 'http://via.placeholder.com/500x620'?>" class="snd-img">
                                            @else
                                                <img src="<?=$value->IMAGEN_101 ? $value->IMAGEN_101 : 'http://via.placeholder.com/500x620'?>" class="snd-img">
                                            @endif
                                        </div>

                                        <div class="product-content text-center margin-top">
                                            <h2><?=$value->DESCRIPCION_MODELO?></h2>
                                            <span>$<?=$value->PRECIO_VENTA?></span>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <!-- End: Destacados -->
    @endif

    <!-- Start: Rebajas -->
    <section id="index-seccion-6">
        <div class="container-flex margin-top">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card flex-md-row mb-4  h-md-250">
                        @foreach($imagenes as $key => $value)
                            @if($value->POSISION == 4)
                                @foreach($menuSubNivel as $sub)
                                    @foreach($sub as $s)
                                        @if($s->ID_CATEGORIA_FLEXIBLE == $value->ID_CATEGORIA_FLEXIBLE)
                                        <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $value->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA]) }}" style="color: white;">
                                            <img class="photo card-img" data-src="<?=$value->RUTA?>" alt="Card image">
                                            <div class="card-img-overlay">
                                                <div class="centered centered text-center">
                                                    <h2 class="txt-white" style="font-family: 'Rufina', sans-serif !important;">REBAJAS</h2>
                                                    <p class=" text-center">HASTA 60% DE DESCUENTO</p>
                                                </div>
                                            </div>
                                        </a>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid g-mt-50 g-brd-bottom g-brd-gray-light-v4 ">
            <div class="row">
                <div class="col-md-4 g-mb-30">
                    <div class="text-center">
                        <h3 class="text-uppercase g-font-prompt ">Envío GRATIS</h3>
                        <p class="g-font-prompt ">A partir de $999<br>Costo envío $100</p>
                    </div>
                </div>
                <div class="col-md-4 g-mb-30">
                    <div class="text-center">
                        <h3 class="text-uppercase g-font-prompt  ">TIEMPO DE ENTREGA</h3>
                        <p class="g-font-prompt">De 2 a 4 días hábiles<br>Envíos nacionales</p>
                    </div>
                </div>
                <div class="col-md-4 g-mb-30">
                    <div class="text-center">
                        <h3 class="text-uppercase g-font-prompt ">PAGO SEGURO</h3>
                        <p class="g-font-prompt">VISA | Mastercard<br>Paypal | Mercado Pago</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End: Rebajas -->

    <!-- Start: Explorar -->
    <section id="index-seccion-5">
        <div class="container-fluid margin-top">
            <div class="row">
                <div class="col txt-muletta-gris mbot-20 text-center">
                    <h2>
                        EXPLORAR
                    </h2>
                </div>
            </div>
            <div class="row no-gutters">
                <!--vista version desktop-->
                <div class="col-6 g-hidden-md-down-v2">
                    <div class="card bg-dark-blue text-white">
                        @if($id_tipo_producto == 17 || $id_tipo_producto == 40 )
                            <a href="/hombre" style="color: white;">
                                <img class="photo card-img" data-src="{{ asset('assets/global/img/index/index-fondo-explora-hombre.jpg') }}" alt="Card image">

                                <div class="card-img-overlay">
                                    <div class="centered">
                                        <h2>HOMBRE</h2>
                                    </div>
                                </div>
                            </a>
                        @else
                            <a href="/mujer" style="color: white;">
                                <img class="photo card-img" data-src="{{ asset('assets/global/img/index/Mujer1.jpg') }}" alt="Card image">

                                <div class="card-img-overlay">
                                    <div class="centered">
                                        <h2>MUJER</h2>
                                    </div>
                                </div>
                            </a>
                        @endif
                    </div>
                </div>
                <!--vista version movil-->
                <div class="col-12 g-hidden-md-up-v2">
                    <div class="card bg-dark-blue text-white">
                        @if($id_tipo_producto == 17 || $id_tipo_producto == 40 )
                        <a href="/hombre" style="color: white;">
                            <img class="photo card-img" data-src="{{ asset('assets/global/img/index/index-fondo-explora-hombre.jpg') }}" alt="Card image">

                            <div class="card-img-overlay">
                                <div class="centered">
                                    <h2>HOMBRE</h2>
                                </div>
                            </div>
                        </a>
                        @else
                        <a href="/mujer" style="color: white;">
                            <img class="photo card-img" data-src="{{ asset('assets/global/img/index/Mujer1.jpg') }}" alt="Card image">

                            <div class="card-img-overlay">
                                <div class="centered">
                                    <h2>MUJER</h2>
                                </div>
                            </div>
                        </a>
                        @endif
                    </div>
                </div>
                @if($id_tipo_producto != 40)
                    <!--vista version desktop-->
                    <div class="col-6 g-hidden-md-down-v2">
                        <a href="/outlet" style="color: white;">
                            <div class="card bg-dark-blue text-white">
                                <img class="photo card-img" data-src="{{ asset('assets/global/img/index/Outlet.jpg') }}" alt="Card image">
                                <div class="card-img-overlay">
                                    <div class="centered">
                                        <h2>OUTLET</h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!--vista version movil-->
                    <div class="col-12 g-hidden-md-up-v2">
                        <a href="/outlet" style="color: white;">
                            <div class="card bg-dark-blue text-white">
                                <img class="photo card-img" data-src="{{ asset('assets/global/img/index/Outlet.jpg') }}" alt="Card image">
                                <div class="card-img-overlay">
                                    <div class="centered">
                                        <h2>OUTLET</h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @else
                    <!--vista version desktop-->
                    <div class="col-6 g-hidden-md-down-v2">
                        <a href="/mujer" style="color: white;">
                            <img class="photo card-img" data-src="{{ asset('assets/global/img/index/Mujer1.jpg') }}" alt="Card image">

                            <div class="card-img-overlay">
                                <div class="centered">
                                    <h2>MUJER</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!--vista version movil-->
                    <div class="col-12 g-hidden-md-up-v2">
                        <a href="/mujer" style="color: white;">
                            <img class="photo card-img" data-src="{{ asset('assets/global/img/index/Mujer1.jpg') }}" alt="Card image">

                            <div class="card-img-overlay">
                                <div class="centered">
                                    <h2>MUJER</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <!-- End: Explorar -->

    <!-- Start: Redes sociales -->
    <section id="index-seccion-7">
        
        <div class="container-flex margin-top">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card flex-md-row mb-4  h-md-250">
                        <img class="photo card-img" data-src="{{ asset('assets/global/img/index/index-fondo-show-room.jpg') }}" alt="Card image">
                        <div class="card-img-overlay">
                            <div class="centered">
                                <h2 class="txt-white texto-sobre-portada">SHOWROOM</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End: Redes sociales -->

@endsection