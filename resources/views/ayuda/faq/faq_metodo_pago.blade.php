@extends('template.main')

@section('title', 'Inicio')

@section('content')
<!-- Breadcrumbs -->
<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
        <ul class="u-list-inline">
            <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
            </li>
            <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-text" href="{{route('faq_menu')}}">Preguntas frecuentes</a>
                <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
            </li>
            <li class="list-inline-item txt-muletta-oro">
                <span>Métodos de pago</span>
            </li>
        </ul>
    </div>
</section>
<!-- End Breadcrumbs -->

<div class="container text-center g-py-100">
    <div class="mb-5">
        <span class="d-block g-color-gray-light-v1 g-font-size-70 g-font-size-90--md mb-4">
            <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i>
        </span>

        <div class="text-center g-mb-20">
            <img src="../../assets/global/img/icons/iconos-pw-11.png" class="rounded faq-icon-top" alt="...">
        </div>

        <h2 class="g-mb-30">MÉTODOS DE PAGO</h2>
        <div class="row">
            <div class="col-12 accordeon-ctm-wraper">
                <ul class="accordion-ctm">
                    @if($contenido)
                        @foreach($contenido AS $key => $cont)
                            <li class="accordion-ctm-item">
                                <a href="#!"><?= $cont->TITULO_FOOTER ?><i class=" ml-auto fas fa-chevron-down"></i></a>
                                <div class="content">
                                    <div class=" g-pa-40">
                                        <?= $cont->CONTENIDO_FOOTER ?>
                                    </div><!-- .u-shadow-v1-3 .g-pa-40 -->
                                </div><!-- .content -->
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>

        </div>
    </div>
</div>
@endsection
