@extends('template.main')

@section('title', 'Inicio')

@section('content')
    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item txt-muletta-oro">
                    <span>Catálogo 2018 </span>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container mb-5 g-pt-50">
        <div class="  rounded mx-auto g-pa-20 ">
            <div class="  g-bg-white  mb-4">
                <header class="text-center ">
                    <h4 class="g-font-weight-600 txt-muletta-gris ">Catálogo 2018 </h4>
                </header>
        
                <div class="wpb_wrapper g-pt-50">
                    <div class="issuuembed" style="width: 100%; height: 1325px;" data-configid="12011275/63080711">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
@endsection