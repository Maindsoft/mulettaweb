@extends('template.main')

@section('title', 'Inicio')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/register.css') }}">
@endsection

@section('content')
    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                
                <li class="list-inline-item txt-muletta-oro">
                <span>Políticas de Cambios y Devoluciones </span>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container mb-5 g-pt-50">
        <div class="rounded mx-auto g-pa-20 ">
            <div class="g-bg-white  mb-4">
                <header class="text-center ">
                    <h1 class="h4  g-font-weight-600 txt-muletta-gris ">Políticas de Cambios y Devoluciones</h1>
                </header>
            
                <div class="entry-content g-pt-50">
                
                    <p><b>Horarios de Atención</b></p>
                    <p>Lunes a Viernes &nbsp;&nbsp;&nbsp;9:00 am &nbsp;a 5:00 pm</p>
                    <p>Sábado &nbsp;9:30 am a 1:30 pm</p>
                    <p>&nbsp;</p>
                    <p>La Tienda Online Muletta funciona con una política de devolución bien marcada y establecida en las condiciones de contratación.</p>
                    <p>Esta política de devoluciones solamente es válido en productos &nbsp;comprados en nuestra Tienda Online, Facebook y WhatsApp.</p>
                    <p>Sólo se cubrirá &nbsp;el importe de los gastos de envío realizado en online en los siguientes casos:</p>
                    <ul>
                        <li><b>Error en el Envío</b></li>
                    </ul>
                    <ul>
                        <li><b>Error en el producto Enviado</b></li>
                    </ul>
                    <ul>
                        <li><b>Defectos de Fabricación</b></li>
                    </ul>
                    <p>En el resto de casos, el cliente se hará cargo de los gastos de envío.</p>
                    <p>Cualquier persona puede cambiar un artículo sin utilizar dentro de los 10 días hábiles de la compra poniéndose en contacto con nuestro servicio de atención al cliente y cumpliendo el siguiente proceso.</p>
                    <p>&nbsp;</p>
                    <p><b>Tarifa de Envíos</b></p>
                    <p>Dentro de la Ciudad de Aguascalientes: &nbsp;<b>$99.00 MN</b></p>
                    <p>Interior de la República: <b>$99.00 MN</b></p>
                    <p>Resto del Mundo:<b> se definirá en base a una cotización.</b></p>
                    <p>&nbsp;</p>
                    <p><b>PROCESO PARA CAMBIO DE PRODUCTOS</b></p>
                    <ul>
                        <li><b>Envíanos un mensaje a nuestros canales de Atención a Clientes</b></li>
                    </ul>
                    <ol>
                        <li>Vía correo electrónico: &nbsp;<a href="mailto:ventas@muletta.com">ventas@muletta.com</a></li>
                        <li>Vía WhatsApp / &nbsp;Teléfono <b>(449) 919 6307</b></li>
                    </ol>
                    <ul>
                        <li><b>Datos para realizar un Cambio</b></li>
                    </ul>
                    <ol>
                        <li style="list-style-type: none;">
                            <ol>
                                <li>Motivo para realizar cambio</li>
                                <li>Foto o Nombre del Producto que deseas cambiar</li>
                                <li>Talla</li>
                                <li>Foto o Nombre del Producto por el cual deseas realizar el cambio</li>
                                <li>Validación de Existencia</li>
                            </ol>
                        </li>
                    </ol>
                    <ul>
                        <li><b>Una vez realizada la solicitud de cambio, tendremos 48 hrs, para responder a tu solicitud.</b></li>
                    </ul>
                    <ul>
                        <li><b>Un vez atendida la solicitud, con la confirmación de la existencia del producto del cual deseas realizar el cambio deberás de seguir estos pasos:</b></li>
                    </ul>
                    <ol>
                        <li style="list-style-type: none;">
                            <ol>
                                <li>Revisa que las condiciones del producto cumplan con características</li>
                            </ol>
                        </li>
                    </ol>
                    <ol>
                        <li style="list-style-type: none;">
                            <ol>
                                <li style="list-style-type: none;">
                                    <ol>
                                        <li>Revisar que el producto este en buenas condiciones, como se le envío o adquirió en algún Punto de Venta.</li>
                                        <li>Revisa que el producto mantenga el etiquetado original.</li>
                                        <li>De ser posible enviar el producto con el embalaje original.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                    </ol>
                    <ol>
                        <li style="list-style-type: none;">
                            <ol>
                                <li>Envíanos el producto que deseas cambiar a esta dirección:</li>
                            </ol>
                        </li>
                    </ol>
                    <p><b>Destinario:</b> Muletta</p>
                    <p><b>Calle:</b> 22 de Octubre # 105</p>
                    <p><b>Colonia:</b> Modelo</p>
                    <p><b>CP:</b> 20080</p>
                    <p><b>Ciudad:</b> Aguascalientes</p>
                    <p><b>Estado:</b> Aguascalientes</p>
                    <p><b>País:</b> México</p>
                    <p><b>Teléfono: </b>449 919 63 07</p>
                    <ol>
                        <li style="list-style-type: none;">
                    <ol>
                        <li>Revisaremos que el producto cumpla con los condiciones mencionadas en el <b>paso 1.</b></li>
                        <li>Una vez aprobado el producto se procederá al envío del producto que nos estés solicitando.</li>
                        <li>Te enviaremos tu número de guía.</li>
                    </ol>
                    </li>
                    </ol>
                    <p><b>PROCESO PARA DEVOLUCIONES &nbsp;DE PRODUCTOS</b></p>
                    <ul>
                        <li><b>Envíanos un mensaje a nuestros canales de Atención a Clientes</b></li>
                    </ul>
                    <ol>
                        <li>Vía correo electrónico: &nbsp;<a href="mailto:ventas@muletta.com">ventas@muletta.com</a></li>
                        <li>Vía WhatsApp / &nbsp;Teléfono <b>(449) 919 6307</b></li>
                    </ol>
                    <ul>
                        <li><b>Datos para realizar la devolución</b></li>
                    </ul>
                    <ol>
                        <li>Aclaración del error ya sea de talla, modelo o datos de envío</li>
                        <li>Foto del Defecto (si es el caso)</li>
                        <li>Foto o Nombre del Producto por el cual deseas realizar el cambio (si es el caso)</li>
                        <li>Validación de Existencia</li>
                    </ol>
                    <ul>
                        <li><b>Una vez realizada la solicitud de devolución, tendremos 48 hrs. para responder a tu solicitud.</b></li>
                    </ul>
                    <ul>
                        <li><b>Una vez atendida la solicitud, con la confirmación de la existencia del producto del cual deseas realizar el cambio (si es el caso) deberás de seguir estos pasos:</b></li>
                    </ul>
                    <ol>
                        <li>Revisa que las condiciones del producto cumplan con características</li>
                    </ol>
                    <ol>
                        <li style="list-style-type: none;">
                    <ol>
                        <li style="list-style-type: none;">
                    <ol>
                        <li>Revisar que el producto este en buenas condiciones, como se le envío o adquirió en algún Punto de Venta.</li>
                        <li>Revisa que el producto mantenga el etiquetado original.</li>
                        <li>De ser posible enviar el producto con el embalaje original.</li>
                    </ol>
                    </li>
                    </ol>
                    </li>
                    </ol>
                    <ol>
                        <li>Envíanos el producto que deseas devolver a esta dirección:</li>
                    </ol>
                    <p><b>Destinario:</b> Muletta</p>
                    <p><b>Calle:</b> 22 de Octubre # 105</p>
                    <p><b>Colonia:</b> Modelo</p>
                    <p><b>CP:</b> 20080</p>
                    <p><b>Ciudad:</b> Aguascalientes</p>
                    <p><b>Estado:</b> Aguascalientes</p>
                    <p><b>País:</b> México</p>
                    <p><b>Teléfono: </b>449 919 63 07</p>
                    <ol>
                        <li>Revisaremos que el producto cumpla con los condiciones &nbsp;mencionadas en el <b>paso 1.</b></li>
                        <li>Una vez aprobado el producto se procederá el envío del producto que nos estés solicitando o se te pedirá un número de cuenta bancaria para poder depositar el monto pagado por el producto de la devolución.</li>
                    </ol>
                    <ol>
                        <li>Te enviaremos tu número de guía o te depositaremos en los siguientes 2 días hábiles el monto pagado por el producto de la devolución.</li>
                    </ol>
                </div>
            </div><!-- .g-bg-white  mb -->   
        </div><!-- .g-max-width-445 .g-brd-around .g-brd-gray-light-v4 .text-center .rounded mx-auto .g-pa-20 -->   
    </div>
@endsection