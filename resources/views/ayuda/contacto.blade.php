@extends('template.main')

@section('title', 'Inicio')

@section('content')
    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>

                <li class="list-inline-item txt-muletta-oro">
                    <span>Contacto</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container mb-5 g-pt-50">
        <div class="  g-max-width-445 text-center rounded mx-auto g-pa-20 ">
            <div class="  g-bg-white  mb-4">

                <header class="text-center ">
                    <h1 class="h4  g-font-weight-600 txt-muletta-gris">CONTACTO</h1>
                </header>
                
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
               @endif
                
                 @if ($message = Session::get('exito'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                 @endif

                <!-- Form -->
                <form class="g-py-15" method="POST" action="{{url('sendemail/send')}}">
                    {{ csrf_field() }}

                    <div class="row no-gutters">
                        <!-- Nombre -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="name" type="text" placeholder="NOMBRE*">
                            </div>
                        </div>

                        <!-- Email -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="email" type="email" placeholder="CORREO ELECTRÓNICO*">
                            </div>
                        </div>

                        <!-- Mensaje -->
                        <div class="col-12 px-5 mb-4 ">
                            <label for="exampleTextarea">MENSAJE</label>
                            <div class="input-group">
                                <textarea name="message" class="form-control rounded-0 form-control-md" id="exampleTextarea" rows="6"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-md btn-block u-btn-primary rounded-0  mb-3 g-recaptcha" 
                                data-sitekey="6Ld9iqUdAAAAANf2RrIPNpn-L8uGUPFxCUX9Af-x" 
                                data-callback='onSubmit' 
                                data-action='submit'>ENVIAR</button>
                            <p class="g-color-gray-dark-v5 mb-0 text-center">* CAMPOS REQUERIDO </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row text-center" style="margin-right: 10vw;margin-left: 10vw;">
        <div class="col-4">
            <div>
                <h4>Dirección</h4>
                <p class="text-right">
                    <a href="https://goo.gl/maps/rejRGHDyeSz8UrTh7" target="_blank">
                        Av. Francisco I. Madero #514
                        <br>
                        Barrio de la Purísima
                        <br>
                        C.P. 20259
                        <br>
                        Aguascalientes, Aguascalientes México
                    </a>
                </p>
            </div>
            <div>
                <h4>Telefono</h4>
                <p class="text-right">
                    (449) 919 6307
                </p>
            </div>
            <div>
                <h4>Correo</h4>
                <p class="text-right">
                    contacto@muletta.com
                </p>
            </div>
        </div>
        <div class="col-6">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3702.3189796856327!2d-102.28879098505266!3d21.883789285541475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8429ef06d1a10c15%3A0x93d6ed00f74329bc!2sMuletta!5e0!3m2!1ses-419!2smx!4v1610662840044!5m2!1ses-419!2smx" width="900" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>

@endsection