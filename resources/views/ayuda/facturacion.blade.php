@extends('template.main')

@section('title', 'Inicio')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/register.css') }}">
@endsection

@section('content')

    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item txt-muletta-oro">
                    <span>Facturación</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container mb-5 g-pt-50">
        <div class="  g-max-width-445 text-center rounded mx-auto g-pa-20 ">
            <div class="  g-bg-white  mb-4">
        
                <header class="text-center ">
                    <h1 class="h4  g-font-weight-600 txt-muletta-gris">FACTURACIÓN</h1>
                    <p>
                        Solicita tu factura llenando el siguiente formulario, nos pondremos en contacto contigo.
                    </p>
                </header>
                
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            
                @if ($message = Session::get('exito'))
                    <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
        
                <!-- Form -->
                <form class="g-py-15" method="POST" action="{{url('sendemail/sendFacturacion')}}">
                    {{ csrf_field() }}
                    
                    <div class="row no-gutters">
                        <!-- Nombre -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="name" type="text" placeholder="NOMBRE*">
                            </div>
                        </div>

                        <!-- Email -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="email" type="email" placeholder="CORREO ELECTRÓNICO*">
                            </div>
                        </div>

                        <!-- RFC -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="rfc" type="text" placeholder="RFC*">
                            </div>
                        </div>

                        <!-- Nombre empresa -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="empresa" type="text" placeholder="NOMBRE EMPRESA*">
                            </div>
                        </div>

                        <!-- Tipo de empresa -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="tipo" type="text" placeholder="TIPO DE EMPRESA*">
                            </div>
                        </div>

                        <!-- Calle -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="calle" type="text" placeholder="CALLE*">
                            </div>
                        </div>

                        <!-- Numero -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="numero" type="text" placeholder="NUMERO*">  
                            </div>
                        </div>

                        <!-- Colonia -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="colonia" type="text" placeholder="COLONIA*">
                            </div>
                        </div>
                        
                        <!-- Codigo Postal -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="cp" type="text" placeholder="CÓDIGO POSTAL*">
                            </div>
                        </div>

                        <!-- Estado -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="estado" type="text" placeholder="ESTADO*">
                            </div>
                        </div>

                        <!-- Ciudad -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="ciudad" type="text" placeholder="CIUDAD*">
                            </div>
                        </div>

                        <!-- Apellido -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="nodepedido" type="text" placeholder="ID DE PEDIDO*">
                            </div>
                        </div>

                        <!-- Total -->
                        <div class="col-12 px-5 mb-4 ">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="totaldecompra" type="text" placeholder="TOTAL DE LA COMPRA*">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-md btn-block u-btn-primary rounded-0  mb-3" type="submit" name="send" >
                                ENVIAR
                            </button>
                            <p class="g-color-gray-dark-v5 mb-0 text-center">* CAMPOS REQUERIDO </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection