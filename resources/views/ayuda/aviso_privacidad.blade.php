@extends('template.main')

@section('title', 'Inicio')

@section('content')


<!-- Breadcrumbs -->
<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
        <ul class="u-list-inline">
            <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
            </li>
            <li class="list-inline-item txt-muletta-oro">
                <span>Aviso de Privacidad</span>
            </li>
        </ul>
    </div>
</section>
<!-- End Breadcrumbs -->


<div class="container mb-5 g-pt-50">
    <div class="  rounded mx-auto g-pa-20 ">
        <div class="  g-bg-white  mb-4">
            <header class="text-center ">
                <h1 class="h4  g-font-weight-600 txt-muletta-gris ">Aviso de Privacidad</h1>
            </header>
            <div class="wpb_wrapper g-pt-50">
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                        <p>En cumplimiento a la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, Creatmos S.A.P.I. de C.V. (Muletta) le informa que es responsable de sus datos personales. El “Usuario” podrá contactar a Creatmos S.A.P.I. de C.V. (Muletta) en cualquier momento a través de nuestro correo electrónico ventas@muletta.com o directamente en 22 Octubre #105, Colonia Modelo, Aguascalientes, Ags. C.P. 20080. Nuestros teléfonos son y  449 919 6307  . Protegemos y salvaguardamos sus datos personales para evitar el daño, pérdida, destrucción, robo, extravío, alteración, así como el tratamiento no autorizado de sus datos personales.</p>
                        
                        <p>DATOS PERSONALES</p>
                        <p>La información deberá ser siempre veraz y completa. El usuario responderá en todo momento por los datos proporcionados y en ningún caso Creatmos S.A.P.I. de C.V. (Muletta) será responsable de la veracidad de los mismos.</p>
                        <p>La información solicitada al usuario en el sitio www.muletta.com es: Nombre completo, Teléfonos de contacto, Dirección Postal, Correo Electrónico, Datos de Facturación, Datos de Tarjeta de Crédito o Débito. Sus datos personales serán tratados con base a en los principios de licitud, consentimiento, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad en términos de la Legislación. Se mantendrá la confidencialidad de sus datos personales estableciendo y manteniendo de forma efectiva las medidas de seguridad administrativas, técnicas y físicas, para evitar su daño, pérdida, alteración, destrucción, uso, acceso o divulgación indebida.</p>

                        <p>TRANSFERENCIA DE DATOS</p>
                        <p>El titular de los datos personales, autoriza a Creatmos S.A.P.I. de C.V. (Muletta), a transferir los datos proporcionados por él a terceros (ya sea persona física o moral), que con motivo de la relación comercial con Creatmos S.A.P.I. de C.V. (Muletta), lo requieran para entero cumplimiento del negocio; asimismo podrá transferirlos a instituciones financieras de banca múltiple para contacto y comercialización de sus productos o servicios financieros. El titular de los datos personales, entiende y acepta que en ningún momento se podrán transferir datos sensibles ni patrimoniales. En el caso de no estar de acuerdo con la transmisión aquí mencionada, el titular deberá ponerse en contacto al correo electrónico: contacto@muletta.com, o en nuestros teléfonos: 449 919 6307, en un plazo no mayor a 5 (cinco) días hábiles a partir de la publicación del presente aviso ya que de lo contrario y transcurrido dicho término, acepta y autoriza a la Marca Muletta a la transferencia de los mismos.</p>
                        
                        <p>QUÉ SON LOS COOKIES Y CÓMO SE UTILIZAN</p>
                        <p>La aceptación de las cookies no es un requisito para visitar nuestra página. Sin embargo, nos gustaría señalar que el uso del “carrito”, funcionalidad en Muletta y orden solo es posible con la activación de las cookies. Las cookies son pequeños archivos de texto que identifican a tu computadora con nuestro servidor como un usuario único cuando tú visitas ciertas páginas en nuestro sitio y que son guardados por tu navegador de internet en el disco duro de tu computadora. Las cookies se pueden utilizar para reconocer tu dirección de protocolo de internet, que te ahorra tiempo mientras quieres entrar a nuestro sitio. Solo utilizamos cookies para tu comodidad en el uso de Muletta (por ejemplo, para recordar quién eres cuando deseas modificar tu carrito de compra sin tener que volver a introducir tu dirección de correo electrónico) y no para obtener o usar cualquier otra información sobre ti (por ejemplo de publicidad segmentada). Tu navegador puede ser configurado para no aceptar cookies, pero esto sería restringir el uso de nuestra página. Por favor, acepta nuestra garantía de que el uso de cookies no contiene datos de carácter personal o privado, y están libres de virus. Si deseas obtener más información acerca de las cookies, ve a http://www.allaboutcookies.org, y para obtener información sobre la eliminación de ellos desde el navegador, ve a http://www.allaboutcookies.org/manage-cookies/index.html. En el caso de empleo de cookies, el botón de “ayuda” que se encuentra en la barra de herramientas de la mayoría de los navegadores, le dirá cómo evitar aceptar nuevos cookies, cómo hacer que el navegador le notifique cuando recibe un nuevo cookie o cómo deshabilitar todos los cookies.</p>
                        
                        <p>USO DE LA INFORMACIÓN</p>
                        <p>La información solicitada permite a José Adrián Martínez López (Muletta) a contactar a los usuarios y potenciales clientes cuando sea necesario para completar los procedimientos de compra. Asimismo, Creatmos S.A.P.I. de C.V. (Muletta)  utilizará la información obtenida para:</p>
                        <ul>
                            <li>Procurar un servicio eficiente</li>
                            <li>Informar sobre nuevos productos o servicios que estén relacionados con el contratado o adquirido por el cliente</li>
                            <li>Dar cumplimiento a obligaciones contraídas con nuestros clientes </li>
                            <li>Informar sobre cambios de nuestros productos o servicios</li>
                            <li>Proveer una mejor atención al usuario.</li>
                        </ul>
                        <p>Los datos personales o empresariales proporcionados por el usuario formarán parte de un archivo que contendrá su perfil. El usuario puede modificar su perfil en cualquier momento utilizando su número de usuario y contraseña.</p>


                        <p>LIMITACIÓN DE USO Y DIVULGACIÓN DE INFORMACIÓN</p>
                        <p>En nuestro programa de notificación de promociones, ofertas y servicios a través de correo electrónico, solo Creatmos S.A.P.I. de C.V. (Muletta) tiene acceso a la información recabada. Este tipo de publicidad se realiza mediante avisos y mensajes promocionales de correo electrónico, los cuales solo serán enviados a usted y a aquellos contactos registrados para tal propósito, esta indicación podrá usted modificarla en cualquier momento enviando un correo a ventas@muletta.com.</p>
                        
                        <p>DERECHOS ARCO (ACCESO, RECTIFICACIÓN, CANCELACIÓN Y OPOSICIÓN)</p>
                        <p>El área responsable del manejo y la administración de los datos personales es: Servicio al Cliente a quien puede contactar mediante en el correo electrónico contacto@muletta.com o directamente en calle 22 de Octubre #105, Colonia Modelo, Aguascalientes, Ags. C.P. 20080. Nuestros teléfono es: 449 919 6307</p>
                        
                        <p>PARA EJERCER SUS DERECHOS ARCO Y REVOCAR EL CONSENTIMIENTO OTORGADO</p>
                        <p>Como titular de datos personales, el “Usuario” podrá ejercitar los derechos ARCO (acceso, cancelación, rectificación y oposición al tratamiento de sus datos personales), o bien, revocar el consentimiento que haya otorgado a Creatmos S.A.P.I. de C.V. (Muletta), para el tratamiento de sus datos personales, enviando directamente su solicitud al área Servicio de Atención al Clientes a través de la cuenta de correo electrónico: ventas@muletta.com. Dicha solicitud deberá contener por lo menos: (a) nombre y domicilio u otro medio para comunicarle la respuesta a su solicitud; (b) los documentos que acrediten su identidad o, en su caso, la representación legal; (c) la descripción clara y precisa de los datos personales respecto de los que se solicita ejercer alguno de los derechos ARCO, (d) la manifestación expresa para revocar su consentimiento al tratamiento de sus datos personales y por tanto, para que no se usen; (d) cualquier otro elemento que facilite la localización de los datos personales. Su petición deberá ir acompañada de los fundamentos por los que solicita dicha revocación y una identificación oficial del titular de los datos o de su apoderado. En un plazo máximo de 20 (veinte) días hábiles atenderemos su solicitud y le informaremos sobre la procedencia de la misma a través del correo electrónico del que provenga la petición. José Adrián Martínez López (Muletta) solicita al usuario que actualice sus datos cada vez que éstos sufran alguna modificación, ya que esto permitirá brindarle un servicio eficiente y personalizado.</p>
                        
                        <p>TRANSFERENCIAS DE INFORMACIÓN POR SOLICITUD JUDICIAL</p>
                        <p>Creatmos S.A.P.I. de C.V. (Muletta), únicamente realiza remisiones de datos para cumplir con las obligaciones contraídas con los clientes. Creatmos S.A.P.I. de C.V. (Muletta), solo compartirá datos cuando haya sido requerido por orden judicial para cumplir con las disposiciones procesales.</p>
                        
                        <p>PROTECCIÓN</p>
                        <p>Al momento de comprar un producto en línea, se pedirán datos bancarios para los cuales le ofrecemos seguridad y confidencialidad, ya que contamos con un servidor seguro bajo el protocolo SSL (Secure Socket Layer) de tal menara que la información que envían, se transmite encriptada para asegurar su protección. Para verificar que se encuentra en un entorno protegido, asegúrese de que aparezca una “S” en la barra de navegación “httpS”://. Sin embargo, y a pesar de contar cada día con herramientas más seguras, la protección de los datos enviados a través de Internet no se puede garantizar al 100%; por lo que una vez recibidos, se hará todo lo posible por salvaguardar la información.</p>
                        
                        <p>CAMBIOS EN EL AVISO DE PRIVACIDAD</p>
                        <p>Nos reservamos el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente aviso de privacidad, para la atención de novedades legislativas o jurisprudenciales, políticas internas, nuevos requerimientos para la prestación u ofrecimiento de nuestros servicios o productos y prácticas del mercado. Estas modificaciones estarán disponibles al público a través de nuestra página de Internet www.muletta.com, sección aviso de privacidad.</p>
                        
                        <p>ACEPTACIÓN DE LOS TÉRMINOS</p>
                        <p>Esta declaración de Privacidad está sujeta a los términos y condiciones del sitio web de Creatmos S.A.P.I. de C.V. (Muletta) antes descrito, lo cual constituye un acuerdo legal entre el usuario y Creatmos S.A.P.I. de C.V. (Muletta). Si el usuario utiliza los servicios del sitio de Creatmos S.A.P.I. de C.V. (Muletta), significa que ha leído, entendido y acordado los términos antes expuestos.</p>
                        
                        <p>AUTORIDAD</p>
                        <p>Si el usuario/cliente considera que han sido vulnerados sus derechos respecto de la protección de datos personales, tiene el derecho de acudir a la autoridad correspondiente para defender su ejercicio. La autoridad es el Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos (INAI), su sitio web es: &nbsp;<a href="http://www.ifai.mx/">www.ifai.mx</a>.</p>
                        
                        <p>Ultima actualización al presente Aviso de Privacidad:</p>
                        <p>11/Junio/2020 Aguascalientes, Aguascalientes. México.</p>

                        <p class="margin-top">Limitación de responsabilidad</p>
                        <p>EN NINGÚN CASO WWW.MULETTA.COM O SUS ENTIDADES AFILIADAS Y PROVEEDORES SERÁN RESPONSABLES DE NINGÚN DAÑO INDIRECTO, ESPECIAL, SANCIONADOR, INCIDENTAL, EJEMPLAR O INDIRECTO, INCLUSO SI WWW.MULETTA.COM HA SIDO PREVENIDO CON ANTERIORIDAD DE LA POSIBILIDAD DE DICHOS DAÑOS, CON INDEPENDENCIA DE QUE SE DEBA A UNA ACCIÓN CONTRACTUAL, A UNA NEGLIGENCIA O A CUALQUIER OTRA TEORÍA, QUE PUEDA OCURRIR DEBIDO A O EN RELACIÓN CON EL USO, LA IMPOSIBILIDAD DE USO O EJECUCIÓN DE LA INFORMACIÓN, SERVICIOS, PRODUCTOS Y MATERIALES DISPONIBLES EN EL SITIO. Estas limitaciones se aplicarán independientemente de cualquier fallo del propósito esencial o de la existencia de un recurso limitado. Debido a que algunas jurisdicciones no permiten limitaciones en la duración de las garantías implícitas, ni la exclusión o limitación de responsabilidad de daños indirectos o incidentales, es posible que las anteriores limitaciones no sean aplicables.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
