@extends('template.main')

@section('title', $descripcion)

<?php
    $host= $_SERVER["HTTP_HOST"];
    $url= $_SERVER["REQUEST_URI"];

    $usuario = session()->get('perfilUsuario') ? session()->get('perfilUsuario')['usuario'] : [];
?>

@section('meta_type', "Producto")
@section('meta_title', $descripcion)
@section('meta_url', "https://".$host.$url)
@section('meta_image', $productos[0]->IMAGEN_100 != null ? str_replace('~', '', str_replace(' ', '%20', $productos[0]->IMAGEN_100) ) : null)

@section('css')
   
    <!--se ocupa para el carrousel de productos en la vista movil-->
    <link rel="stylesheet" href="{{ asset('assets/global/slick-carousel/slick/slick.min.css') }}">
   
    <!-- Unify -->
    <link rel="stylesheet" href="{{ asset('assets/global/css/unify/unify.min.css') }}">
    
    <!-- lightgallery -->
    <link rel="stylesheet" href="{{ asset('assets/global/lightgallery/dist/css/lightgallery.css') }}">
    
    <!-- desc-producto -->
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    
@endsection

@section('content')
    
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container-flex">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{ route('index') }}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item ">
                    <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $categoria_bradcrumbs['id_tipo_producto'], 'URL_TIPO_PRODUCTO' => $categoria_bradcrumbs['url_tipo_producto']]) }}">Catálogo de productos</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item txt-muletta-oro text-lowercase">
                    <span>{{$productos[0]->DESCRIPCION_MODELO}}</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <div class="loader" style="position: fixed;"></div><!-- .loader -->

    <div class="row" style="margin-left: 1.2%; margin-right: 1.2%;">
        <!-- Start: imagenes -->
        <div class="col-xs-12 col-md-6 col-lg-8 g-py-30">
            <!-- version movil -->
            <div class="js-carousel text-center g-pb-30 g-hidden-sm-up" data-infinite="true" data-pagi-classes="u-carousel-indicators-v1 g-absolute-centered--x g-bottom-0">
                @if ($productos[0]->IMAGEN_100 != null)
                    <div class="js-slide" id="lightgalleryCarouselSlide1">
                        <a class="col-lg-6 g-mb-30" href="{{!empty($productos[0]->IMAGEN_100) ? str_replace('~', 'https://creatmos.net', $productos[0]->IMAGEN_100)  : 'https://via.placeholder.com/605x745'}}" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @if(substr($productos[0]->IMAGEN_100, 0, 4) != 'http')
                                <img id="imgProd" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_100) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_100)  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @else
                                <img id="imgProd" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_100) ? $productos[0]->IMAGEN_100  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @endif
                        </a>

                        <!-- Container (notice the relative width) :  -->
                    </div>
                @endif
                @if ($productos[0]->IMAGEN_101 != null)
                    <div class="js-slide" id="lightgalleryCarouselSlide2">
                        <a class="col-lg-6 g-mb-30" href="{{!empty($productos[0]->IMAGEN_101) ? str_replace('~', 'https://creatmos.net', $productos[0]->IMAGEN_101)  : 'https://via.placeholder.com/605x745'}}" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @if(substr($productos[0]->IMAGEN_101, 0, 4) != 'http')
                                <img id="imgProd2" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_101) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_101)  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @else
                                <img id="imgProd2" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_101) ? $productos[0]->IMAGEN_101  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @endif 
                        </a>
                        <!-- Container (notice the relative width) :  -->
                    </div>
                @endif
                @if ($productos[0]->IMAGEN_102 != null)
                    <div class="js-slide" id="lightgalleryCarouselSlide3">
                        <a class="col-lg-6 g-mb-30" href="{{!empty($productos[0]->IMAGEN_102) ? str_replace('~', 'https://creatmos.net', $productos[0]->IMAGEN_102)  : 'https://via.placeholder.com/605x745'}}" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @if(substr($productos[0]->IMAGEN_102, 0, 4) != 'http')
                                <img id="imgProd3" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_102) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_102)  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @else
                                <img id="imgProd3" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_102) ? $productos[0]->IMAGEN_102  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @endif 
                        </a>
                        <!-- Container (notice the relative width) :  -->
                    </div>
                @endif
                @if ($productos[0]->IMAGEN_103 != null)
                    <div class="js-slide" id="lightgalleryCarouselSlide4">
                        <a class="col-lg-6 g-mb-30" href="{{!empty($productos[0]->IMAGEN_103) ? str_replace('~', 'https://creatmos.net', $productos[0]->IMAGEN_103)  : 'https://via.placeholder.com/605x745'}}" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @if(substr($productos[0]->IMAGEN_103, 0, 4) != 'http')
                                <img id="imgProd4" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_103) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_103)  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @else
                                <img id="imgProd4" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_103) ? $productos[0]->IMAGEN_103 : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                            @endif
                        </a>
                        <!-- Container (notice the relative width) :  -->
                    </div>
                @endif
            </div>
        
            <!-- version desktop -->
            <div class="row g-hidden-xs-down g-hidden-sm-down " id="lightgallery">
                @if ($productos[0]->IMAGEN_100 != null)
                    <a class="col-6 px-5 py-5 " href="{{!empty($productos[0]->IMAGEN_100) ? str_replace('~', 'https://creatmos.net', $productos[0]->IMAGEN_100)  : '/assets/global/img/producto/producto-no-hay-producto.jpg'}}" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                        @if(substr($productos[0]->IMAGEN_100, 0, 4) != 'http')
                            <img id="imgProd_1" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_100) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_100)  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}" style="width: 100%;">
                        @else
                            <img id="imgProd_1" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_100) ? $productos[0]->IMAGEN_100  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                        @endif    
                    </a>
                @else
                    <a class="col-6 px-5 py-5 " href="#" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                        <img id="imgProd_1" class="photo img-fluid" data-src="https://via.placeholder.com/605x745" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                    </a>
                    <!-- Container (notice the relative width) :  -->
                @endif
                @if ($productos[0]->IMAGEN_101 != null)
                    <a class="col-6 px-5 py-5" href="{{!empty($productos[0]->IMAGEN_101) ? str_replace('~', 'https://creatmos.net', $productos[0]->IMAGEN_101)  : '/assets/global/img/producto/producto-no-hay-producto.jpg'}}" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                        @if(substr($productos[0]->IMAGEN_101, 0, 4) != 'http')
                            <img id="imgProd_2" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_101) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_101)  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}" style="width: 100%;">
                        @else
                            <img id="imgProd_2" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_101) ? $productos[0]->IMAGEN_101  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                        @endif 
                    </a>
                @else
                    <a class="col-6 px-5 py-5 px-5 py-5 " href="#" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                        <img id="imgProd_2" class="photo img-fluid" data-src="https://via.placeholder.com/605x745" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                    </a>
                    <!-- Container (notice the relative width) :  -->
                @endif
                @if ($productos[0]->IMAGEN_102 != null)
                    <a class="col-6 px-5 py-5 " href="{{!empty($productos[0]->IMAGEN_102) ? str_replace('~', 'https://creatmos.net', $productos[0]->IMAGEN_102)  : '/assets/global/img/producto/producto-no-hay-producto.jpg'}}" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                        @if(substr($productos[0]->IMAGEN_102, 0, 4) != 'http')
                            <img id="imgProd_3" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_102) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_102)  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}" style="width: 100%;">
                        @else
                            <img id="imgProd_3" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_102) ? $productos[0]->IMAGEN_102  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                        @endif  
                    </a>
                @else
                    <a class="col-6 px-5 py-5 " href="#" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                        <img id="imgProd_3" class="photo img-fluid" data-src="https://via.placeholder.com/605x745" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                    </a>
                    <!-- Container (notice the relative width) :  -->
                @endif
                @if ($productos[0]->IMAGEN_103 != null)
                    <a class="col-6 px-5 py-5 " href="{{!empty($productos[0]->IMAGEN_103) ? str_replace('~', 'https://creatmos.net', $productos[0]->IMAGEN_103)  : '/assets/global/img/producto/producto-no-hay-producto.jpg'}}" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                        @if(substr($productos[0]->IMAGEN_103, 0, 4) != 'http')
                            <img id="imgProd_4" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_103) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_103)  : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}" style="width: 100%;">
                        @else
                            <img id="imgProd_4" class="photo img-fluid" data-src="{{!empty($productos[0]->IMAGEN_103) ? $productos[0]->IMAGEN_103 : 'https://via.placeholder.com/605x745'}}" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                        @endif  
                    </a>
                @else
                    <a class="col-6 px-5 py-5 " href="#" title="{{$productos[0]->DESCRIPCION_MODELO}}">
                        <img id="imgProd_4" class="photo img-fluid" data-src="https://via.placeholder.com/605x745" alt="{{$productos[0]->DESCRIPCION_MODELO}}">
                    </a>
                    <!-- Container (notice the relative width) :  -->
                @endif
            </div>
        </div>
        <!-- End: imagenes -->

        <!-- Start: right sidebar -->
        <div class="col-xs-12 col-md-6 col-lg-4 g-py-30 g-px-40--lg">
            <meta name="csrf-token" content="{{ csrf_token() }}">

            <div class="g-mb-0 text-center">
                <h1 id="model" class="g-font-weight-300 g-mb-0 g-pb-0 txt-muletta-oro g-font-prompt g-font-size-35 text-uppercase">
                    {{$productos[0]->DESCRIPCION_MODELO}}
                </h1>
                <b>
                    <hr class="g-bg-primary g-pt-0 g-my-10 g-brd-2" style="width: 5% !important;">
                </b>
                <b>
                    <p id="tipoProd" hidden="">{{$productos[0]->DESCRIPCION_TIPO_PRODUCTO}}</p>
                </b>
            </div>

            @if($productos[0]->PRECIO_VENTA_DESCUENTO != null && $productos[0]->PRECIO_VENTA != $productos[0]->PRECIO_VENTA_DESCUENTO)
                <div class="g-mb-30 g-pt-0 text-center">
                    <span class="txt-muletta-gris-claro g-pt-0 g-font-weight-500 g-font-prompt g-font-size-30 mr-2" style="text-decoration:line-through;">
                        $<span id="price">
                            @include('componentes.precio')
                        </span>
                        <span class="g-color-primary g-pt-0 g-font-weight-500 g-font-prompt g-font-size-30 mr-2">
                            $<span id="price_desc">
                                <?=number_format($productos[0]->PRECIO_VENTA_DESCUENTO, 2)?>
                            </span>
                        </span>
                    </span>
                </div>
            @else
                <div class="g-mb-30 g-pt-0 text-center">
                    <span class="g-color-primary g-pt-0 g-font-weight-500 g-font-prompt g-font-size-30 mr-2">
                        $ @include('componentes.precio')
                        <span id="price_desc"></span>
                    </span>
                </div>
            @endif

            @if (count($variantes_productos) > 0 && $variantes_productos[0]->DESCRIPCION_TALLA != null)
                <div class="g-pt-0 text-center">
                    <span class="txt-muletta-gris-claro g-pb-10 g-font-weight-500 g-font-size-20 mr-2 text-uppercase g-font-prompt">
                        {{isset($variantes_productos[0]->DESCRIPCION_COMERCIAL) ? $variantes_productos[0]->DESCRIPCION_COMERCIAL : 'TALLAS'}}
                    </span>

                    <ul class="list-inline mb-0 g-pt-10">
                        <?php
                            $check = 0
                        ?>
                        @foreach($variantes_productos as $key => $variante)
                            @if ($variante->EXISTENCIA == null || $variante->EXISTENCIA <= 0)
                                @if ($check == 0)
                                    <input type="radio" id="{{ trim($variante->DESCRIPCION_TALLA) }}" name="variantes" value="{{$variante->NO_PARTE}}" checked class="u-checkbox-v1--checked-color-primary u-checkbox-v1--checked-brd-primary g-font-prompt" onchange="refreshProductPrice({{$variante->PRECIO_VENTA}}, {{ $variante->EXISTENCIA }});refreshProductName('{{$variante->DESCRIPCION_TALLA}}')">
                                    <label for="{{ trim($variante->DESCRIPCION_TALLA) }}" style="border-radius: 50%;" class="d-inline-block text-center g-width-35 g-height-35 g-font-size-10 g-px-5 g-brd-around g-brd-2 g-brd-muletta-gray-light txt-muletta-gris-claro  g-brd-primary--hover g-color-primary--hover g-transition-0_3 g-py-4 g-mr-3 sin-existencia">{{$variante->DESCRIPCION_TALLA}}</label>
                                    <?php
                                        $check = 1;
                                    ?>
                                @elseif($check > 0)
                                    <input type="radio" id="{{ trim($variante->DESCRIPCION_TALLA) }}" name="variantes" value="{{$variante->NO_PARTE}}" class="u-checkbox-v1--checked-color-primary u-checkbox-v1--checked-brd-primary g-font-prompt" onchange="refreshProductPrice({{$variante->PRECIO_VENTA}}, {{ $variante->EXISTENCIA }});refreshProductName('{{$variante->DESCRIPCION_TALLA}}')">
                                    <label for="{{ trim($variante->DESCRIPCION_TALLA) }}" style="border-radius: 50%;" class="d-inline-block text-center g-width-35 g-height-35 g-font-size-10 g-px-5 g-brd-around g-brd-2 g-brd-muletta-gray-light txt-muletta-gris-claro  g-brd-primary--hover g-color-primary--hover g-transition-0_3 g-py-4 g-mr-3 sin-existencia">{{$variante->DESCRIPCION_TALLA}}</label>
                                @endif
                            @else
                                @if ($check == 0)
                                    <input type="radio" id="{{ trim($variante->DESCRIPCION_TALLA) }}" name="variantes" value="{{$variante->NO_PARTE}}" checked class="u-checkbox-v1--checked-color-primary u-checkbox-v1--checked-brd-primary g-font-prompt" onchange="refreshProductPrice({{$variante->PRECIO_VENTA}}, {{ $variante->EXISTENCIA }});refreshProductName('{{$variante->DESCRIPCION_TALLA}}')">
                                    <label for="{{ trim($variante->DESCRIPCION_TALLA) }}" style="border-radius: 50%;" class="d-inline-block text-center g-width-35 g-height-35 g-font-size-10 g-px-5 g-brd-around g-brd-2 g-brd-muletta-gray-light txt-muletta-gris-claro  g-brd-primary--hover g-color-primary--hover g-transition-0_3 g-py-4 g-mr-3">{{$variante->DESCRIPCION_TALLA}}</label>
                                    <?php
                                        $check = 1;
                                    ?>
                                @elseif($check > 0)
                                    <input type="radio" id="{{ trim($variante->DESCRIPCION_TALLA) }}" name="variantes" value="{{$variante->NO_PARTE}}" class="u-checkbox-v1--checked-color-primary u-checkbox-v1--checked-brd-primary g-font-prompt" onchange="refreshProductPrice({{$variante->PRECIO_VENTA}}, {{ $variante->EXISTENCIA }});refreshProductName('{{$variante->DESCRIPCION_TALLA}}')">
                                    <label for="{{ trim($variante->DESCRIPCION_TALLA) }}" style="border-radius: 50%;" class="d-inline-block text-center g-width-35 g-height-35  g-font-size-10 g-px-5 g-brd-around g-brd-2 g-brd-muletta-gray-light txt-muletta-gris-claro  g-brd-primary--hover g-color-primary--hover g-transition-0_3 g-py-6 g-mr-3">{{$variante->DESCRIPCION_TALLA}} </label>
                                @endif
                            @endif
                            <input type="hidden" id="check_{{ trim($variante->DESCRIPCION_TALLA) }}" name="variantes_exist" value="{{ $variante->EXISTENCIA }}">
                        @endforeach
                    </ul>
                </div>
                
                <div class="row margin-top">
                    <div class="col-12">
                        <h4 class="text-disponible">
                            <?php
                                $disponibilidad = 0;
                            ?>
                            @foreach($variantes_productos as $key => $variante)
                                @if ($disponibilidad == 0)
                                    <?php
                                        $disponibilidad = 1;
                                    ?>
                                    Disponible:
                                    <span id="existencia_sku">{{ $variante->EXISTENCIA }}</span>
                                    <?php break; ?>
                                @endif
                            @endforeach
                        </h4>
                    </div>
                </div>

                <div id="sinExt" class="u-accordion__body g-font-size-13 g-color-gray-dark-v5 text-left row">

                    <div class="col-1">
                    </div>
                    <div class="col-10 g-px-5 g-mb-10 g-mb-15">
                            <h4 style="color:red;">
                                No hay existencia, en algunas tallas de este producto.
                            </h4>
                            <input type="hidden" value="<?=$usuario ? $usuario->NOMBRE : ''?>" id="txtNombre">
                            <input type="hidden" value="<?=$usuario ? $usuario->EMAIL : ''?>" id="txtCorreo">
                            <input type="hidden" value="{{ route('notify') }}" id="urlNotify">

                            <button class="btn-block u-btn-primary g-font-size-12 text-uppercase g-py-6 g-px-25 g-bg-black--hover g-brd-transparent g-brd-black--hover g-rounded-0" type="button" onclick="productoNegado()">
                                Solicitar notificación cuando este disponible
                            </button>
                        </div>
                        <div class="col-1">
                        </div>

                    </div>

                <div class="row g-mb-30 margin-top">
                    <div class="col-12 g-brd-muletta-gray-light g-brd-right">
                        <a href="{{route('guia_tallas')}}" class="liga-muletta" >
                            <h3 class="g-font-size-16 g-py-15 g-px-15 text-center">Guía de tallas </h3>
                        </a>
                    </div>
                    <!-- <div class="col-6 g-brd-muletta-gray-light g-brd-right text-center">
                        <a href="{{route('guia_tallas')}}" class="liga-muletta">
                            <h3 class="g-font-size-16 g-py-15 g-px-15">Descubre tu talla</h3>
                        </a>
                    </div> -->
                </div>
            @endif

            <?php
                $aux = 0;
                $auxTotal = 0;
                foreach ($variantes_productos as $key => $variante) {
                    $auxTotal = $auxTotal + $variante->EXISTENCIA;
                }
            ?>
            <input type="hidden" id="existenciatotal" value="<?=$auxTotal?>">

            @foreach($variantes_productos as $key => $variante)
                @if ( $aux == 0 )
                    <input type="hidden" id="existenciaParte" value="<?=$variante->EXISTENCIA?>">
                    <?php
                        $aux = 1;
                    ?>
                @endif
            @endforeach

           

            <div id="conExt" class="row g-mx-minus-5 g-mb-50--md g-mb-30">
                <div class="col-lg-4 g-mb-15 g-ml-2--lg g-px-0--lg text-center">
                    <input type="number" class="form-control rounded-0" id="cantidad" value="1">
                </div>

                <div class="col-lg-7 g-px-5 g-mb-10 g-mb-15">
                    <button onclick="addCar();" class="btn-block u-btn-primary g-font-size-12 text-uppercase g-py-6 g-px-25 g-bg-black--hover g-brd-transparent g-brd-black--hover g-rounded-0" type="button">
                        Añadir al carrito 
                    </button>
                </div>

            </div>
            
            <!-- end: Cantidad -->
            
            <!-- Detalles -->
            <div id="accordion-07" class="u-accordion " role="tablist" aria-multiselectable="true">
                <!-- Card -->
                <div class="card rounded-0 g-brd-none">
                    <div id="accordion-07-heading-01" class="u-accordion__header g-pa-10" role="tab">
                        <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700  mb-0">
                            <a class="d-flex g-color-gray-dark-v3 g-text-underline--none--hover" href="#accordion-07-body-01" data-toggle="collapse" data-parent="#accordion-07" aria-expanded="true" aria-controls="accordion-07-body-01">
                                <span class="u-accordion__control-icon g-color-gray-dark-v3 g-brd-right text-center">
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </span>
                                <span class="">
                                    DETALLES DEL PRODUCTO
                                </span>
                            </a>
                        </h5>
                    </div>
                    <div id="accordion-07-body-01" class="g-color-gray-dark-v3 collapse show" role="tabpanel" aria-labelledby="accordion-07-heading-01" data-parent="#accordion-07" style="">
                        <div class="u-accordion__body  g-font-size-13 g-color-gray-dark-v5 text-left">
                            SKU <span id="sku_prod"> {{ $id }} </span>
                            <br>
                            
                            @if ($productos[0]->DESCRIPCION_COMERCIAL != null)
                                <span id="descripcion">{{ $productos[0]->DESCRIPCION_COMERCIAL }}</span>
                            @else
                                <p style="color: red;" id="descripcion">
                                    Falta llenar la descripción comercial del producto en la base de datos. Si eres staff de Muletta, haga favor de llenarla mediante su modulo en el ERP o informar a la persona que pueda llenarlo. 
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- End Card -->
            </div>
            
            <?php
                // echo '<script type="application/ld+json">
                //     {
                //       "@context": "https://schema.org/", 
                //       "@type": "Product", 
                //       "name": "'.$productos[0]->DESCRIPCION_MODELO.'",
                //       "image": "https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_100).'",
                //       "description": "'.$productos[0]->DESCRIPCION_COMERCIAL.'",
                //       "brand": {
                //         "@type": "Brand",
                //         "name": "Muletta"
                //       },
                //       "sku": "'.$id.'",
                //       "offers": {
                //         "@type": "Offer",
                //         "url": "'.$_SERVER['REQUEST_SCHEME'].'",
                //         "priceCurrency": "MXN",
                //         "price": "799",
                //         "availability": "https://schema.org/InStock",
                //         "itemCondition": "https://schema.org/NewCondition"
                //       }
                //     }
                // </script>';
            ?>
            <!-- Fin Detalles -->

            <!-- Cuidados -->
            <div id="accordion-072" class="u-accordion " role="tablist" aria-multiselectable="true">
                <!-- Card -->
                <div class="card rounded-0 g-brd-none">
                    <div id="accordion-07-heading-03" class="u-accordion__header g-pa-10" role="tab">
                        <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700  mb-0">
                            <a class="d-flex g-color-gray-dark-v3 g-text-underline--none--hover" href="#accordion-07-body-03" data-toggle="collapse" data-parent="#accordion-072" aria-expanded="true" aria-controls="accordion-07-body-03">
                                <span class="u-accordion__control-icon g-color-gray-dark-v3 g-brd-right  text-center ">
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </span>
                                <span class="">
                                    COMPOSICIÓN Y CUIDADOS
                                </span>
                            </a>
                        </h5>
                    </div>
                    <div id="accordion-07-body-03" class="g-color-gray-dark-v3 collapse show" role="tabpanel" aria-labelledby="accordion-07-heading-03" data-parent="#accordion-072">
                        <div class="u-accordion__body  g-font-size-13 g-color-gray-dark-v5 text-left">
                            <p>
                                <img data-src="{{ asset('assets/global/img/icons/iconos-pw-01.png') }}" class="photo rounded faq-icon-bot" alt="...">
                                Lavar en máquina máx. 30 grados<br>
                                <img data-src="{{ asset('assets/global/img/icons/iconos-pw-02.png') }}" class="photo rounded faq-icon-bot" alt="...">
                                No usar blanqueadores<br>
                                <img data-src="{{ asset('assets/global/img/icons/iconos-pw-03.png') }}" class="photo rounded faq-icon-bot" alt="...">
                                No usar secadora<br>
                                <img data-src="{{ asset('assets/global/img/icons/iconos-pw-05.png') }}" class="photo rounded faq-icon-bot" alt="...">
                                Planchar máx. 150<br>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- End Card -->
            </div>
            <!-- Fin Cuidados -->

            <!-- Envios -->
            <div id="accordion-071" class="u-accordion " role="tablist" aria-multiselectable="true">
                <!-- Card -->
                <div class="card rounded-0 g-brd-none">
                    <div id="accordion-07-heading-02" class="u-accordion__header g-pa-10" role="tab">
                        <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700  mb-0">
                            <a class="d-flex g-color-gray-dark-v3 g-text-underline--none--hover" href="#accordion-07-body-02" data-toggle="collapse" data-parent="#accordion-071" aria-expanded="true" aria-controls="accordion-07-body-02">
                                <span class="u-accordion__control-icon g-color-gray-dark-v3 g-brd-right  text-center ">
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </span>
                                <span class="">
                                    ENVÍOS Y DEVOLUCIONES
                                </span>
                            </a>
                        </h5>
                    </div>
                    <div id="accordion-07-body-02" class="g-color-gray-dark-v3 collapse show" role="tabpanel" aria-labelledby="accordion-07-heading-02" data-parent="#accordion-071">
                        <div class="u-accordion__body  g-font-size-13 g-color-gray-dark-v5 text-left ">
                            Dentro de la Cd. de Aguascalientes: $99.00 MXN<br>
                            Interior de la República: $99.00 MXN<br>
                            <span class="g-pt-0">Resto del Mundo: se definirá en base a una cotización.</span>
                            <hr class="g-bg-gray-light-v5 g-pt-0 g-my-10 g-mb-0">
                            Para cambios y devoluciones envíanos un mensaje
                            a nuestros canales de Atención a Clientes
                        </div>
                    </div>
                </div>
                <!-- End Card -->
            </div>
            <!-- Fin Envios -->

            <!-- Compartir -->
            <div id="accordion-076" class="u-accordion " role="tablist" aria-multiselectable="true">
                <!-- Card -->
                <div class="card rounded-0 g-brd-none">
                    <div id="accordion-07-heading-02" class="u-accordion__header g-pa-10" role="tab">
                        <h5 class="mb-0 text-uppercase g-font-size-default g-font-weight-700  mb-0">
                            <a class="d-flex g-color-gray-dark-v3 g-text-underline--none--hover" href="#accordion-07-body-06" data-toggle="collapse" data-parent="#accordion-076" aria-expanded="true" aria-controls="accordion-07-body-06">
                                <span class="u-accordion__control-icon g-color-gray-dark-v3 g-brd-right  text-center ">
                                    <i class="fa fa-plus"></i>
                                    <i class="fa fa-minus"></i>
                                </span>
                                <span class="">
                                    COMPARTIR 
                                </span>
                            </a>
                        </h5>
                    </div>
                    <div id="accordion-07-body-06" class="g-color-gray-dark-v3 collapse show" role="tabpanel" aria-labelledby="accordion-07-heading-06" data-parent="#accordion-076">
                        <div class="u-accordion__body  g-font-size-30 g-color-gray-dark-v5 text-left ">
                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" target="_blank">
                                <i class="fab fa-facebook-square"></i> 
                            </a>
                            
                            <a href="https://twitter.com/home?status={{ url()->current() }}" target="_blank">
                                <i class="fab fa-twitter-square"></i> 
                            </a>

                            <a href="https://pinterest.com/pin/create/button/?url={{ url()->current() }}" target="_blank">
                                <i class="fab fa-pinterest-square"></i> 
                            </a>

                            <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ url()->current() }}" target="_blank">
                                <i class="fab fa-linkedin"></i> 
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Card -->
            </div>
            <!-- Fin Compartir -->

        </div>
        <!-- End: right sidebar -->

    </div>

    @if ($productos[0]->CAMPO_ESPECIAL_1 != null)
        <div class="container">
            <div class="g-brd-y g-pt-100 g-pb-70">
                <h2 class="h4 g-mb-35">Más Información</h2>

                <div class="row">

                    <div class="col-md-6 g-mb-0 g-mb-30--md">
                        <ul class="list-unstyled g-color-text">
                            <li class="g-brd-bottom--dashed g-brd-gray-light-v3 pt-1 mb-3">
                                <span>{{$productos[0]->CAMPO_ESPECIAL_1}}</span>
                            </li>
                        </ul>
                    </div>

                    @if ($productos[0]->CAMPO_ESPECIAL_2 != null)
                    <div class="col-md-6 g-mb-0 g-mb-30--md">
                        <ul class="list-unstyled g-color-text">
                            <li class="g-brd-bottom--dashed g-brd-gray-light-v3 pt-1 mb-3">
                                <span>{{$productos[0]->CAMPO_ESPECIAL_2}}</span>
                            </li>
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    @endif

    @if(count($productos_accesorios) > 0)
        <!-- Start: Seccion Interes -->
        <section id="index-seccion-4" class="margin-top" style="width: 99vw;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col txt-muletta-gris mbot-20 text-center">
                        <h2>
                            COMPLETA TU LOOK
                        </h2>
                    </div>
                </div>

                <input type="hidden" id="productos_completa_cantidad" value="{{ count($productos_accesorios) }}">

                <!--carousel te puede interesar-->
                <div class="carousel-wrap" style="padding: 0 2% !important;">
                    <div class="owl-carousel product-carousel">
                        @foreach($productos_accesorios as $key => $value)
                            <a href="{{ route('producto_individual', ['DESCRIPCION_MODELO' => str_replace(' ','-',str_replace('/','-',$value->DESCRIPCION_MODELO)), 'ID_MODELO' => $value->ID_MODELO])}}">
                                <div class="item  container-change-img">
                                    <img src="<?=$value->IMAGEN_100 ? str_replace('http:', 'https:', $value->IMAGEN_100) :  asset('assets/global/img/index/temporada-7/placeholder-500x620.png')?>" class="image-change-img" >
                                    <div class="overlay-change-img">
                                        <img src="<?=$value->IMAGEN_101 ? str_replace('http:', 'https:', $value->IMAGEN_101) : asset('assets/global/img/index/temporada-7/placeholder-500x620.png')?>" class="snd-img" >
                                    </div>
                                    
                                    <div class="product-content text-center">
                                        <br>
                                        
                                        <!-- <p>COMPLETA TU LOOK</p>-->
                                        <h2><?=$value->DESCRIPCION_MODELO?></h2>
                                        @if($value->PRECIO_VENTA_DESCUENTO != null && $value->PRECIO_VENTA_DESCUENTO != $value->PRECIO_VENTA )
                                            <span style="text-decoration:line-through; color: red !important;">${{number_format($value->PRECIO_VENTA, 2)}}</span>  &nbsp;
                                            <span>${{number_format($value->PRECIO_VENTA_DESCUENTO, 2)}}</span>
                                        @else
                                            <span>${{number_format($value->PRECIO_VENTA, 2)}}</span>
                                        @endif
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>

            </div>
        </section>
        <!-- End: Seccion Interes -->
    @endif

    @if(count($productos_interesar) > 0)
        <!-- Start: Seccion 04 -->
        <section id="index-seccion-4" class="margin-top" style="width: 99vw;">
            <div class="container-fluid g-mb-100">
                <div class="row">
                    <div class="col txt-muletta-gris mbot-20 text-center">
                        <h2>
                            DESTACADOS
                        </h2>
                    </div>
                </div>

                <input type="hidden" id="productos_destacados_cantidad" value="{{ count($productos_interesar) }}">

                <div class="carousel-wrap" style="padding: 0 2% !important;">
                    <div class="owl-carousel product-carousel destacado">

                        @foreach($productos_interesar as $key => $value)
                            <a href="{{ route('producto_individual', ['DESCRIPCION_MODELO' => str_replace(' ','-',str_replace('/','-',$value->DESCRIPCION_MODELO)), 'ID_MODELO' => $value->ID_MODELO])}}">
                                <div class="item  container-change-img">
                                    @if(substr($value->IMAGEN_100, 0, 1) == '~')
                                        <img src="<?=$value->IMAGEN_100 ? str_replace('http:', 'https:', $value->IMAGEN_100) : asset('assets/global/img/index/temporada-7/placeholder-500x620.png')?>" class="image-change-img">
                                    @else
                                        <img src="<?=$value->IMAGEN_100 ? str_replace('http:', 'https:', $value->IMAGEN_100) : asset('assets/global/img/index/temporada-7/placeholder-500x620.png')?>" class="image-change-img">
                                    @endif
                                    <div class="overlay-change-img">
                                        @if(substr($value->IMAGEN_100, 0, 1) == '~')
                                            <img src="<?=$value->IMAGEN_101 ? str_replace('http:', 'https:', $value->IMAGEN_101) : asset('assets/global/img/index/temporada-7/placeholder-500x620.png')?>" class="snd-img">
                                        @else
                                            <img src="<?=$value->IMAGEN_101 ? str_replace('http:', 'https:', $value->IMAGEN_101) : asset('assets/global/img/index/temporada-7/placeholder-500x620.png')?>" class="snd-img">
                                        @endif
                                    </div>
                                    
                                    <div class="product-content text-center">
                                        <br>
                                        <!--<p>DESTACADOS</p>-->
                                        <h2><?=$value->DESCRIPCION_MODELO?></h2>
                                        @if($value->PRECIO_VENTA_DESCUENTO != null && $value->PRECIO_VENTA_DESCUENTO != $value->PRECIO_VENTA )
                                            <span style="text-decoration:line-through; color: red !important;">${{number_format($value->PRECIO_VENTA, 2)}}</span>  &nbsp;
                                            <span>${{number_format($value->PRECIO_VENTA_DESCUENTO, 2)}}</span>
                                        @else
                                            <span>${{number_format($value->PRECIO_VENTA, 2)}}</span>
                                        @endif
                                    </div>
                                </div>
                            </a>
                        @endforeach

                    </div>
                </div>
            </div>

        </section>
        <!-- End: Seccion 04 -->
    @endif

    <!-- Modal que te sugiere ir al checkout -->
    <div class="modal fade" id="modalCheckout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#!" onclick="refreshCheckout();" class="float-right g-font-size-20 g-text-underline--none--hover g-color-muletta-gray-dark g-color-primary--hover g-mb-10">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="modal-body">
                    
                    <div class="w-100 g-mt-15 g-px-35--md">
                        <div class="row">
                            <div class="col-md-5">
                                @if(substr($productos[0]->IMAGEN_100, 0, 4) != 'http')
                                    <img data-src="{{!empty($productos[0]->IMAGEN_100) ? 'https://creatmos.net'.str_replace('~', '', $productos[0]->IMAGEN_100)  : '/assets/global/img/producto/producto-no-hay-producto.jpg'}}" class="photo img-fluid" alt="Producto">
                                @else
                                    <img data-src="{{!empty($productos[0]->IMAGEN_100) ? $productos[0]->IMAGEN_100  : '/assets/global/img/producto/producto-no-hay-producto.jpg'}}" class="photo img-fluid" alt="Producto">
                                @endif
                            </div>
                            <div class="col-md-7 text-center">
                                <p class="g-color-muletta-gray-theme  muletta-tittle-featured-products g-mt-70--md">{{$productos[0]->DESCRIPCION_MODELO}}</p>
                                <p class="g-color-muletta-gray-theme muletta-price-featured-products">
                                    @if($productos[0]->PRECIO_VENTA_DESCUENTO != null && $productos[0]->PRECIO_VENTA != $productos[0]->PRECIO_VENTA_DESCUENTO)
                                        <span style="text-decoration:line-through;">
                                            $ @include('componentes.precio')
                                        </span>
                                        $ <span>
                                            <?=number_format($productos[0]->PRECIO_VENTA_DESCUENTO, 2)?>
                                        </span>
                                    @else
                                        $ <span id="price">
                                            @include('componentes.precio')
                                        </span>
                                    @endif
                                     | <span id="name"></span>
                                </p>
                            </div>
                        </div>
                        <p class="g-color-muletta-gray-light  muletta-price-modal-text g-mt-20 g-mb-10 text-center">
                            <span class="fa fa-check"> </span> Producto añadido correctamente al carrito de compras
                        </p>
                        <div class="w-100">
                            <div class="row">
                                <div class="col-md-6 g-pr-5--md g-py-10">
                                    <a href="#!" onclick="refreshCheckout();" class="btn-block text-center g-bg-primary u-btn-primary g-font-size-12 text-uppercase g-py-5 g-px-25 g-bg-black--hover g-brd-transparent g-brd-black--hover g-rounded-0" style="color: white;">
                                        SEGUIR COMPRANDO 
                                    </a>
                                </div>
                                <div class="col-md-6 g-pl-5--md g-py-10">
                                    <a href="{{route('compras')}}" class="btn-block text-center g-bg-primary u-btn-primary g-font-size-12 text-uppercase g-py-5 g-px-25 g-bg-black--hover g-brd-transparent g-brd-black--hover g-rounded-0">
                                        COMPRAR AHORA 
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Para agregar nombre -->
    <div class="modal fade" id="modalNotificaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" style="top: 14vh; !important">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header text-right" style="display: block !important;">
                    <a href="#!" onclick="closeModal();" class="g-font-size-20 g-text-underline--none--hover g-color-muletta-gray-dark g-color-primary--hover g-mb-10">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">RECORDATORIO DE STOCK</h4>

                    <div class="row">
                        <div class="col-12 text-center">
                            @foreach($variantes_productos as $key => $variante)
                                @if ($variante->EXISTENCIA == null || $variante->EXISTENCIA <= 0)
                                    <input type="radio" id="modal_{{ trim($variante->DESCRIPCION_TALLA) }}" name="modal_variantes" class="u-checkbox-v1--checked-color-primary u-checkbox-v1--checked-brd-primary g-font-prompt" onchange="refreshProductSize('<?=$variante->DESCRIPCION_TALLA?>')">
                                    <label for="modal_{{ trim($variante->DESCRIPCION_TALLA) }}" style="border-radius: 50%;" class="d-inline-block text-center g-width-35 g-height-35 g-font-size-10 g-px-5 g-brd-around g-brd-2 g-brd-muletta-gray-light txt-muletta-gris-claro  g-brd-primary--hover g-color-primary--hover g-transition-0_3 g-py-4 g-mr-3">{{$variante->DESCRIPCION_TALLA}}</label>
                                @endif
                                <input type="hidden" id="checkModal_{{ trim($variante->DESCRIPCION_TALLA) }}" name="modal_variantes_exist" value="{{ $variante->EXISTENCIA }}">
                            @endforeach
                            <p class="text-center">
                                Selecciona la talla de tu preferencia
                            </p>
                        </div>
                    </div>
                    
                    <div class="w-100 g-mt-15 g-px-35--md g-py-15">
                        <div class="input-group g-rounded-left-3 mb-4">
                            <div class="col-12">
                                <div class="input-group g-rounded-left-3 mb-2">
                                    <span class="input-group-prepend g-width-45">
                                        <span class="input-group-text justify-content-center w-100 g-bg-transparent g-brd-gray-light-v3 g-color-gray-dark-v5">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </span>
                                    <input type="text" placeholder="NOMBRE" id="txtNombreManual" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-rounded-left-0 g-rounded-right-3 g-py-15 g-px-15 input-negado">
                                </div>
                            </div>
                            <div class="col-12">
                                <p id="errorNombre" class="text-danger">Campo obligatorio</p>
                            </div>
                        </div>
                        <div class="input-group g-rounded-left-3 mb-2">
                            <div class="col-12">
                                <div class="input-group g-rounded-left-3 mb-4">
                                    <span class="input-group-prepend g-width-45">
                                        <span class="input-group-text justify-content-center w-100 g-bg-transparent g-brd-gray-light-v3 g-color-gray-dark-v5">
                                            <i class="fa fa-envelope-open"></i>
                                        </span>
                                    </span>
                                    <input type="email" placeholder="E-MAIL" id="txtCorreoManual" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-rounded-left-0 g-rounded-right-3 g-py-15 g-px-15 input-negado">
                                </div>
                            </div>
                            <div class="col-12">
                                <p id="errorCorreo" class="text-danger">Campo obligatorio</p>
                                <p id="errorCorreoValidacion" class="text-danger">Correo no válido</p>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <button onclick="btnProductoNegado()" class="btn u-btn-primary g-font-size-20 text-uppercase  g-px-25">
                                SOLICITAR NOTIFICACIÓN
                            </button>

                            <P class="margin-top text-center">Al solicitar la notificación <br> te enviaremos un correo cuando el producto este disponible.</P>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{{ asset('assets/global/lightgallery/demo/js/lightgallery.js') }}"></script>
    <script src="{{ asset('assets/global/lightgallery/demo/js/lg-pager.js') }}"></script>
    <script src="{{ asset('assets/global/lightgallery/demo/js/lg-autoplay.js') }}"></script>
    <script src="{{ asset('assets/global/lightgallery/demo/js/lg-fullscreen.js') }}"></script>
    <script src="{{ asset('assets/global/lightgallery/demo/js/lg-zoom.js') }}"></script>
    <script src="{{ asset('assets/global/lightgallery/demo/js/lg-hash.js') }}"></script>
    <script src="{{ asset('assets/global/lightgallery/demo/js/lg-share.js') }}"></script>
    
    <!--las 3 se ocupa para el carrousel de productos en la vista movil-->
    <script src="{{ asset('assets/global/slick-carousel/slick/slick.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/hs.core.js') }}"></script>
    <script src="{{ asset('assets/global/js/carousel/hs.carousel.min.js') }}"></script>

    <!--js para esta vista-->
    <script src="{{ asset('assets/global/js/maindsoft/single_product.js') }}"></script>
    
@endsection