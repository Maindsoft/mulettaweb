
@extends('template.main')

@section('title', 'Mi cuenta')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/account.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-grayp-light-v4 g-py-30">
        <div class="container">
            <div class="d-sm-flex ">
                <div class="align-self-left ">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-mr-5">
                            <span>Mis Pedidos</span>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Devoluciones</span>
                        </li>
                    </ul>
                </div>
                <div class="align-self-center ml-auto">
                    <h1 class="h3 mb-0">Proceso de Devolución</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- Mi Cuenta -->
    <div class="container g-pt-70 g-pb-30">
        <div class="g-mb-1">
            <form class="js-validate js-step-form" id="formPago" onsubmit="return formSubmit(this);" method="post" data-progress-id="#stepFormProgress" data-steps-id="#stepFormSteps">
                {{ csrf_field() }}
                <meta name="csrf-token" content="{{ csrf_token() }}">

                <div class="loader" style="position: fixed;"></div>

                <!-- START: Paso 01 - Shopping Cart -------------------------------------------------------------------->
                <div id="stepFormSteps">

                
                    <!-- END Paso 01 -Shopping Cart -------------------------------------------------------------------->
                    <div id="step1" class="active">
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-oro text-uppercase g-mb-30" style="text-align:center;">
                                <h3>TRAMITAR DEVOLUCIÓN</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris text-uppercase g-mb-40" style="text-align:center;">
                                <h4>POLÍTICAS DE CAMBIOS Y DEVOLUCIONES</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-20" style="text-align:left; ">
                                La Tienda Online Muletta funciona con una politica de devolución bien marcada y establecida en las condiciones de contratación
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-20" style="text-align:left;">
                                Esta política de devoluciones solamente es válido en productos comprados en nuestra Tienda Online, Facebook y WhatsApp.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris  g-mb-20" style="text-align:left; ">
                                Sólo se cubrirá el importe de los gastos de envío realizado en online en los siguientes casos:
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight: bold;">
                                - Error en el Envío
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight: bold;">
                                - Error en el producto Enviado
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-20" style="text-align:left;  font-weight: bold;">
                                - Defectos de Fabricación
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris  g-mb-20" style="text-align:left; ">
                                En el resto de casos, el cliente se hará cargo de los gastos de envío.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris  g-mb-40" style="text-align:left;">
                                Cualquier persona puede cambiar un artículo sin utilizar dentro de los 10 días hábiles de la compra, poniéndose en contacto con nuestro servicio de atención al cliente y cumpliendo el siguiente proceso.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-20" style="text-align:left; ">
                                Tarifa de Envíos
                            </div>                  
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight: bold;">
                                Dentro de la Ciudad de Aguascalientes: $99.00 MXN
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight: bold;">
                                Interior de la República: $99.00 MXN
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9 txt-muletta-gris" style="text-align:left; font-weight: bold;">
                                Resto del Mundo: se definirá en base a una cotización.
                            </div>
                            <!-- <div class="col-sm-3 txt-muletta-oro text-uppercase g-mb-30 pull-right" style="text-align:center;">
                                <button class="btn btn-block u-btn-primary g-font-size-13 text-uppercase mb-4 g-bg-black--hover g-brd-black--hover " id="procesar" type="button" data-next-step="#step2">Tramitar cambio</button>
                            </div> -->
                            <div class="col-sm-3 txt-muletta-oro text-uppercase g-mb-30 pull-right" style="text-align:center;">
                                <button class="btn btn-block u-btn-primary g-font-size-13 text-uppercase mb-4 g-bg-black--hover g-brd-black--hover " id="procesar" type="button" data-next-step="#step2">Tramitar devolución</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                            </div>  
                        
                        </div>
                    
                    </div>
                    <!-- START: Paso 02 -Envio -------------------------------------------------------------------->
                    
                    <div id="step2">
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-oro text-uppercase g-mb-30" style="text-align:center;">
                                <h3>TRAMITAR DEVOLUCIÓN</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris text-uppercase  " style="text-align:center;">
                                <h4>PRODUCTOS DEL PEDIDO</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-25 " style="text-align:center;">
                                <h5>Selecciona los productos que deseas devolver</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 g-brd-bottom g-brd-gray-light-v3 txt-muletta-gris text-uppercase g-mb-50">
                            </div>
                            <div class="col-12 g-mb-30" style="overflow: auto;">
                                <!-- Products Block -->
                                @if($productos)
                                    <table class="text-center w-100">
                                        <thead class="h6 g-brd-gray-light-v3 txt-muletta-gris text-uppercase">
                                            <tr>
                                                <th>
                                                    ID COMPRA
                                                </th>
                                                <th>
                                                    # DE TICKET
                                                </th>
                                                <th>
                                                    FECHA
                                                </th>
                                                <th>
                                                    HORA
                                                </th>
                                                <th>
                                                    STATUS
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr class="g-brd-bottom g-brd-gray-light-v3 g-mb-25 text-uppercase">
                                                <td class="text-center g-py-25">
                                                    <span id="id_venta"><?= $ID_VENTA ?></span>
                                                </td>
                                                <td class="text-center g-py-25">
                                                    <span id="no_ticket">{{ $ordenes->NO_TICKET }} </span>
                                                </td>
                                                <td class="text-center g-py-25">
                                                    {{ $ordenes->FECHA_VENTA }}
                                                </td>
                                                <td class="text-center g-py-25">
                                                    {{ $ordenes->HORA_VENTA }}
                                                </td> 
                                                <td class="text-center g-py-25">
                                                    {{ $ordenes->STATUS }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                @endIf
                                <!-- End Products Block -->
                            </div>

                        
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris text-uppercase">
                            </div>
                            <div class="col-12 g-mb-30">
                                <!-- Products Block -->
                                <div class="g-overflow-x-scroll g-overflow-x-visible--lg">
                                    @if($productos)
                                        <table class="text-center w-100">
                                            <thead class="h6  g-brd-gray-light-v3 txt-muletta-gris">
                                                <tr class="g-brd-bottom g-brd-gray-light-v3">
                                                    <th colspan="3">
                                                    </th>
                                                    <th style="font-size: 2vh;">
                                                        Talla
                                                    </th> 
                                                    <th style="font-size: 2vh;">
                                                        Uni.
                                                    </th>
                                                    <th style="font-size: 2vh;">
                                                        Precio
                                                    </th>
                                                    <th style="font-size: 2vh;">
                                                        Desc.
                                                    </th>
                                                    <th style="font-size: 2vh;">
                                                        Total
                                                    </th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach ($productos as $key => $producto)
                                                    <tr class="g-brd-bottom g-brd-gray-light-v3 g-mb-25 text-uppercase">
                                                        <td class="text-center g-py-25">
                                                            <label class="form-check-inline u-check g-pl-25">
                                                                <input id="devolucion_{{ $key }}" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                                    <i class="fa" data-check-icon=""></i>
                                                                </div>
                                                            </label>
                                                        </td>
                                                        <td class="text-center g-py-25">
                                                            <img src="{{ $producto->IMAGEN }}" width="150" height="150" alt="Logo-Muletta">
                                                        </td>
                                                        <td class="text-left g-py-25 txt-muletta-oro" style="font-size: 2vh;">
                                                            <div>{{ $producto->DESCRIPCION }}</div>
                                                            <div  style="font-size: 1.5vh;">{{ $producto->ID_MODELO }}</div>
                                                            <input type="hidden" class="no_parte" value="{{ $producto->ID_MODELO.' '.$producto->DESCRIPCION_TALLA }}">
                                                        </td>
                                                        <td class="text-center g-py-25" style="font-size: 2vh;">
                                                            {{ $producto->DESCRIPCION_TALLA }}
                                                        </td>
                                                        <td class="text-center g-py-25" style="font-size: 2vh;">
                                                            <div class="js-quantity input-group u-quantity-v1 g-width-150 g-brd-primary--focus text-center" style="margin: auto;">
                                                                <i class="js-minus g-color-gray g-color-primary--hover fa fa-minus g-ml-25" onclick=" changedown( {{ $key }} )"></i>
                                                                <input type="hidden" value="{{ $producto->CANTIDAD_VENDIDA }}" id="total_{{ $key }}">
                                                                <input class="js-result form-control text-center g-font-size-13 rounded-0 g-pa-0 g-brd-none product-cart-quantity" id="cantidad_{{ $key }}" type="text" value="{{ $producto->CANTIDAD_VENDIDA }}" readonly  >
                                                                <i class="js-plus g-color-gray g-color-primary--hover fa fa-plus g-mr-25 " onclick=" changeup( {{ $key }} )"></i>
                                                            </div>
                                                        </td>
                                                        <td class="text-center g-py-25" style="font-size: 2vh;">
                                                            ${{ number_format($producto->PRECIO_SIN_DESC,2) }}
                                                        </td>
                                                        <td class="text-center g-py-25 text-danger" style="font-size: 2vh;">
                                                            ${{ number_format ($producto->PRECIO_SIN_DESC - $producto->PRECIO_ORIGINAL,2) }}
                                                            <input id="precio_unitario_{{ $key }}" type="hidden" value="{{ $producto->PRECIO_ORIGINAL ? $producto->PRECIO_ORIGINAL : $producto->PRECIO_SIN_DESC }}">
                                                        </td>
                                                        <td class="text-center g-py-25" style="font-size: 2vh;">
                                                            $ <span id="precio_total_label_{{ $key }}">{{ number_format ($producto->PRECIO_ORIGINAL * $producto->CANTIDAD_VENDIDA,2) }}</span>
                                                            <input class="total" id="precio_total_{{ $key }}" type="hidden" value="{{ $producto->PRECIO_ORIGINAL * $producto->CANTIDAD_VENDIDA }}">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endIf
                                </div>
                                <!-- End Products Block -->
                            </div>

                        
                        </div>
                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <div class="row">
                                    <div class="col-sm-12 txt-muletta-gris text-uppercase " style="text-align:left;font-size:16px;">
                                        <h4 style="font-size: 13px !important;">MOTIVO DE DEVOLUCIÓN: </h4>
                                    </div>
                                    <div class="col-sm-12 txt-muletta-gris text-uppercase " style="text-align:left;">
                                        <select id="motivo_user" class="form-control g-placeholder-gray-light-v1 rounded-0" name="state_user" style="position: relative;top: -1vh;">
                                            <option disabled selected value> Selecciona una opcion </option>
                                            @foreach($motivos AS $key => $motivo )
                                                <option value="{{ $motivo->ID_MOTIVO_DEVOLUCION }}">{{ $motivo->RAZON }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <button class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover pull-right" id="procesar" type="button" data-next-step="#step3" onclick="secondStep()">Continuar</button>
                            </div>  
                        </div>
                    </div>
                    <!-- START: PASO 03 - Pago -------------------------------------------------------------------->
                    <div id="step3">
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-oro text-uppercase g-mb-30" style="text-align:center;">
                                <h3>PROCESO DE CAMBIO</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris text-uppercase " style="text-align:center;">
                                <h4>
                                    PRODUCTOS A DEVOLVER
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris text-uppercase  " style="text-align:center;">
                                <h4>MOTIVO DE DEVOLUCIÓN: <span id="motivo_cambio"><span></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 g-brd-bottom g-brd-gray-light-v3 txt-muletta-gris text-uppercase g-mb-50">
                            </div>
                            <div class="col-12 g-mb-30">
                                <!-- Products Block -->
                                <div class="g-overflow-x-scroll g-overflow-x-visible--lg">
                                    @if($productos)
                                    <table class="text-center w-100">
                                        <thead class="h6  g-brd-gray-light-v3 txt-muletta-gris text-uppercase">
                                            <tr>
                                                <th>
                                                    ID COMPRA
                                                </th>
                                                <th>
                                                    # DE TICKET
                                                </th>
                                                <th>
                                                    FECHA
                                                </th>
                                                <th>
                                                    HORA
                                                </th>
                                                <th>
                                                    STATUS
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr class="g-brd-bottom g-brd-gray-light-v3 g-mb-25 text-uppercase">
                                                <td class="text-center g-py-25">
                                                    <?= $ID_VENTA ?>
                                                </td>
                                                <td class="text-center g-py-25">
                                                    {{ $ordenes->NO_TICKET }}
                                                </td>
                                                <td class="text-center g-py-25">
                                                    {{ $ordenes->FECHA_VENTA }}
                                                </td>
                                                <td class="text-center g-py-25">
                                                    {{ $ordenes->HORA_VENTA }}
                                                </td> 
                                                <td class="text-center g-py-25">
                                                    {{ $ordenes->STATUS }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @endIf
                                </div>
                                <!-- End Products Block -->
                            </div>

                        
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris text-uppercase">
                                </div>
                            <div class="col-12 g-mb-30">
                                <!-- Products Block -->
                                <div class="g-overflow-x-scroll g-overflow-x-visible--lg">
                                    @if($productos)
                                        <table class="text-center w-100">
                                            <thead class="h6  g-brd-gray-light-v3 txt-muletta-gris">
                                                <tr class="g-brd-bottom g-brd-gray-light-v3">
                                                    <th colspan="2">
                                                    </th>
                                                    <th style="font-size: 2vh;">
                                                        Talla
                                                    </th> 
                                                    <th style="font-size: 2vh;">
                                                        Uni.
                                                    </th>
                                                    <th style="font-size: 2vh;">
                                                        Precio
                                                    </th>
                                                    <th style="font-size: 2vh;">
                                                        Desc.
                                                    </th>
                                                    <th style="font-size: 2vh;">
                                                        Total
                                                    </th>
                                                </tr>
                                            </thead>

                                            <tbody id='tableBody'>
                                            </tbody>
                                        </table>
                                    @endIf
                                </div>
                                <!-- End Products Block -->
                            </div>

                        
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-oro text-uppercase g-mb-40" style="text-align:center;">
                                <h4>INSTRUCCIONES</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-20" style="text-align:left; ">
                                Al terminar la solicitud de cambio y aceptar los términos, se generará un # de Folio con el que se podrá
                                dar seguimiento a este proceso de manera personal a través de nuestro Whatsapp de Atención a Clientes 449 919 63 07.
                                De igual manera, se te enviará un e-mail con la información del trámite al correo electronico registrado.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-20" style="text-align:left;">
                                Una vez recibida la información, tendremos 48 hrs para responder a tu solicitud.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris  g-mb-20" style="text-align:left; ">
                                Atendida la soliciutd, con la confirmacion de la existencia del producto del cual deseas realizar el cambio deberás de seguir estos pasos:
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left;">
                                -Revisa que las condiciones del prodcuto cumplan con características.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left;">
                                -Revisar que el producto este en buenas condiciones, como se le envío o adquirió en algún Punto de Venta.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left;">
                                -Revisar que el producto mantenga el etiquetado original.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left;">
                                -De ser posible enviar el producto con el embalaje original.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-20" style="text-align:left;">
                                -Envíanos el producto que deseas cambiar a esta dirección:
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight:bold;">
                                Destinarario: Muletta
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight:bold;">
                                Calle: 22 de Octubre #105
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight:bold;">
                                Colonia: Modelo
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight:bold;">
                                CP: 20080
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight:bold;">
                                Ciudad: Aguascalientes
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight:bold;">
                            Estado: Aguascalientes
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; font-weight:bold;">
                                País: México
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris g-mb-20" style="text-align:left; font-weight:bold;">
                                Teléfono: 449 919 63 07
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; ">
                                Revisaremos que el producto cumpla con las condiciones mencionadas en el paso 1.
                            </div>                  
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; ">
                                Una vez aprobado el producto se procederá al envío del productos que nos estés solicitando.
                            </div>                  
                        </div>
                        <div class="row">
                            <div class="col-sm-12 txt-muletta-gris" style="text-align:left; ">
                                Te enviaremos tu número de guía.
                            </div>                  
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" id="generarPDF" value="{{ Route('GenerarPdf', ['ID_VENTA' => $ID_VENTA]) }}">
                                <input type="hidden" id="urlPedidos" value="{{ Route('returns') }}">
                                <button id="terminar" type="button" class="btn u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover pull-right" style="float: right;width: 11vw;">Continuar</button>
                            </div>
                        </div>

                    </div><!-- #step3 -->
                </div>
            </form>
    </div>
</div>
    <!-- Fin mi Cuenta -->
@endsection
@section('script')
<script src="{{ asset('assets/global/js/maindsoft/devolucion.js') }}"></script>
@endsection