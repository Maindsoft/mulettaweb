<!DOCTYPE html>
<html>

<head>
    <style>
        #contenido {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #contenido td,
        #contenido th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #contenido tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #contenido tr:hover {
            background-color: #ddd;
        }

        #contenido th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #b78b1e;
            color: white;
        }

        #datos-solicitados {
            width: 30% !important;
        }

        #info-recibida {
            width: 70% !important;
        }

    </style>
</head>

<body>

    <center>
        <img src="{{ asset('assets/global/img/logo/logo-muletta-header.png') }}" width="200" height="50" alt="Logo-Muletta">
    </center>
    <h1>NUEVO CORREO</h1>
    <h6>DE LA PAGINA PARA FACTURACIÓN</h6>

    <table id="contenido">
        <tr>
            <th id="datos-solicitados">DATOS SOLICITADOS:</th>
            <th id="info-recibida">INFORMACION RECIBIDA:</th>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td>{{ $data['name'] }}</td>
        </tr>

        <tr>
            <td>Correo:</td>
            <td>{{ $data['email'] }}</td>
        </tr>

        <tr>
            <td>rfc:</td>
            <td>{{ $data['rfc'] }}</td>
        </tr>

        <tr>
            <td>empresa:</td>
            <td>{{ $data['empresa'] }}</td>
        </tr>
        <tr>
            <td>tipo:</td>
            <td>{{ $data['tipo'] }}</td>
        </tr>
        <tr>
            <td>calle:</td>
            <td>{{ $data['calle'] }}</td>
        </tr>
        <tr>
            <td>numero:</td>
            <td>{{ $data['numero'] }}</td>
        </tr>
        <tr>
            <td>colonia:</td>
            <td>{{ $data['colonia'] }}</td>
        </tr>
        <tr>
            <td>código postal:</td>
            <td>{{ $data['cp'] }}</td>
        </tr>
        <tr>
            <td>estado:</td>
            <td>{{ $data['estado'] }}</td>
        </tr>
        <tr>
            <td>ciudad:</td>
            <td>{{ $data['ciudad'] }}</td>
        </tr>
        <tr>
            <td>No.pedido:</td>
            <td>{{ $data['nodepedido'] }}</td>
        </tr>
        <tr>
            <td>total:</td>
            <td>{{ $data['totaldecompra'] }}</td>
        </tr>


    </table>

</body>

</html>
