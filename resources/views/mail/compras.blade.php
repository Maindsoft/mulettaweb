<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Nueva compra</title>

		<style type="text/css">
			.fondo{
				background-color: #ffffff;
				width: 60%;
				margin-left: auto;
				margin-right: auto;
				padding-top: 1px;
				padding-left: 15px;
				padding-right: 15px;
				padding-bottom: 30px;
			}
			.div_productos{
				width: 95%;
				margin-left: auto;
				margin-right: auto;
				
			}
			.div_totales{
				width: 100%;
				margin-top: 3vh;
				margin-bottom: 3vh;
				margin-left: auto;
				margin-right: 4px;
			}
			.listo, .pago{
				padding-top: 0;
			}
			.herradura{
				width: 5vw;
			}
			.table_totales{
				margin-right: 2%;
				margin-left: auto;
			}
			.maindsoft-table{
				margin-top:16px;
				margin-left:auto;
				margin-right:auto;
				width:80%;
				text-align:center;
			}
			@media only screen and (max-width:425px){
				.fondo{
					width: 100%;
					margin-left: 0 !important;
					margin-right: 0 !important;
					padding-left: 0 !important;
					padding-right: 0 !important;
				}
				.herradura{
					width: 20vw;
				}
				.table_totales{
					margin-right: auto;
					margin-left: auto;
				}
				.listo, .pago{
					padding-top: 2vh;
				}
				.encuesta{
					padding-top: 2.2vh;
				}
				.maindsoft-table{
					width:100%;
				}
			}
			@media only screen and (max-width:360px){
				.listo, .pago, .encuesta{
					padding-top: 3vh;
				}
			}
			@media only screen and (max-width:320px){
				.fondo{
					width: 113% !important;
				}
				.listo, .pago, .encuesta{
					padding-top: 2.2vh;
				}
			}
		</style>

	</head>
	<body>
		<div class="fondo">
			<div style="margin-top: 5vh;text-align: center;">
				<img class="herradura" src="https://muletta.com/assets/global/img/logo/logo-muletta-footer.png">
				<br>
				<img src="https://muletta.com/assets/global/img/logo/logo-muletta-header.png">

				<table class="maindsoft-table">
					<th>
						<a href="https://muletta.com/mujer" style="text-decoration: none;color:#808080;" target="_blank">
							MUJER
						</a>
					</th>
					<th>
						<a href="https://muletta.com/hombre" style="text-decoration: none;color:#808080;" target="_blank">
							HOMBRE
						</a>
					</th>
					<th>
						<a href="https://muletta.com/match" style="text-decoration: none;color:#808080;" target="_blank">
							#MATCH
						</a>
					</th>
					<th>
						<a href="https://muletta.com/outlet" style="text-decoration: none;color:#808080;" target="_blank">
							OUTLET
						</a>
					</th>
				</table>

			</div>

			<hr style="background-color: #cecece;height: 1px;border: 0;">

			<div style="text-align: center;color: #848484;margin-top:3vh;">
				Estimado (a) {{ $orden['nombre'] }}
				<br>
				<h2 style="color:#b78b1e">
					¡Gracias por tu compra!
				</h2>
				@if($orden['titulo'] && $orden['titulo'] === 'Compra realizada')
					Le informamos que hemos recibido la confirmación de su pago. Su pedido está siendo preparado en almacén, te enviaremos otro correo cuando esté en camino.
				@else
					Le confirmamos que hemos recibido su pedido con éxito. Una vez que validemos su pago, recibirá un correo de confirmación.
				@endif
				
				<div style="width:100%;margin-top:3vh;margin-bottom:3vh;">
					<table style="width: 100%;">
						<tbody>
							<td style="width: 20%;text-align:center">
								<img src="https://muletta.com/assets/global/img/mail/Completado.png" style="width: 45px;">
								<br>
								<div style="background:#d8d7d7;height:16px;padding-top: 4px;">
									<div style="background:#b78b1e;height:10px;width:92%;margin-left:4%"></div>
								</div>
								Pedido
							</td>
							<td style="width: 20%;">
								@if($orden['titulo'] && $orden['titulo'] === 'Compra realizada')
									<img src="https://muletta.com/assets/global/img/mail/Completado.png" style="width: 45px;">
									<br>
									<div style="background:#d8d7d7;height:16px;padding-top: 4px;">
										<div style="background:#b78b1e;height:10px;width:92%;margin-left:4%"></div>
									</div>
								@else
									<img src="https://muletta.com/assets/global/img/mail/Pago.png" style="width: 45px;">
									<br>
									<div style="background:#d8d7d7;height:16px;padding-top: 4px;">
										<div style="background:#848484;height:10px;width:92%;margin-left:4%"></div>
									</div>
								@endif

								Pago
							</td>
							<td style="width: 20%;">
								<img src="https://muletta.com/assets/global/img/mail/Envio.png" style="width: 45px;">
								<br>
								<div style="background:#d8d7d7;height:16px;padding-top: 4px;">
									<div style="background:#848484;height:10px;width:92%;margin-left:4%"></div>
								</div>
								Enviado
							</td>
							<td style="width: 20%;">
								<img src="https://muletta.com/assets/global/img/mail/Entregado.png" style="width: 45px;">
								<br>
								<div style="background:#d8d7d7;height:16px;padding-top: 4px;">
									<div style="background:#848484;height:10px;width:92%;margin-left:4%"></div>
								</div>
								Entregado
							</td>
							<td style="width: 20%;">
								<img src="https://muletta.com/assets/global/img/mail/Encuesta.png" style="width: 45px;">
								<br>
								<div style="background:#d8d7d7;height:16px;padding-top: 4px;">
									<div style="background:#848484;height:10px;width:92%;margin-left:4%"></div>
								</div>
								Encuesta
							</td>
						</tbody>
					</table>
				</div>

			</div>

			<hr style="background-color: #cecece;height: 1px;border: 0;">

			<div style="text-align: center;color: #848484;margin-top:3vh;margin-bottom:3vh;">
				RESUMEN DE COMPRA
			</div>

			<hr style="background-color: #cecece;height: 1px;border: 0;">

			<div>
				<table style="width: 100%">
					<tr style="text-align: center;">
						<th><span style="color:#808080"><strong>N° Pedido</strong></span></th>
						<th><span style="color:#808080"><strong># Ticket</strong></span></th>
						<th><span style="color:#808080"><strong>Fecha</strong></span></th>
						<th><span style="color:#808080"><strong>Hora</strong></span></th>
						<th><span style="color:#808080"><strong>Status</strong></span></th>
					</tr>
					<tr style="text-align: center;">
						<td>
							<span style="color: #848484;">{{ $orden['id_orden'] }}</span>
						</td>
						<td>
							<span style="color: #848484;">{{ $orden['no_ticket'] }}</span>
						</td>
						<td>
							<span style="color: #848484;">{{ $orden['FECHA_VENTA'] }}</span>
						</td>
						<td>
							<span style="color: #848484;">{{ $orden['HORA_VENTA'] }}</span>
						</td>
						<td>
							<span style="color: #848484;">
								@if($orden['titulo'] && $orden['titulo'] === 'Compra realizada')
									Preparando Pedido
								@else
									Pendiente de envío
								@endif
							</span>
						</td>
					</tr>
				</table>
			</div>

			<hr style="background-color: #cecece;height: 1px;border: 0;">

			<div style="text-align: center;color: #848484;margin-top:3vh;margin-bottom:3vh;">
				<span style="color:#808080"><strong>DATOS DE ENVÍO</strong></span>
				<br>
				<br>
				<span style="font-size:16px">
					{{ $orden['calle'] }} {{ $orden['numero_exterior'] }}
					@if($orden['numero_interior'] != null)
						int. {{ $orden['numero_interior'] }}
					@endif
				</span>
				<br>
				<span style="font-size:16px">Fracc. {{ $orden['colonia'] }}</span>
				<br>
				<span style="font-size:16px">
					C.P. {{ $orden['codigo_postal'] }}
					<br>
					{{ ucfirst(strtolower($orden['municipio'])) }}, {{ ucfirst(strtolower($orden['estado'])) }} México
				</span>
			</div>

			<hr style="background-color: #cecece;height: 1px;border: 0;">

			<div style="text-align: center;color: #848484;margin-top:3vh;margin-bottom:3vh;">
				<span style="color:#808080">
					<strong>MÉTODO DE PAGO</strong>
				</span><br>
				<span style="font-size:16px">
					{{ $orden ['metodo_pago'] }}
				</span>
			</div>

			<hr style="background-color: #cecece;height: 1px;border: 0;">

			<div class="div_productos">
				<table>
					<tbody>
						<?php
							$subtotal = 0;
						?>
						@foreach($orden['productos'] as $producto)
							<tr style="border-bottom: 1px #cecece solid;">
								<td style="width: 25%; padding: 13px;">
									<img src="{{ $producto->IMAGEN }}" width="100%">
								</td>
								<td style="width:25%;">
									<p style="font-size: 16px; color: #b78b1e">{{ $producto->DESCRIPCION }}</p>
									<p style="font-size: 12px; color: #b78b1e">{{ $producto->ID_MODELO }}</p>
									<p style="font-size: 12px;">TALLA:</p>
									<p style="font-size: 12px;">{{ $producto->TALLA }}</p>
								</td>
								<td style="width:50%">
									<p>
										<span style="font-size: 16px; text-align: center;">Cantidad: </span>
										<span style="font-size: 14px; text-align: center; float: right;">{{ $producto->CANTIDAD_VENDIDA }}</span>
									</p>
									<p>
										<span style="font-size: 16px; text-align: center;">Precio: </span>
										<span style="font-size: 14px; text-align: center; float: right;">$ {{ $producto->PRECIO_ORIGINAL }}</span>
									</p>
									<p>
										<span style="font-size: 16px; text-align: center;">Descuento: </span>
										<span style="font-size: 14px; text-align: center;color:red; float: right;">$ {{ $producto->PRECIO_SIN_DESC - $producto->PRECIO_ORIGINAL }}</span>
									</p>
									<p>
										<span style="font-size: 16px; text-align: center;">Total: </span>
										<span style="font-size: 14px; text-align: center; float: right;">$ {{ $producto->PRECIO * $producto->CANTIDAD_VENDIDA }}</span>
									</p>
								</td>
							</tr>
							<?php
								$subtotal = $subtotal + ($producto->PRECIO * $producto->CANTIDAD_VENDIDA);
							?>
						@endforeach
					</tbody>
				</table>
			</div>

			<hr style="background-color: #cecece;height: 1px;border: 0;">

			<div class="div_totales">
				<table class="table_totales">
					<tr>
						<td valign="top" style="padding: 0px 18px 9px;color: #6F6F6E;font-size: 12px;font-style: normal;font-weight: normal;text-align: left;">
						
							<span style="font-size:14px">TOTAL PRODUCTOS:</span><br>
							<span style="font-size:14px">ENVÍO 2 A 5 DÍAS :</span><br>
							<span style="font-size:14px">CÓDIGO DE DESCUENTO:</span><br>
							<span style="font-size:14px">PACKING DE REGALO:</span><br>
							<span style="font-size:14px">IMPORTE TOTAL:</span>
						</td>
						
						<td valign="top" style="padding: 0px 18px 9px;color: #6F6F6E;font-size: 12px;font-style: normal;font-weight: normal;text-align: right;">
							<?php
								$envio = $subtotal < 1200 ? 100 : 0;
								$descuento = $orden['monto_desc'] ? $orden['monto_desc'] : 0;
								$regalo = $orden['regalo'] ? $orden['regalo'] : 0;
							?>
							<span style="font-size:14px">$ {{ number_format($subtotal, 2) }}</span><br>
							<span style="font-size:14px">$ {{ number_format($envio, 2) }}</span><br>
							<span style="font-size:14px;color:red;">$ {{ number_format($descuento, 2) }}</span><br>
							<span style="font-size:14px">$ {{ number_format($regalo, 2) }}</span><br>
							<span style="font-size:14px">$ {{ number_format(($subtotal + $envio + $regalo - $descuento), 2) }}</span>
						</td>
					</tr>
				</table>
			</div>

			<hr style="background-color: #cecece;height: 1px;border: 0;">

			<div style="text-align:center;">
				<p>
					<a href="https://muletta.com/ayuda/politicas_envio" style="color: #6F6F6E;text-decoration: none;">Condiciones de compra</a>
				</p>
				<p>
					<a href="https://muletta.com/ayuda/aviso_privacidad" style="color: #6F6F6E;text-decoration: none;">Política de privacidad</a>
				</p>

				<p style="color: #6F6F6E;">Ayuda</p>
				<a href="https://api.whatsapp.com/send?phone=5214499196307" class="whatsapp" target="_blank">
					<img src="https://assets.stickpng.com/images/5a4e2ef62da5ad73df7efe6e.png" alt="whatsapp" height="24" width="24">
				</a>
				<p style="color: #6F6F6E;">SIGUENOS</p>
				<table align="center">
					<td>
						<a href="https://www.instagram.com/mulettaoficial/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-dark-instagram-48.png" alt="Instagram" style="display:block;" height="24" width="24" class=""></a>
					</td>
					<td>
						<a href="https://www.facebook.com/MulettaOficial" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-dark-facebook-48.png" alt="Facebook" style="display:block;" height="24" width="24" class=""></a>
					</td>
				</table>
			</div>
		</div>
	</body>
</html>