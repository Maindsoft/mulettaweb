<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="width: 100%;">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="x-apple-disable-message-reformatting">
    <title>Email Tempalte</title>
    <style type="text/css">
@media only screen and (max-width: 650px) {
  .res-pad {
    width: 92%;
    max-width: 92%;
  }

  .res-full {
    width: 100%;
    max-width: 100%;
  }

  .res-left {
    text-align: left !important;
  }

  .res-right {
    text-align: right !important;
  }

  .res-center {
    text-align: center !important;
  }
}
@media only screen and (max-width: 750px) {
  .margin-full {
    width: 100%;
    max-width: 100%;
  }

  .margin-pad {
    width: 92%;
    max-width: 92%;
    max-width: 600px;
  }
}
</style>
</head>

<body style="-webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; font-family: 'Open Sans', Arial, Sans-serif;">
    <!-- ====== Module : Header ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
        <tr>
            <td>
                <table bgcolor="#ac822e" align="center" width="750" class="margin-full" style="border-spacing: 0; table-layout: auto; margin: 0 auto; background-size: cover; background-position: center; border-radius: 6px 6px 0 0;" border="0" cellpadding="0" cellspacing="0" background="https://maindsoft.net/email_rec/module02-bg01.png">
                    <tr>
                        <td>
                            <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                <tr>
                                    <td height="10" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- column x2 -->
                                <tr>
                                    <td>
                                        <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                            <tr>
                                                <td valign="top">
                                                    <!-- left column -->
                                                    <table width="150" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                        <!-- image -->
                                                        <tr>
                                                            <td>
                                                                <table align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <img width="80" style="max-width: 80px; width: 100%; line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://maindsoft.net/email_rec/logotipo_hierro_muletta_white.png">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <!-- image end -->
                                                    </table>
                                                    <!-- left column end -->
                                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                    <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                        <tr>
                                                            <td height="20" style="font-size:0px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                    <!-- right column -->
                                                    <table width="430" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                        <tr>
                                                            <td height="28" style="font-size: 0px;">&nbsp;</td>
                                                        </tr>
                                                        <!-- nested column -->
                                                        <tr>
                                                            <td>
                                                                <table align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><![endif]-->
                                                                                        <!--[if !((gte mso 9)|(IE))]-->
                                                                                        <!-- column -->
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <!--[endif]-->
                                                                                                <!-- link -->
                                                                                                <td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                    <a href="https://muletta.host/mujer" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                        MUJER
                                                                                                    </a>
                                                                                                </td>
                                                                                                <!-- link end -->
                                                                                                <!--[if !((gte mso 9)|(IE))]-->
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!-- column end -->
                                                                                        <!-- column -->
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <!--[endif]-->
                                                                                                <!-- link -->
                                                                                                <td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                    <a href="https://muletta.host/hombre" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                        HOMBRE
                                                                                                    </a>
                                                                                                </td>
                                                                                                <!-- link end -->
                                                                                                <!--[if !((gte mso 9)|(IE))]-->
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!-- column end -->
                                                                                        <!-- column -->
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <!--[endif]-->
                                                                                                <!-- link -->
                                                                                                <td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                    <a href="https://muletta.host/match" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                        MATCH
                                                                                                    </a>
                                                                                                </td>
                                                                                                <!-- link end -->
                                                                                                <!--[if !((gte mso 9)|(IE))]-->
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!-- column end -->
                                                                                        <!-- column -->
                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <!--[endif]-->
                                                                                                <!-- link -->
                                                                                                <td class="res-center" style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                    <a href="https://muletta.host/outlet" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                        OUTLET
                                                                                                    </a>
                                                                                                </td>
                                                                                                <!-- link end -->
                                                                                                <!--[if !((gte mso 9)|(IE))]-->
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!-- column end -->
                                                                                        <!--[endif]-->
                                                                                        <!--[if (gte mso 9)|(IE)]></tr></table><![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <!-- nested column end -->
                                                    </table>
                                                    <!-- right column end -->
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="5" style="font-size:0px">&nbsp;</td>
                                </tr>




                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    
    
    <!-- ====== Module : Mensaje Bienvenida ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
        <tr>
            <td>
                <table bgcolor="white" width="750" align="center" class="margin-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                    <tr>
                        <td>
                            <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                <tr>
                                    <td height="35" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- subtitle -->
                                <tr>
                                    <td class="res-center" style="text-align: center; color: #6f6f6e; font-family: 'Nunito', Arial, Sans-serif; font-size: 19px; letter-spacing: 1.1px; word-break: break-word; font-size: 14px; color: #6f6f6e; font-weight: 700;">
                                        ¡Gracias por comprar con nosotros!
                                    </td>
                                </tr>
                                <!-- subtitle end -->
                                <tr>
                                    <td height="12" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- title -->
                                <tr>
                                    <td class="res-center" style="text-align: center; color: #707070; font-family: 'Raleway', Arial, Sans-serif; font-size: 20px; letter-spacing: 1px; word-break: break-word;">
                                        Ya recibimos tu pedido <br>{{ $orden['nombre'] }}
                                    </td>
                                </tr>
                                <!-- title end -->
                                <tr>
                                    <td height="13" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- dash -->
                                <tr>
                                    <td>
                                        <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                            <tr>
                                                <td>
                                                    <table bgcolor="#6f6f6e" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="55" height="3" style="font-size:0px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- dash end -->
                                
                                
                                <tr>
                                    <td height="27" style="font-size:0px">&nbsp;</td>
                                </tr>

                               
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- ====== Module : Blog Post 2 Columns ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
        <tr>
            <td>
                <table bgcolor="white" width="750" align="center" class="margin-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                    <tr>
                        <td>
                            <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                <tr>
                                    <td height="35" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- column x2 -->
                                <tr>
                                    <td>
                                        <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                            <tr>
                                                <td valign="top">
                                                    <!-- left column -->
                                                    <table style="border-spacing: 0; table-layout: auto; margin: 0 auto; border: 1px solid #F0F0F0; border-radius: 2px; padding: 10px;" width="290" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0">

                                                        <tr>
                                                            <td>
                                                                <table style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%; padding: 0 5px;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td height="20" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- title -->
                                                                    <tr>
                                                                        <td class="res-center" style="text-align: left; color: #505050; font-family: 'Raleway', Arial, Sans-serif; font-size: 19px; letter-spacing: 0.7px; word-break: break-word; font-weight: 500; line-height: 28px;">
                                                                            Detalles del pedido
                                                                        </td>
                                                                    </tr>
                                                                    <!-- title end -->


                                                                    <tr>
                                                                        <td height="22" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="6" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Método de pago
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden ['metodo_pago'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="6" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Correo
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden['email'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="3" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Num. de Orden
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden['id_orden'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="3" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Num. de Pago
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    # 24110953
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="3" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    {{ $orden['id_pago'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    FedEx
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- left column end -->
                                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                    <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                        <tr>
                                                            <td height="70" style="font-size:0px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                    <!-- right column -->
                                                    <table style="border-spacing: 0; table-layout: auto; margin: 0 auto; border: 1px solid #F0F0F0; border-radius: 2px; padding: 10px;" width="290" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0">

                                                        <tr>
                                                            <td>
                                                                <table style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%; padding: 0 5px;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td height="20" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- title -->
                                                                    <tr>
                                                                        <td class="res-center" style="text-align: left; color: #505050; font-family: 'Raleway', Arial, Sans-serif; font-size: 19px; letter-spacing: 0.7px; word-break: break-word; font-weight: 500; line-height: 28px;">
                                                                            Dirección de Envío
                                                                        </td>
                                                                    </tr>
                                                                    <!-- title end -->
                                                                    <tr>
                                                                        <td height="10" style="font-size:0px">&nbsp;</td>
                                                                    </tr>


                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="12" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Calle
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden['calle'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->
                                                                    <tr>
                                                                        <td height="3" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="12" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Num.Int
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden['numero_interior'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->

                                                                    <tr>
                                                                        <td height="3" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="12" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Colonia
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden['colonia'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->

                                                                    <tr>
                                                                        <td height="3" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="12" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Código Postal
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden['codigo_postal'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->

                                                                    <tr>
                                                                        <td height="3" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="12" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Municipio
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden['municipio'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->


                                                                    <tr>
                                                                        <td height="3" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- dash -->
                                                                    <tr>
                                                                        <td>
                                                                            <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <table bgcolor="#F0F0F0" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- dash end -->
                                                                    <tr>
                                                                        <td height="12" style="font-size:0px">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- column x2 -->
                                                                    <tr>
                                                                        <td>
                                                                            <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                                                <tr>
                                                                                    <td valign="top">
                                                                                        <!-- left column -->
                                                                                        <table width="125" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                                                                                    Estado
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- left column end -->
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                                                                        <table width="1" align="left" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <tr>
                                                                                                <td height="5" style="font-size:0px">&nbsp;</td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                                                                        <!-- right column -->
                                                                                        <table width="125" align="right" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                                                            <!-- paragraph -->
                                                                                            <tr>
                                                                                                <td class="res-center" style="text-align: right; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 14.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 600;">
                                                                                                    {{ $orden['estado'] }}
                                                                                                </td>
                                                                                            </tr>
                                                                                            <!-- paragraph end -->
                                                                                        </table>
                                                                                        <!-- right column end -->
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- column x2 end -->


                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- right column end -->
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- column x2 end -->
                                <tr>
                                    <td height="70" style="font-size:0px">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- ====== Module : Mensaje Ver pedido Boton a pagina  ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
        <tr>
            <td>
                <table bgcolor="#f7f7f7 " align="center" width="750" class="margin-full" style="border-spacing: 0; table-layout: auto; margin: 0 auto; background-size: cover; background-position: center;" border="0" cellpadding="0" cellspacing="0" background="https://maindsoft.net/email_rec/module05-bg01.png">
                    <tr>
                        <td>
                            <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                <tr>
                                    <td height="35" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- title -->
                                <tr>
                                    <td class="res-center" style="text-align: center; color: #777; font-family: 'Raleway', Arial, Sans-serif; font-size: 27px; letter-spacing: 1.5px; word-break: break-word; font-weight: 300;">
                                        Tu pedido esta en proceso
                                    </td>
                                </tr>
                                <!-- title end -->
                                <tr>
                                    <td height="24" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- paragraph -->
                                <tr>
                                    <td class="res-center" style="text-align: left; color: #777; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-weight: 400;">
                                       Estamos preparando todo para que tengas tu pedido lo antes posible, tan pronto este saliendo de nuestra sucursal te enviaremos un correo con el numero de guia. Mientras puedes ver los detalles de tu pedido en nuestra pagina web dando clic aquí
                                    </td>
                                </tr>
                                <!-- paragraph end -->
                                <tr>
                                    <td height="27" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- button -->
                                <tr>
                                    <td>
                                        <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                            <tr>
                                                <td>
                                                    <table align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border: 1.5px solid #777; border-radius: 0px; border-radius: 28px;" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td height="42" style="padding: 0 27px; text-align: center;">
                                                                <a target="_blank" href="https://muletta.host/usuario/cuenta/pedidos" style="color: #777; letter-spacing: 1.4px; font-size: 14px; font-family: 'Nunito', Arial, Sans-serif; text-decoration: none; text-align: center; line-height: 24px; word-break: break-word; font-weight: 600;">
                                                                    VER DESDE PAGINA
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- button end -->
                                <tr>
                                    <td height="35" style="font-size:0px">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    <!-- ====== Module : Mensaje Que sigue  ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
        <tr>
            <td>
                <table bgcolor="white" width="750" align="center" class="margin-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                    <tr>
                        <td>
                            <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                <tr>
                                    <td height="70" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- title -->
                                <tr>
                                    <td class="res-center" style="text-align: center; color: #707070; font-family: 'Raleway', Arial, Sans-serif; font-size: 20px; letter-spacing: 1px; word-break: break-word;">
                                        ¿Que sigue?
                                    </td>
                                </tr>
                                <!-- title end -->
                                <tr>
                                    <td height="13" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- dash -->
                                <tr>
                                    <td>
                                        <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                            <tr>
                                                <td>
                                                    <table bgcolor="#6f6f6e" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="55" height="3" style="font-size:0px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- dash end -->
                                <tr>
                                    <td height="25" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- paragraph -->
                                <tr>
                                    <td class="res-center" style="text-align: left; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word">
                                        Nuestro transportista preparará su pedido para la entrega. Te enviaremos otro correo cuando este en camino y finalmente cuando llegue. Una vez más, gracias por elegirnos, si tienes algún comentario, estoy a solo un correo electrónico de distancia.
                                    </td>
                                </tr>
                                <!-- paragraph end -->


                                <tr>
                                    <td height="70" style="font-size:0px">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
  



    <!-- ====== Module : Footer ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
        <tr>
            <td>
                <table bgcolor="#ac822e" align="center" width="750" class="margin-full" style="border-spacing: 0; table-layout: auto; margin: 0 auto; background-size: cover; background-position: center; border-radius: 0 0 6px 6px;" border="0" cellpadding="0" cellspacing="0" background="https://maindsoft.net/email_rec/module20-bg01.png">
                    <tr>
                        <td>
                            <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                <tr>
                                    <td height="70" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- image -->
                                <tr>
                                    <td>
                                        <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                            <tr>
                                                <td>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                        <tr>
                                                            <td>
                                                                <img width="80" style="max-width: 80px; width: 100%; line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://maindsoft.net/email_rec/logotipo_hierro_muletta_white.png">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- image end -->
                                <tr>
                                    <td height="20" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                            <!-- image -->
                                            <tr>
                                                <td>
                                                    <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                                        <tr>
                                                            <td style="padding: 0 4px;">
                                                                <table style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 50%; padding: 10px; border: 1px solid white;" align="center" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                           <a target="_blank" href="https://es-la.facebook.com/MulettaOficial/" style="color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 17px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                            <img width="15" style="max-width: 15px; width: 100%; line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://maindsoft.net/email_rec/module20-img02.png">
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="padding: 0 4px;">
                                                                <table style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 50%; padding: 10px; border: 1px solid white;" align="center" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <a target="_blank" href="https://twitter.com/muletta_?lang=en" style="color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 17px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                <img width="15" style="max-width: 15px; width: 100%; line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://maindsoft.net/email_rec/module20-img03.png">
                                                                            </a>

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="padding: 0 4px;">
                                                                <table style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 50%; padding: 10px; border: 1px solid white;" align="center" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                           <a target="_blank" href="https://www.instagram.com/mulettaoficial/?hl=en" style="color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 17px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                            <img width="15" style="max-width: 15px; width: 100%; line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://maindsoft.net/email_rec/module20-img04.png">
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="padding: 0 4px;">
                                                                <table style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 50%; padding: 10px; border: 1px solid white;" align="center" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <a target="_blank" href="https://muletta.host/" style="color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 17px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                            <img width="15" style="max-width: 15px; width: 100%; line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://maindsoft.net/email_rec/module20-img05.png">
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </td>
                                            </tr>
                                            <!-- image end -->
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- dash -->
                                <tr>
                                    <td>
                                        <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                            <tr>
                                                <td>
                                                    <table bgcolor="#fff" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- dash end -->
                                <tr>
                                    <td height="20" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- paragraph -->
                                <tr>
                                    <td class="res-center" style="text-align: center; color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; opacity: .7;">
                                       Av. Francisco I. Madero #514
                                        <br>Barrio de la Purísima
                                        <br>20259 Aguascalientes, Ags.                                                             
                                    </td>
                                </tr>
                                <!-- paragraph end -->
                                <!-- paragraph -->
                                <tr>
                                    <td class="res-center" style="text-align: center; color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; opacity: .7;">
                                       Muletta © Todos los derechos reservados &nbsp;<small>•</small>&nbsp;
                                        <a href="https://muletta.host/ayuda/terminos_condiciones" style="color: white; text-decoration: none;">
                                            Terminos &amp; Condiciones
                                        </a>
                                    </td>
                                </tr>
                                <!-- paragraph end -->
                                <tr>
                                    <td height="20" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <!-- dash -->
                                <tr>
                                    <td>
                                        <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                            <tr>
                                                <td>
                                                    <table bgcolor="#fff" align="center" style="border-spacing: 0; table-layout: auto; margin: 0 auto; border-radius: 10px;" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- dash end -->
                                <tr>
                                    <td height="70" style="font-size:0px">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- ====== Module : Unsubscribe ====== -->
    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
        <tr>
            <td>
                <table bgcolor="#F5F5F5" width="750" align="center" class="margin-full" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                    <tr>
                        <td>
                            <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                <tr>
                                    <td height="50" style="font-size:0px">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                                            <tr>
                                                <td>
                                                    <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                        <!-- paragraph -->
                                                        <tr>
                                                            <td class="res-center" style="text-align: center; color: #909090; font-family: 'Nunito', Arial, Sans-serif; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-size: 15px;">
                                                                ¿Enviamos el correo a la persona equivocada?
                                                            </td>
                                                        </tr>
                                                        <!-- paragraph end -->
                                                    </table>
                                                </td>
                                                <td>
                                                    <table class="full" align="center" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto; width: 100%;" width="100%">
                                                        <!-- paragraph -->
                                                        <tr>
                                                            <td class="res-center" style="text-align: center; color: #ac822e; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; padding-left: 7px; font-weight: 600;">
                                                                <unsubscribe>De-subscribir</unsubscribe>
                                                            </td>
                                                        </tr>
                                                        <!-- paragraph end -->
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="50" style="font-size:0px">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
