<!DOCTYPE html>
<html>

<head>
    <style>
        #contenido {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #contenido td,
        #contenido th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #contenido tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #contenido tr:hover {
            background-color: #ddd;
        }

        #contenido th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #b78b1e;
            color: white;
        }

        #datos-solicitados {
            width: 30% !important;
        }

        #info-recibida {
            width: 70% !important;
        }

    </style>
</head>

<body>

    <center>
        <img src="https://www.muletta.com/wp-content/uploads/2019/03/LogotipoMuletta-042.png" width="200" height="50" alt="Logo-Muletta">
    </center>
    <h1>NUEVO CORREO</h1>
    <h6>DE LA PAGINA PARA CONTACTO</h6>

    <table id="contenido">
        <tr>
            <th id="datos-solicitados">DATOS SOLICITADOS:</th>
            <th id="info-recibida">INFORMACION RECIBIDA:</th>
        </tr>
        <tr>
            <td>Nombre:</td>
            <td>{{ $data['name'] }}</td>
        </tr>

       <tr>
            <td>Correo:</td>
            <td>{{ $data['email'] }}</td>
        </tr>

        <tr>
            <td>Mensaje:</td>
            <td>{{ $data['message'] }}</td>
        </tr>


    </table>

</body>

</html>
